namespace = swp_test

country_event = {
  id = swp_test.1
  fire_only_once = yes
  hide_window = yes
  is_triggered_only = yes

  immediate = {
    add_resource = {
      alloys = 10000
      minerals = 10000
      energy = 10000
      food = 10000
      consumer_goods = 10000
      sr_bacta = 10000
      sr_laser_cells = 10000
      sr_plasma_cells = 10000
      sr_ordnance = 10000
      sr_starfighter_parts = 10000
      sr_hyperfuel = 10000
      sr_hypermatter = 10000
      sr_tibanna_gas = 10000
      sr_focus = 10000
    }
  }
}

ship_event = {
  id = swp_test.2
  hide_window = yes
  is_triggered_only = yes
  trigger = {
    is_ship_class = shipclass_starbase
    solar_system = {
      has_any_megastructure = yes
      any_system_megastructure = {
        is_shipyard = yes
      }
    }
  }
  immediate = {
    # log = "I disabled [This.GetName] in [This.Solar_System.GetName]"
    solar_system = {
      every_system_megastructure = {
        limit = {
          is_shipyard = yes
        }
        toggle_modifier = {
          modifier = "ms_closed_shipyard"
        }
      }
    }
  }
}

## Make Sith always force-sensitive
event = {
  id = swp_test.6000
  hide_window = yes
  is_triggered_only = yes

  immediate = {
    every_playable_country = {
      every_owned_leader = {
        limit = {
          species = { is_species_class = SIT }
          NOT = { has_trait = leader_trait_swp_force_sensitive }
        }
        add_trait = leader_trait_swp_force_sensitive
      }
    }
  }
}

# reenable shipyard in special case
ship_event = {
  id = swp_test.20
  hide_window = yes
  is_triggered_only = yes
  trigger = {
    is_ship_class = shipclass_starbase
    solar_system = {
      is_within_borders_of = root.owner
      has_any_megastructure = yes
      any_system_megastructure = {
        is_shipyard = yes
        has_modifier = ms_closed_shipyard
      }
    }
  }
  immediate = {
    # log = "I reenabled [This.GetName] in [This.Solar_System.GetName]"
    solar_system = {
      every_system_megastructure = {
        limit = {
          is_shipyard = yes
        }
        remove_modifier = ms_closed_shipyard
      }
    }
  }
}

country_event = {
  id = swp_test.3
  hide_window = yes
  is_triggered_only = yes
  trigger = {
    from = {
      has_any_megastructure = yes
      any_system_megastructure = {
        is_shipyard = yes
        has_modifier = ms_closed_shipyard
      }
    }
  }
  immediate = {
    # log = "System gained by [This.GetName] - [From.GetName]"
    from = {
      every_system_megastructure = {
        limit = {
          is_shipyard = yes
        }
        remove_modifier = ms_closed_shipyard
      }
    }
  }
}

country_event = {
  id = swp_test.4
  hide_window = yes
  is_triggered_only = yes
  immediate = {
    # log = "war ended | this: [This.GetName]  root: [Root.GetName] from: [From.GetName]"
    from = {
      random_war = {
        limit = {
          any_war_participant = {
            is_country = Root
          }
        }
        every_war_participant = {
          every_owned_megastructure = {
            limit = {
              is_shipyard = yes
              has_modifier = ms_closed_shipyard
            }
            remove_modifier = ms_closed_shipyard
          }
        }
      }
    }
  }
}

## defense cannons/towers
planet_event = {
  id = swp_test.5
  hide_window = yes
  is_triggered_only = yes
  trigger = {
    OR = {
      has_building = building_defense_cannon
      has_building = building_defense_tower
    }
    any_fleet_in_orbit = {
      owner = { is_country = from }
      any_owned_ship = {
        OR = {
          is_ship_size_corvette = yes
          is_ship_size_frigate = yes
          is_ship_size_cruiser = yes
          is_ship_size_heavy_cruiser = yes
        }
      }
    }
  }
  immediate = {
    log = "planet bombarded | this: [This.GetName]  root: [Root.GetName] from: [From.GetName]"
    ## incrementing damage
    variable_random_increment = {
      variable = ship_damage
      value = 20
    }
    log = "ship damage incremented to [PrevPrev.ship_damage]"
    if = {
      limit = {
        check_variable = {
          which = ship_damage
          value >= 80
        }
      }
      solar_system = {
        random_fleet_in_system = {
          limit = {
            owner = { is_country = from }
            orbit = { is_planet = this }
            any_owned_ship = {
              OR = {
                is_ship_size_corvette = yes
                is_ship_size_frigate = yes
                is_ship_size_cruiser = yes
                is_ship_size_heavy_cruiser = yes
              }
            }
          }
          log = "I can bring down a ship with my ship damage [PrevPrev.ship_damage]"
          random_owned_ship = {
            limit = {
              OR = {
                is_ship_size_corvette = yes
                is_ship_size_cruiser = yes
                is_ship_size_frigate = yes
                is_ship_size_heavy_cruiser = yes
              }
            }
            ### 10% chance to hit a corvette
            ### 15% chance to hit a frigate
            ### 20% chance to hit a cruiser
            ### 25% chance to hit a heavy cruiser
            random_list = {
              10 = {
                modifier = {
                  is_ship_size_frigate = yes
                  add = 5 # 10 + 5 = 15
                }
                modifier = {
                  is_ship_size_cruiser = yes
                  add = 10 # 10 + 10 = 20
                }
                modifier = {
                  is_ship_size_heavy_cruiser = yes
                  add = 15 # 10 + 15 = 25
                }
                log = "I will bring down a ship with my ship_damage [PrevPrevPrev.ship_damage]"
                ## defense tower
                if = {
                  limit = {
                    prevprevprev = {
                      has_building = building_defense_tower
                    }
                  }
                  planetary_defense_damage_ship = {
                    corvette_min_ship_damage_value = 80
                    frigate_min_ship_damage_value = 100
                    cruiser_min_ship_damage_value = 150
                    heavy_cruiser_min_ship_damage_value = 190
                  }
                  ## WIP: I still have to make this more dynamic
                  prevprevprev = {
                    variable_random_increment = {
                      variable = ship_damage
                      value = -150
                    }
                    log = "ship destroyed, variable down to [This.ship_damage]"
                  }
                }
                ## defense cannon
                else = {
                  planetary_defense_damage_ship = {
                    corvette_min_ship_damage_value = 100
                    frigate_min_ship_damage_value = 150
                    cruiser_min_ship_damage_value = 200
                    heavy_cruiser_min_ship_damage_value = 275
                  }
                  ## WIP: I still have to make this more dynamic
                  prevprevprev = {
                    variable_random_increment = {
                      variable = ship_damage
                      value = -200
                    }
                    log = "ship destroyed, variable down to [This.ship_damage]"
                  }
                }
              }
              90 = {
                modifier = {
                  is_ship_size_frigate = yes
                  add = -5
                }
                modifier = {
                  is_ship_size_cruiser = yes
                  add = -10
                }
                modifier = {
                  is_ship_size_heavy_cruiser = yes
                  add = -15
                }
                prevprevprev = {
                  variable_random_increment = {
                    variable = ship_damage
                    value = -10
                  }
                  log = "oooops, I missed, variable down to [This.ship_damage]"
                }
              }
            }
          }
        }
      }
    }
  }
}