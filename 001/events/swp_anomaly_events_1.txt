namespace = swp_anomaly

#####################################################
## Planet of the Thousand Moons (Drol event-chain) ##
##			    by Jonas Kuester (Akyla)	       ##
#####################################################

ship_event = {
	id = swp_anomaly.0
	title = swp_anomaly.0.name
	desc = swp_anomaly.0.desc
	picture = GFX_evt_Iego_Planet
	show_sound = event_radio_chatter
	location = from
	
	is_triggered_only = yes
	
	trigger = {
		owner = {
			is_AI = no
			NOT = { has_country_flag = swp_encountered_iego }
		}
		from = { any_system_planet = { has_planet_flag = planet_iego } }
	}
	immediate = {
		owner = {
			set_country_flag = swp_encountered_iego
		}
	}	
	option = {
		name = swp_anomaly.0.a
		enable_special_project = {
			owner = fromfrom
			name = swp_Iego_investigation_project
			location = event_target:event_iego	## had to add a global event target on system init 
		}
	}
}

ship_event = {
	id = swp_anomaly.1
	title = swp_anomaly.1.name
	desc = swp_anomaly.1.desc
	is_triggered_only = yes ## Added swp_anomaly.0 that gets called the 1st time a player ship enters the Iego system - Harain
	picture = GFX_evt_Iego_Planet

	## attemp to leave the planet
	option = {
		name = swp_anomaly.1.a

		hidden_effect = {
			if = {
				limit = {
					leader = {
						OR = {
							has_trait = leader_trait_swp_force_sensitive  
							is_advanced_force_user = yes
						}
					}
				}
				ship_event = { id = swp_anomaly.2 }
			}
			else = {
				ship_event = { id = swp_anomaly.4 }
			}
		}
	}
	## ask the locals
	option = {
		name = swp_anomaly.1.b
		hidden_effect = {
			ship_event = { id = swp_anomaly.12 }
		}
	}
}

## Leader with Force Sensitive-trait escape attempt
ship_event = {
	id = swp_anomaly.2
	title = swp_anomaly.2.name
	desc = swp_anomaly.2.desc
	is_triggered_only = yes
	location = From
	picture = GFX_evt_Drol_in_action
	
	## Leave the Planet
	option = {
		name = swp_anomaly.2.a

		leader = {
			add_experience = 100
		}
		ship_event = {
			id = swp_anomaly.3
		}
	}
	## Do something about Drol
	option = {
		name = swp_anomaly.2.b

		leader = {
			add_experience = 100
		}
		ship_event = {
			id = swp_anomaly.5
		}
	}
}

## Leave the planet and Add Engineering Research
ship_event = {
	id = swp_anomaly.3
	title = swp_anomaly.3.name
	desc = swp_anomaly.3.desc
	is_triggered_only = yes
	location = From
	picture = GFX_evt_People_watching_Drol
	after ={
		clear_global_event_target = event_iego
	}

	option = {
		name = swp_anomaly.3.a

		root.owner = {
			add_resource = {
				engineering_research = 1000
			}
		}
	}
}

## Escape attempt without Force sensitive trait
ship_event = {
	id = swp_anomaly.4
	title = swp_anomaly.4.name
	desc = swp_anomaly.4.desc
	is_triggered_only = yes
	location = From
	picture = GFX_evt_Drol_in_action

	option = {
		name = swp_anomaly.4.a

		fleet = {
			destroy_fleet = {
				target = THIS
			}
		}
		enable_special_project = {
			name = swp_The_Drol_Dilema_project
			location = event_target:event_iego
		}
	}
	option = {
		name = swp_anomaly.4.b

		clear_global_event_target = event_iego
		root.owner = {
			add_resource = {
				influence = -100
			}
		}
	}
}

## Discover what drol is
ship_event = {
	id = swp_anomaly.5
	title = swp_anomaly.5.name
	desc = swp_anomaly.5.desc
	is_triggered_only = yes
	picture = GFX_evt_People_watching_Drol

	after ={
		clear_global_event_target = event_iego
	}
 
	option = {
		name = swp_anomaly.5.a

		hidden_effect = {
			random_list = {
				10 = { ship_event = { id = swp_anomaly.6 }}
				50 = { ship_event = { id = swp_anomaly.7 }}
				40 = { ship_event = { id = swp_anomaly.8 }}
			}
		}
	}
	option = {
		name = swp_anomaly.5.b

		hidden_effect = {
			random_list = {
				10 = { ship_event = { id = swp_anomaly.9 }}
				50 = { ship_event = { id = swp_anomaly.10 }}
				40 = { ship_event = { id = swp_anomaly.11 }}
			}
		}
	}
}

## Destroy it fail
ship_event = {
	id = swp_anomaly.6
	title = swp_anomaly.6.name
	desc = swp_anomaly.6.desc
	is_triggered_only = yes
	picture = GFX_evt_Drol_Destroyed

	option = {
		name = swp_anomaly.6.a

		root.owner = {
			add_resource = {
				alloys = -100
			}
		}
	}
}
## Destroy it normal
ship_event = {
	id = swp_anomaly.7
	title = swp_anomaly.7.name
	desc = swp_anomaly.7.desc
	is_triggered_only = yes
	picture = GFX_evt_Drol_Destroyed

	option = {
		name = swp_anomaly.7.a

		leader = {
			add_experience = 100
		}

		root.owner = {
			random_owned_planet = {
				limit = {
					is_capital = yes
				}

				while = {
					count = 4
					create_pop = {
						species = THIS
					}
				}
			}

		}
	}
}
## Destroy it success
ship_event = {
	id = swp_anomaly.8
	title = swp_anomaly.8.name
	desc = swp_anomaly.8.desc
	is_triggered_only = yes
	picture = GFX_evt_Drol_Destroyed

	option = {
		name = swp_anomaly.8.a

		leader = {
			add_experience = 200
		}

		root.owner = {
			random_owned_planet = {
				limit = {
					is_capital = yes
				}

				while = {
					count = 4
					create_pop = {
						species = THIS
					}
				}
			}

		}
	}
}

## Salvage success
ship_event = {
	id = swp_anomaly.9
	title = swp_anomaly.9.name
	desc = swp_anomaly.9.desc
	is_triggered_only = yes
	picture = GFX_evt_Drol_Destroyed

	option = {
		name = swp_anomaly.9.a

		root.owner = {
			add_resource = {
				alloys = 150
				sr_laser_cells = 200
			}
			random_owned_planet = {
				limit = {
					is_capital = yes
				}

				while = {
					count = 4
					create_pop = {
						species = THIS
					}
				}
			}
		}

		leader = {
			add_experience = 200
		}
	}
}
## Salvage normal
ship_event = {
	id = swp_anomaly.10
	title = swp_anomaly.10.name
	desc = swp_anomaly.10.desc
	is_triggered_only = yes
	picture = GFX_evt_Drol_Destroyed

	option = {
		name = swp_anomaly.10.a

		root.owner = {
			add_resource = {
				alloys = 100
				sr_laser_cells = 150
			}
			random_owned_planet = {
				limit = {
					is_capital = yes
				}

				while = {
					count = 4
					create_pop = {
						species = THIS
					}
				}
			}
		}
		
		leader = {
			add_experience = 100
		}
	}
}
## Salvage fail
ship_event = {
	id = swp_anomaly.11
	title = swp_anomaly.11.name
	desc = swp_anomaly.11.desc
	is_triggered_only = yes
	picture = GFX_evt_Drol_Destroyed

	option = {
		name = swp_anomaly.11.a

		root.owner = {
			add_resource = {
				alloys = 75
				sr_laser_cells = 75
			}
		}
		
		leader = {
			add_experience = 50
			add_trait = leader_trait_maimed
		}
	}
}

## Ask the locals about Drol
ship_event = {
	id = swp_anomaly.12
	title = swp_anomaly.12.name
	desc = swp_anomaly.12.desc
	is_triggered_only = yes
	picture = GFX_evt_Iego_Planet

	option = {
		name = swp_anomaly.12.a

		ship_event = {
			id = swp_anomaly.5
		}
	}
}


## Scan System Anomalies

### Abandoned Cargoship
ship_event = {
	id = swp_anomaly.100
	title = "swp_anomaly.100.name"
	desc = "swp_anomaly.100.desc"
	picture = GFX_evt_discover_signal
	show_sound = event_ship_bridge
	location = FROM

	is_triggered_only = yes

	immediate = {
		# from = { clear_deposits = yes }
	}

	option = {
		name = swp_anomaly.100.a

		hidden_effect = {
			random_list = {
				##
				40 = { ship_event = { id = swp_anomaly.101 days = 3 }} 
				##
				45 = { ship_event = { id = swp_anomaly.102 days = 3 }}
                ##
				15 = { ship_event = { id = swp_anomaly.103 days = 3 }}
			}
		}
	}
}

ship_event = {
	id = swp_anomaly.101
	title = "swp_anomaly.101.name"
	desc = "swp_anomaly.101.desc"
	picture = GFX_evt_cargo_ship
	show_sound = event_ship_bridge
	location = FROMFROMFROM
	
	is_triggered_only = yes

	option = {
		name = swp_anomaly.101.a
		root.owner = {
			add_resource = {
				sr_bacta = 400
				
			}
		}
	}
}

ship_event = {
	id = swp_anomaly.102
	title = "swp_anomaly.102.name"
	desc = "swp_anomaly.102.desc"
	picture = GFX_evt_cargo_ship
	show_sound = event_ship_bridge
	location = FROMFROMFROM
	
	is_triggered_only = yes

	option = {
		name = swp_anomaly.102.a
		root.owner = {
			add_resource = {
				sr_bacta = 250
				consumer_goods = 250 
				
			}
		}
	}
}

ship_event = {
	id = swp_anomaly.103
	title = "swp_anomaly.103.name"
	desc = "swp_anomaly.103.desc"
	picture = GFX_evt_discover_signal
	show_sound = event_ship_bridge
	location = FROMFROMFROM
	
	is_triggered_only = yes

	option = {
		name = swp_anomaly.103.a
		root.owner = {
			add_resource = {
				influence = -50
				
			}
		}
	}
}

### Tibanna Gas Tanks
ship_event = {
	id = swp_anomaly.200
	title = "swp_anomaly.200.name"
	desc = "swp_anomaly.200.desc"
	picture = GFX_evt_discover_signal
	show_sound = event_ship_bridge
	location = FROM

	is_triggered_only = yes

	immediate = {
		# from = { clear_deposits = yes }
	}

	option = {
		name = swp_anomaly.200.a

		hidden_effect = {
			random_list = {
				## 
				40 = { ship_event = { id = swp_anomaly.201 days = 3 }} 
				## 
				45 = { ship_event = { id = swp_anomaly.202 days = 3 }}
                ## 
				15 = { ship_event = { id = swp_anomaly.203 days = 3 }}
			}
		}
	}
}

ship_event = {
	id = swp_anomaly.201
	title = "swp_anomaly.201.name"
	desc = "swp_anomaly.201.desc"
	picture = GFX_evt_tibanna_tanks
	show_sound = event_ship_bridge
	location = FROMFROMFROM
	
	is_triggered_only = yes

	option = {
		name = swp_anomaly.201.a
		root.owner = {
			add_resource = {
				sr_tibanna_gas = 500
				
			}
		}
	}
}

ship_event = {
	id = swp_anomaly.202
	title = "swp_anomaly.202.name"
	desc = "swp_anomaly.202.desc"
	picture = GFX_evt_tibanna_tanks
	show_sound = event_ship_bridge
	location = FROMFROMFROM
	
	is_triggered_only = yes

	option = {
		name = swp_anomaly.202.a
		root.owner = {
			add_resource = {
				sr_tibanna_gas = 250
				engineering_research = 250 
				
			}
		}
	}
}

ship_event = {
	id = swp_anomaly.203
	title = "swp_anomaly.203.name"
	desc = "swp_anomaly.203.desc"
	picture = GFX_evt_discover_signal
	show_sound = event_ship_bridge
	location = FROMFROMFROM
	
	is_triggered_only = yes

	option = {
		name = swp_anomaly.203.a
		root.owner = {
			add_resource = {
				influence = -50
				
			}
		}
	}
}

### Old Clonewars Outpost
ship_event = {
	id = swp_anomaly.300
	title = "swp_anomaly.300.name"
	desc = "swp_anomaly.300.desc"
	picture = GFX_evt_discover_signal
	show_sound = event_ship_bridge
	location = FROM

	is_triggered_only = yes

	immediate = {
		# from = { clear_deposits = yes }
	}

	option = {
		name = swp_anomaly.300.a

		hidden_effect = {
			random_list = {
				##
				40 = { ship_event = { id = swp_anomaly.301 days = 3 }} 
				##
				45 = { ship_event = { id = swp_anomaly.302 days = 3 }}
                ##
				15 = { ship_event = { id = swp_anomaly.303 days = 3 }}
			}
		}
	}
}

ship_event = {
	id = swp_anomaly.301
	title = "swp_anomaly.301.name"
	desc = "swp_anomaly.301.desc"
	picture = GFX_evt_clonewars_outpost
	show_sound = event_ship_bridge
	location = FROMFROMFROM
	
	is_triggered_only = yes

	option = {
		name = swp_anomaly.301.a
		root.owner = {
			add_resource = {
				sr_laser_cells = 500
				
			}
		}
	}
}

ship_event = {
	id = swp_anomaly.302
	title = "swp_anomaly.302.name"
	desc = "swp_anomaly.302.desc"
	picture = GFX_evt_clonewars_outpost
	show_sound = event_ship_bridge
	location = FROMFROMFROM
	
	is_triggered_only = yes

	option = {
		name = swp_anomaly.302.a
		root.owner = {
			add_resource = {
				sr_laser_cells = 250
				physics_research = 250 
				
			}
		}
	}
}

ship_event = {
	id = swp_anomaly.303
	title = "swp_anomaly.303.name"
	desc = "swp_anomaly.303.desc"
	picture = GFX_evt_discover_signal
	show_sound = event_ship_bridge
	location = FROMFROMFROM
	
	is_triggered_only = yes

	option = {
		name = swp_anomaly.303.a
		root.owner = {
			add_resource = {
				influence = -50
				
			}
		}
	}
}

### Plasma Refinery
ship_event = {
	id = swp_anomaly.400
	title = "swp_anomaly.400.name"
	desc = "swp_anomaly.400.desc"
	picture = GFX_evt_discover_signal
	show_sound = event_ship_bridge
	location = FROM

	is_triggered_only = yes

	immediate = {
		# from = { clear_deposits = yes }
	}

	option = {
		name = swp_anomaly.400.a

		hidden_effect = {
			random_list = {
				##
				40 = { ship_event = { id = swp_anomaly.401 days = 3 }} 
				##
				45 = { ship_event = { id = swp_anomaly.402 days = 3 }}
                ##
				15 = { ship_event = { id = swp_anomaly.403 days = 3 }}
			}
		}
	}
}

ship_event = {
	id = swp_anomaly.401
	title = "swp_anomaly.401.name"
	desc = "swp_anomaly.401.desc"
	picture = GFX_evt_space_refinery
	show_sound = event_ship_bridge
	location = FROMFROMFROM
	
	is_triggered_only = yes

	option = {
		name = swp_anomaly.401.a
		root.owner = {
			add_resource = {
				sr_plasma_cells = 250
				
			}
		}
	}
}

ship_event = {
	id = swp_anomaly.402
	title = "swp_anomaly.402.name"
	desc = "swp_anomaly.402.desc"
	picture = GFX_evt_space_refinery
	show_sound = event_ship_bridge
	location = FROMFROMFROM
	
	is_triggered_only = yes

	option = {
		name = swp_anomaly.402.a
		root.owner = {
			add_resource = {
				sr_plasma_cells = 250
			}
			add_modifier = {
				modifier = "em_plasma_refinery" 
			}
		}
	}
}

ship_event = {
	id = swp_anomaly.403
	title = "swp_anomaly.403.name"
	desc = "swp_anomaly.403.desc"
	picture = GFX_evt_discover_signal
	show_sound = event_ship_bridge
	location = FROMFROMFROM
	
	is_triggered_only = yes

	option = {
		name = swp_anomaly.403.a
		root.owner = {
			add_resource = {
				influence = -50
				
			}
		}
	}
}


### Droid Gunship
ship_event = {
	id = swp_anomaly.500
	title = "swp_anomaly.500.name"
	desc = "swp_anomaly.500.desc"
	picture = GFX_evt_discover_signal
	show_sound = event_ship_bridge
	location = FROM

	is_triggered_only = yes

	immediate = {
		# from = { clear_deposits = yes }
	}

	option = {
		name = swp_anomaly.500.a

		hidden_effect = {
			random_list = {
				##
				40 = { ship_event = { id = swp_anomaly.501 days = 3 }} 
				##
				45 = { ship_event = { id = swp_anomaly.502 days = 3 }}
                ##
				15 = { ship_event = { id = swp_anomaly.503 days = 3 }}
			}
		}
	}
}

ship_event = {
	id = swp_anomaly.501
	title = "swp_anomaly.501.name"
	desc = "swp_anomaly.501.desc"
	picture = GFX_evt_droid_gunship
	show_sound = event_ship_bridge
	location = FROMFROMFROM
	
	is_triggered_only = yes

	option = {
		name = swp_anomaly.501.a
		root.owner = {
			add_resource = {
				sr_ordnance = 250
				
			}
		}
	}
}

ship_event = {
	id = swp_anomaly.502
	title = "swp_anomaly.502.name"
	desc = "swp_anomaly.502.desc"
	picture = GFX_evt_droid_gunship
	show_sound = event_ship_bridge
	location = FROMFROMFROM
	
	is_triggered_only = yes

	option = {
		name = swp_anomaly.502.a
		root.owner = {
			add_resource = {
				sr_ordnance = 250
				alloys = 250
			}
		}
	}
}

ship_event = {
	id = swp_anomaly.503
	title = "swp_anomaly.503.name"
	desc = "swp_anomaly.503.desc"
	picture = GFX_evt_discover_signal
	show_sound = event_ship_bridge
	location = FROMFROMFROM
	
	is_triggered_only = yes

	option = {
		name = swp_anomaly.503.a
		root.owner = {
			add_resource = {
				influence = -50
				
			}
		}
	}
}


### Crashed CIS Ships
ship_event = {
	id = swp_anomaly.600
	title = "swp_anomaly.600.name"
	desc = "swp_anomaly.600.desc"
	picture = GFX_evt_discover_signal
	show_sound = event_ship_bridge
	location = FROM

	is_triggered_only = yes

	immediate = {
		# from = { clear_deposits = yes }
	}

	option = {
		name = swp_anomaly.600.a

		hidden_effect = {
			random_list = {
				##
				40 = { ship_event = { id = swp_anomaly.601 days = 3 }} 
				##
				45 = { ship_event = { id = swp_anomaly.602 days = 3 }}
                ##
				15 = { ship_event = { id = swp_anomaly.603 days = 3 }}
			}
		}
	}
}

ship_event = {
	id = swp_anomaly.601
	title = "swp_anomaly.601.name"
	desc = "swp_anomaly.601.desc"
	picture = GFX_cis_crashed_ship
	show_sound = event_ship_bridge
	location = FROMFROMFROM
	
	is_triggered_only = yes

	option = {
		name = swp_anomaly.601.a
		root.owner = {
			add_resource = {
				sr_starfighter_parts = 250
				
			}
		}
	}
}

ship_event = {
	id = swp_anomaly.602
	title = "swp_anomaly.602.name"
	desc = "swp_anomaly.602.desc"
	picture = GFX_cis_crashed_ship
	show_sound = event_ship_bridge
	location = FROMFROMFROM
	
	is_triggered_only = yes

	option = {
		name = swp_anomaly.602.a
		root.owner = {
			add_resource = {
				sr_starfighter_parts = 250
				alloys = 250
			}
		}
	}
}

ship_event = {
	id = swp_anomaly.603
	title = "swp_anomaly.603.name"
	desc = "swp_anomaly.603.desc"
	picture = GFX_evt_discover_signal
	show_sound = event_ship_bridge
	location = FROMFROMFROM
	
	is_triggered_only = yes

	option = {
		name = swp_anomaly.603.a
		root.owner = {
			add_resource = {
				influence = -50
				
			}
		}
	}
}


### Damaged Freighter
ship_event = {
	id = swp_anomaly.700
	title = "swp_anomaly.700.name"
	desc = "swp_anomaly.700.desc"
	picture = GFX_evt_discover_signal
	show_sound = event_ship_bridge
	location = FROM

	is_triggered_only = yes

	immediate = {
		# from = { clear_deposits = yes }
	}

	option = {
		name = swp_anomaly.700.a

		hidden_effect = {
			random_list = {
				##
				40 = { ship_event = { id = swp_anomaly.701 days = 3 }} 
				##
				45 = { ship_event = { id = swp_anomaly.702 days = 3 }}
				##
				15 = { ship_event = { id = swp_anomaly.703 days = 3 }}
			}
		}
	}
}

ship_event = {
	id = swp_anomaly.701
	title = "swp_anomaly.701.name"
	desc = "swp_anomaly.701.desc"
	picture = GFX_evt_cargo_ship_2
	show_sound = event_ship_bridge
	location = FROMFROMFROM
	
	is_triggered_only = yes

	option = {
		name = swp_anomaly.701.a
		root.owner = {
			add_resource = {
				sr_hyperfuel = 250
				
			}
		}
	}
}

ship_event = {
	id = swp_anomaly.702
	title = "swp_anomaly.702.name"
	desc = "swp_anomaly.702.desc"
	picture = GFX_evt_cargo_ship_2
	show_sound = event_ship_bridge
	location = FROMFROMFROM
	
	is_triggered_only = yes

	option = {
		name = swp_anomaly.702.a
		root.owner = {
			add_resource = {
				sr_hyperfuel = 250
				food = 250
			}
		}
	}
}

ship_event = {
	id = swp_anomaly.703
	title = "swp_anomaly.703.name"
	desc = "swp_anomaly.703.desc"
	picture = GFX_evt_discover_signal
	show_sound = event_ship_bridge
	location = FROMFROMFROM
	
	is_triggered_only = yes

	option = {
		name = swp_anomaly.703.a
		root.owner = {
			add_resource = {
				influence = -50
				
			}
		}
	}
}
