### Combat slowdown events for SWFR

namespace = swp_shipspeed

ship_event = {
	id = swp_shipspeed.1
	hide_window = yes

	trigger = {
		OR = {
			is_ship_class = shipclass_military
			is_ship_class = shipclass_transport
			is_ship_class = shipclass_science_ship
			is_ship_class = shipclass_constructor
			is_ship_class = shipclass_colonizer
			is_ship_class =	shipclass_military_special
		}
		is_in_combat = yes
		Not = { has_ship_flag = swp_combat_slowdown }
	}
	immediate = {
		set_ship_flag = swp_combat_slowdown
		add_modifier = { modifier = combat_slowdown }
	}
}

ship_event = {
	id = swp_shipspeed.2
	hide_window = yes

	trigger = {
		OR = {
			is_ship_class = shipclass_military
			is_ship_class = shipclass_transport
			is_ship_class = shipclass_science_ship
			is_ship_class = shipclass_constructor
			is_ship_class = shipclass_colonizer
			is_ship_class =	shipclass_military_special
		}
		is_in_combat = no
		has_ship_flag = swp_combat_slowdown
	}
	immediate = {
		remove_ship_flag = swp_combat_slowdown
		remove_modifier = combat_slowdown
	}
}