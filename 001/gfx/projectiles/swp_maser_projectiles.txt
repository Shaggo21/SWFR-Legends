###################################################################################
###																				###
###							Star Wars 				    						###
###																				###
###################################################################################

# PROJECTILE MASER CANNON PD
projectile_gfx_ballistic = {
	#common for all types of projectiles
	name = "MASER_CANNON"
	color = { 1.0	1.0		1.0		1.0 }
	hit_entity = "Maser_F_LC_hit_entity"
	muzzle_flash_entity = "Maser_F_LC_muzzle_entity"
	
	#ballistic specific
	entity = "Fighter_LC_maser_small_entity"
	speed = 125.0			#preferred speed of the projectile
	max_duration = 1.0		#Speed of projectile might be scaled up in order to guarantee reaching the target within <max_duration> seconds
}

# SINGLE MEGAMASER
projectile_gfx_ballistic = {
	#common for all types of projectiles
	name = "MASER_SINGLE"
	color = { 1.0	1.0		1.0		1.0 }
	hit_entity = "Single_Maser_hit_entity"
	shield_hit_entity = "Single_Maser_shield_hit_entity"
	muzzle_flash_entity = "Single_Maser_muzzle_entity"
	
	shield_impact = {
		size = small
		delay = 0.0
	}
	
	#ballistic specific
	entity = "Single_Maser_entity"
	speed = 250.0			#preferred speed of the projectile
	max_duration = 2.0		#Speed of projectile might be scaled up in order to guarantee reaching the target within <max_duration> seconds
}

# DUAL MEGAMASER
projectile_gfx_ballistic = {
	#common for all types of projectiles
	name = "MASER_DUAL"
	color = { 1.0	1.0		1.0		1.0 }
	hit_entity = "Dual_Maser_hit_entity"
	shield_hit_entity = "Dual_Maser_shield_hit_entity"
	muzzle_flash_entity = "Dual_Maser_muzzle_entity"
	
	shield_impact = {
		size = medium
		delay = 0.0
	}
	
	#ballistic specific
	entity = "Dual_Maser_entity"
	speed = 250.0			#preferred speed of the projectile
	max_duration = 2.0		#Speed of projectile might be scaled up in order to guarantee reaching the target within <max_duration> seconds
}

# QUAD MEGAMASER
projectile_gfx_ballistic = {
	#common for all types of projectiles
	name = "MASER_QUAD"
	color = { 1.0	1.0		1.0		1.0 }
	hit_entity = "Heavyquad_Maser_hit_entity"
	shield_hit_entity = "Heavyquad_Maser_shield_hit_entity"
	muzzle_flash_entity = "Heavyquad_Maser_muzzle_entity"
	
	shield_impact = {
		size = medium
		delay = 0.0
	}
	
	#ballistic specific
	entity = "Quad_Maser_entity"
	speed = 250.0			#preferred speed of the projectile
	max_duration = 2.0		#Speed of projectile might be scaled up in order to guarantee reaching the target within <max_duration> seconds
}

# HEAVY QUAD MEGAMASER
projectile_gfx_ballistic = {
	#common for all types of projectiles
	name = "MASER_HEAVYQUAD"
	color = { 1.0	1.0		1.0		1.0 }
	hit_entity = "Heavyquad_TL_b_hit_entity"
	shield_hit_entity = "Heavyquad_TL_b_shield_hit_entity"
	muzzle_flash_entity = "Heavyquad_Maser_muzzle_entity"
	
	shield_impact = {
		size = large
		delay = 0.0
	}
	
	#ballistic specific
	entity = "Heavyquad_Maser_entity"
	speed = 250.0			#preferred speed of the projectile
	max_duration = 2.0		#Speed of projectile might be scaled up in order to guarantee reaching the target within <max_duration> seconds
}
