##############################################################
###	PORTRAITS SETTINGS
###
### Which portraits are used is set in 
### common\species_classes\00_species_classes.txt
###
###	This file configures how portraits and planet backgrounds are built.
###	Default position orientation is lower left.
##############################################################

portraits = {
	# falleen
	falleen_female_01 = { entity = "portrait_human_female_01_entity" clothes_selector = "humanoid_master_female_clothes_01" hair_selector = "falleen_female_hair_01" greeting_sound = "human_female_greetings_03"
		character_textures = {
			"gfx/models/portraits/falleen/falleen_female_body_01.dds"
		}
	}
	falleen_male_01 = { entity = "portrait_human_male_01_entity" clothes_selector = "humanoid_master_male_clothes_01" hair_selector = "falleen_male_hair_01" greeting_sound = "human_male_greetings_03"
		character_textures = {
			"gfx/models/portraits/falleen/falleen_male_body_01.dds"
			"gfx/models/portraits/falleen/falleen_male_body_02.dds"
			"gfx/models/portraits/falleen/falleen_male_body_03.dds"
		}
	}
}

portrait_groups = {
	falleen = {
		default = falleen_male_01
		game_setup = { #will run with a limited country scope. species and government is set but the country does not actually exist
			add = {
				trigger = {
					ruler = { gender = male }
				}
				portraits = {
					falleen_male_01
				}
			}
			add = {
				trigger = {
					ruler = { gender = female }
				}
				portraits = {
					falleen_female_01
				}
			}
			#set = {
			#	trigger = { ... }
			#	portraits = { ... }
			#	#using "set =" instead of "add" will first clear any portraits already added
			#}
		}		
		
		#species scope
		species = { #generic portrait for a species
			add = {
				portraits = {
					falleen_male_01
					falleen_female_01
				}
			}
		}		
		
		#pop scope
		pop = { #for a specific pop
			add = {
				portraits = {
					falleen_female_01
					falleen_male_01
				}
			}
		}
		
		#leader scope
		leader = { #scientists, generals, admirals, governor
			add = {
				trigger = {
					gender = female
				}
				portraits = {
					falleen_female_01
				}
			}
			add = {
				trigger = {
					gender = male
				}
				portraits = {
					falleen_male_01
				}
			}
		}

		#leader scope 
		ruler = {
			add = {
				trigger = {
					gender = female
				}
				portraits = {
					falleen_female_01
				}
			}
			add = {
				trigger = {
					gender = male
				}
				portraits = {
					falleen_male_01
				}
			}
		}
	}
}