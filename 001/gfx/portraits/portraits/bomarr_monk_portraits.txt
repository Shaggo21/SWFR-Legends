##############################################################
###	PORTRAITS SETTINGS
###
### Which portraits are used is set in 
### common\species_classes\00_species_classes.txt
###
###	This file configures how portraits and planet backgrounds are built.
###	Default position orientation is lower left.
##############################################################



portraits = {
	bomarr_monk = { entity = "portrait_human_male_01_entity" clothes_selector = "no_texture" hair_selector = "no_texture" greeting_sound = "robot_arthopoid_greetings"
		character_textures = {
			"gfx/models/portraits/bomarr_monk/bomarr_monk_static.dds"
		}
	}
}