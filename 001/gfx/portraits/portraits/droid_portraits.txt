##############################################################
###	PORTRAITS SETTINGS
###
### Which portraits are used is set in 
### common\species_classes\00_species_classes.txt
###
###	This file configures how portraits and planet backgrounds are built.
###	Default position orientation is lower left.
##############################################################

portraits = {
	# Droids
	sw_droid = { entity = "portrait_robot_01_entity" clothes_selector = "no_texture" hair_selector = "no_texture" greeting_sound = "robot_human_greetings" 
		character_textures = {
			"gfx/models/portraits/droid/protocol_droid_01.dds"
			"gfx/models/portraits/droid/protocol_droid_02.dds"
			"gfx/models/portraits/droid/labor_droid_01.dds"
			"gfx/models/portraits/droid/labor_droid_02.dds"
			"gfx/models/portraits/droid/r2_droid_01.dds"
			"gfx/models/portraits/droid/r2_droid_02.dds"
			"gfx/models/portraits/droid/r2_droid_03.dds"
		}	
	}
}