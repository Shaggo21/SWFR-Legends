##############################################################
###	PORTRAITS SETTINGS
###
### Which portraits are used is set in 
### common\species_classes\00_species_classes.txt
###
###	This file configures how portraits and planet backgrounds are built.
###	Default position orientation is lower left.
##############################################################



portraits = {
	bith = { entity = "portrait_human_male_01_entity" clothes_selector = "humanoid_master_male_clothes_01" hair_selector = "no_texture" greeting_sound = "mammalian_01_greetings"
		character_textures = {
			"gfx/models/portraits/bith/bith_male_body_01.dds"
			"gfx/models/portraits/bith/bith_male_body_02.dds"
			"gfx/models/portraits/bith/bith_male_body_03.dds"
		}
	}
}

