##############################################################
###	PORTRAITS SETTINGS
###
### Which portraits are used is set in 
### common\species_classes\00_species_classes.txt
###
###	This file configures how portraits and planet backgrounds are built.
###	Default position orientation is lower left.
##############################################################

# balosar
portraits = {
	balosar_female_01 = { entity = "portrait_human_female_01_entity" clothes_selector = "humanoid_master_female_clothes_01" hair_selector = "balosar_female_hair_01" greeting_sound = "human_female_greetings_03" }
	balosar_female_02 = { entity = "portrait_human_female_04_entity" clothes_selector = "humanoid_master_female_clothes_01" hair_selector = "balosar_female_hair_01" greeting_sound = "human_female_greetings_01" }
	balosar_male_01 = { entity = "portrait_human_male_01_entity" clothes_selector = "humanoid_master_male_clothes_01" hair_selector = "balosar_male_hair_01" greeting_sound = "human_male_greetings_03" }
	balosar_male_02 = { entity = "portrait_human_male_04_entity" clothes_selector = "humanoid_master_male_clothes_01" hair_selector = "balosar_male_hair_01" greeting_sound = "human_male_greetings_01" }
}

portrait_groups = {
	balosar = {
		default = balosar_female_02
		game_setup = { #will run with a limited country scope. species and government is set but the country does not actually exist
			add = {
				trigger = {
					ruler = { gender = male }
				}
				portraits = {
					balosar_male_01
					balosar_male_02
				}
			}
			add = {
				trigger = {
					ruler = { gender = female }
				}
				portraits = {
					balosar_female_01
					balosar_female_02
				}
			}
		}		
		
		#species scope
		species = { #generic portrait for a species
			add = {
				portraits = {
					balosar_female_01
					balosar_female_02
					balosar_male_01
					balosar_male_02
				}
			}
		}		
		
		#pop scope
		pop = { #for a specific pop
			add = {
				portraits = {
					balosar_female_01
					balosar_female_02
					balosar_male_01
					balosar_male_02
				}
			}
		}
		
		#leader scope
		leader = { #scientists, generals, admirals, governor
			add = {
				trigger = {
					gender = female
				}
				portraits = {
					balosar_female_01
					balosar_female_02
				}
			}
			add = {
				trigger = {
					gender = male
				}
				portraits = {
					balosar_male_01
					balosar_male_02
				}
			}
		}

		#leader scope 
		ruler = {
			add = {
				trigger = {
					gender = female
				}
				portraits = {
					balosar_female_01
					balosar_female_02
				}
			}
			add = {
				trigger = {
					gender = male
				}
				portraits = {
					balosar_male_01
					balosar_male_02
				}
			}
		}
	}
}