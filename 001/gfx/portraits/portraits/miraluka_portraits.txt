##############################################################
###	PORTRAITS SETTINGS
###
### Which portraits are used is set in 
### common\species_classes\00_species_classes.txt
###
###	This file configures how portraits and planet backgrounds are built.
###	Default position orientation is lower left.
##############################################################

portraits = {
	# miraluka
	miraluka_female_01 = { entity = "portrait_human_female_01_entity" clothes_selector = "humanoid_master_female_clothes_01" hair_selector = "humanoid_master_female_hair_01" greeting_sound = "human_female_greetings_03" 
		character_textures = {
			"gfx/models/portraits/miraluka/miraluka_female_body_01.dds"
			"gfx/models/portraits/miraluka/miraluka_female_body_02.dds"
			"gfx/models/portraits/miraluka/miraluka_female_body_03.dds"
			"gfx/models/portraits/miraluka/miraluka_female_body_04.dds"
			"gfx/models/portraits/miraluka/miraluka_female_body_05.dds"
		}
	}
	miraluka_male_01 = { entity = "portrait_human_male_01_entity" clothes_selector = "humanoid_master_male_clothes_01" hair_selector = "humanoid_master_male_hair_01" greeting_sound = "human_male_greetings_03" 
		character_textures = {
			"gfx/models/portraits/miraluka/miraluka_male_body_01.dds"
			"gfx/models/portraits/miraluka/miraluka_male_body_02.dds"
			"gfx/models/portraits/miraluka/miraluka_male_body_03.dds"
			"gfx/models/portraits/miraluka/miraluka_male_body_04.dds"
			"gfx/models/portraits/miraluka/miraluka_male_body_05.dds"
		}
	}
}

portrait_groups = {
	miraluka = {
		default = miraluka_female_01
		game_setup = { #will run with a limited country scope. species and government is set but the country does not actually exist
			add = {
				trigger = {
					ruler = { gender = male }
				}
				portraits = {
					miraluka_male_01
				}
			}
			add = {
				trigger = {
					ruler = { gender = female }
				}
				portraits = {
					miraluka_female_01
				}
			}
			#set = {
			#	trigger = { ... }
			#	portraits = { ... }
			#	#using "set =" instead of "add" will first clear any portraits already added
			#}
		}		
		
		#species scope
		species = { #generic portrait for a species
			add = {
				portraits = {
					miraluka_female_01
					miraluka_male_01
				}
			}
		}		
		
		#pop scope
		pop = { #for a specific pop
			add = {
				portraits = {
					miraluka_female_01
					miraluka_male_01
				}
			}
		}
		
		#leader scope
		leader = { #scientists, generals, admirals, governor
			add = {
				trigger = {
					gender = female
				}
				portraits = {
					miraluka_female_01
				}
			}
			add = {
				trigger = {
					gender = male
				}
				portraits = {
					miraluka_male_01
				}
			}
		}

		#leader scope 
		ruler = {
			add = {
				trigger = {
					gender = female
				}
				portraits = {
					miraluka_female_01
				}
			}
			add = {
				trigger = {
					gender = male
				}
				portraits = {
					miraluka_male_01
				}
			}
		}
	}
}