##############################################################
###	PORTRAITS SETTINGS
###
### Which portraits are used is set in 
### common\species_classes\00_species_classes.txt
###
###	This file configures how portraits and planet backgrounds are built.
###	Default position orientation is lower left.
##############################################################

portraits = {
	# peroenian
	peroenian_female_01 = { entity = "portrait_human_female_01_entity" clothes_selector = "humanoid_master_female_clothes_01" hair_selector = "humanoid_master_female_hair_01" greeting_sound = "human_female_greetings_03" }
	peroenian_female_02 = { entity = "portrait_human_female_03_entity" clothes_selector = "humanoid_master_female_clothes_01" hair_selector = "humanoid_master_female_hair_01" greeting_sound = "human_female_greetings_01" }
	peroenian_male_01 = { entity = "portrait_human_male_01_entity" clothes_selector = "humanoid_master_male_clothes_01" hair_selector = "humanoid_master_male_hair_01" greeting_sound = "human_male_greetings_03" }	
	peroenian_male_02 = { entity = "portrait_human_male_03_entity" clothes_selector = "humanoid_master_male_clothes_01" hair_selector = "humanoid_master_male_hair_01" greeting_sound = "human_male_greetings_01" }
}

portrait_groups = {
	peroenian = {
		default = peroenian_male_01
		game_setup = { #will run with a limited country scope. species and government is set but the country does not actually exist
			add = {
				trigger = {
					ruler = { gender = male }
				}
				portraits = {
					peroenian_male_01
					peroenian_male_02
				}
			}
			add = {
				trigger = {
					ruler = { gender = female }
				}
				portraits = {
					peroenian_female_01
					peroenian_female_02
				}
			}
			#set = {
			#	trigger = { ... }
			#	portraits = { ... }
			#	#using "set =" instead of "add" will first clear any portraits already added
			#}
		}		
		
		#species scope
		species = { #generic portrait for a species
			add = {
				portraits = {
					peroenian_male_01
					peroenian_male_02
					peroenian_female_01
					peroenian_female_02
				}
			}
		}		
		
		#pop scope
		pop = { #for a specific pop
			add = {
				portraits = {
					peroenian_female_01
					peroenian_female_02
					peroenian_male_01
					peroenian_male_02
				}
			}
		}
		
		#leader scope
		leader = { #scientists, generals, admirals, governor
			add = {
				trigger = {
					gender = female
				}
				portraits = {
					peroenian_female_01
					peroenian_female_02
				}
			}
			add = {
				trigger = {
					gender = male
				}
				portraits = {
					peroenian_male_01
					peroenian_male_02
				}
			}
		}

		#leader scope 
		ruler = {
			add = {
				trigger = {
					gender = female
				}
				portraits = {
					peroenian_female_01
					peroenian_female_02
				}
			}
			add = {
				trigger = {
					gender = male
				}
				portraits = {
					peroenian_male_01
					peroenian_male_02
				}
			}
		}
	}
}