##############################################################
###	PORTRAITS SETTINGS
###
### Which portraits are used is set in 
### common\species_classes\00_species_classes.txt
###
###	This file configures how portraits and planet backgrounds are built.
###	Default position orientation is lower left.
##############################################################

portraits = {
	# kiffar
	kiffar_female_01 = { entity = "portrait_human_female_03_entity" clothes_selector = "humanoid_master_female_clothes_01" hair_selector = "korun_female_hair_01" greeting_sound = "human_female_greetings_03"
		character_textures = {
			"gfx/models/portraits/kiffar/kiffar_female_body_01.dds"
			"gfx/models/portraits/kiffar/kiffar_female_body_02.dds"
		}
	}
	kiffar_male_01 = { entity = "portrait_human_male_03_entity" clothes_selector = "humanoid_master_male_clothes_01" hair_selector = "korun_male_hair_01" greeting_sound = "human_male_greetings_03"
		character_textures = {
			"gfx/models/portraits/kiffar/kiffar_male_body_01.dds"
			"gfx/models/portraits/kiffar/kiffar_male_body_02.dds"
		}
	}
}

portrait_groups = {
	kiffar = {
		default = kiffar_male_01
		game_setup = { #will run with a limited country scope. species and government is set but the country does not actually exist
			add = {
				trigger = {
					ruler = { gender = male }
				}
				portraits = {
					kiffar_male_01
				}
			}
			add = {
				trigger = {
					ruler = { gender = female }
				}
				portraits = {
					kiffar_female_01
				}
			}
			#set = {
			#	trigger = { ... }
			#	portraits = { ... }
			#	#using "set =" instead of "add" will first clear any portraits already added
			#}
		}		
		
		#species scope
		species = { #generic portrait for a species
			add = {
				portraits = {
					kiffar_female_01
					kiffar_male_01
				}
			}
		}		
		
		#pop scope
		pop = { #for a specific pop
			add = {
				portraits = {
					kiffar_female_01
					kiffar_male_01
				}
			}
		}
		
		#leader scope
		leader = { #scientists, generals, admirals, governor
			add = {
				trigger = {
					gender = female
				}
				portraits = {
					kiffar_female_01
				}
			}
			add = {
				trigger = {
					gender = male
				}
				portraits = {
					kiffar_male_01
				}
			}
		}

		#leader scope 
		ruler = {
			add = {
				trigger = {
					gender = female
				}
				portraits = {
					kiffar_female_01
				}
			}
			add = {
				trigger = {
					gender = male
				}
				portraits = {
					kiffar_male_01
				}
			}
		}
	}
}