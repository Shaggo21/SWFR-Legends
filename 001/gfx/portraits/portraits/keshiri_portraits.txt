##############################################################
###	PORTRAITS SETTINGS
###
### Which portraits are used is set in 
### common\species_classes\00_species_classes.txt
###
###	This file configures how portraits and planet backgrounds are built.
###	Default position orientation is lower left.
##############################################################

portraits = {
	# keshiri
	keshiri_female_01 = { entity = "portrait_human_female_01_entity" clothes_selector = "humanoid_master_female_clothes_01" hair_selector = "keshiri_female_hair_01" greeting_sound = "human_female_greetings_03" 
		character_textures = {
			"gfx/models/portraits/keshiri/keshiri_female_body_01.dds"
		}
	}
	keshiri_female_02 = { entity = "portrait_human_female_02_entity" clothes_selector = "humanoid_master_female_clothes_01" hair_selector = "keshiri_female_hair_01" greeting_sound = "human_female_greetings_05" 
		character_textures = {
			"gfx/models/portraits/keshiri/keshiri_female_body_02.dds"
		}
	}
	keshiri_female_03 = { entity = "portrait_human_female_03_entity" clothes_selector = "humanoid_master_female_clothes_01" hair_selector = "keshiri_female_hair_01" greeting_sound = "human_female_greetings_05" 
		character_textures = {
			"gfx/models/portraits/keshiri/keshiri_female_body_03.dds"
		}
	}
	keshiri_female_04 = { entity = "portrait_human_female_04_entity" clothes_selector = "humanoid_master_female_clothes_01" hair_selector = "keshiri_female_hair_01" greeting_sound = "human_female_greetings_01"
		character_textures = {
			"gfx/models/portraits/keshiri/keshiri_female_body_04.dds"
		}
	}
	keshiri_male_01 = { entity = "portrait_human_male_01_entity" clothes_selector = "humanoid_master_male_clothes_01" hair_selector = "keshiri_male_hair_01" greeting_sound = "human_male_greetings_03" 
		character_textures = {
			"gfx/models/portraits/keshiri/keshiri_male_body_01.dds"
		}
	}
	keshiri_male_02 = { entity = "portrait_human_male_02_entity" clothes_selector = "humanoid_master_male_clothes_01" hair_selector = "keshiri_male_hair_01" greeting_sound = "human_male_greetings_05" 
		character_textures = {
			"gfx/models/portraits/keshiri/keshiri_male_body_02.dds"
		}
	}
	keshiri_male_03 = { entity = "portrait_human_male_03_entity" clothes_selector = "humanoid_master_male_clothes_01" hair_selector = "keshiri_male_hair_01" greeting_sound = "human_male_greetings_05" 
		character_textures = {
			"gfx/models/portraits/keshiri/keshiri_male_body_03.dds"
		}
	}	
	keshiri_male_03 = { entity = "portrait_human_male_04_entity" clothes_selector = "humanoid_master_male_clothes_01" hair_selector = "keshiri_male_hair_01" greeting_sound = "human_male_greetings_01" 
		character_textures = {
			"gfx/models/portraits/keshiri/keshiri_male_body_04.dds"
		}
	}
}

portrait_groups = {
	keshiri = {
		default = keshiri_male_01
		game_setup = { #will run with a limited country scope. species and government is set but the country does not actually exist
			add = {
				trigger = {
					ruler = { gender = male }
				}
				portraits = {
					keshiri_male_01
					keshiri_male_02
					keshiri_male_03
				}
			}
			add = {
				trigger = {
					ruler = { gender = female }
				}
				portraits = {
					keshiri_female_01
					keshiri_female_02
					keshiri_female_03
				}
			}
			#set = {
			#	trigger = { ... }
			#	portraits = { ... }
			#	#using "set =" instead of "add" will first clear any portraits already added
			#}
		}		
		
		#species scope
		species = { #generic portrait for a species
			add = {
				portraits = {
					keshiri_male_01
					keshiri_male_02
					keshiri_male_03
					keshiri_female_01
					keshiri_female_02
					keshiri_female_03
				}
			}
		}		
		
		#pop scope
		pop = { #for a specific pop
			add = {
				portraits = {
					keshiri_female_01
					keshiri_female_02
					keshiri_female_03
					keshiri_male_01
					keshiri_male_02
					keshiri_male_03
				}
			}
		}
		
		#leader scope
		leader = { #scientists, generals, admirals, governor
			add = {
				trigger = {
					gender = female
				}
				portraits = {
					keshiri_female_01
					keshiri_female_02
					keshiri_female_03
				}
			}
			add = {
				trigger = {
					gender = male
				}
				portraits = {
					keshiri_male_01
					keshiri_male_02
					keshiri_male_03
				}
			}
		}

		#leader scope 
		ruler = {
			add = {
				trigger = {
					gender = female
				}
				portraits = {
					keshiri_female_01
					keshiri_female_02
					keshiri_female_03
				}
			}
			add = {
				trigger = {
					gender = male
				}
				portraits = {
					keshiri_male_01
					keshiri_male_02
					keshiri_male_03
				}
			}
		}
	}
}