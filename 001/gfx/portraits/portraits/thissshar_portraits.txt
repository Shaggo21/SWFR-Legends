##############################################################
###	PORTRAITS SETTINGS
###
### Which portraits are used is set in 
### common\species_classes\00_species_classes.txt
###
###	This file configures how portraits and planet backgrounds are built.
###	Default position orientation is lower left.
##############################################################

portraits = {
	tissshar = { entity = "humanoid_04_male_01_entity" clothes_selector = "no_texture" hair_selector = "no_texture" greeting_sound = "reptilian_01_greetings" 
		character_textures = {
			"gfx/models/portraits/tissshar/tissshar_male_body_01.dds"
			"gfx/models/portraits/tissshar/tissshar_male_body_02.dds"
			"gfx/models/portraits/tissshar/tissshar_male_body_03.dds"
		}
	}
}