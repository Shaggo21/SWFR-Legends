# This is a template which multiple species can use. 

koorivar_male_hair_01 = {
	default = "gfx/models/portraits/koorivar/humanoid_02_male_hair_style_koorivar_01.dds"
	
	game_setup = {#will run with a limited country scope. species and government is set but the country does not actually exist
		default = "gfx/models/portraits/koorivar/humanoid_02_male_hair_style_koorivar_01.dds"
	}
	#species scope
	species = { #generic portrait for a species
		default = "gfx/models/portraits/koorivar/humanoid_02_male_hair_style_koorivar_01.dds"
	}
	#pop scope
	pop = { #for a specific pop
		default = "gfx/models/portraits/koorivar/humanoid_02_male_hair_style_koorivar_01.dds"
		random = {
			list = {
				"gfx/models/portraits/koorivar/humanoid_02_male_hair_style_koorivar_01.dds" 
			}
		}
	}
}

koorivar_male_hair_02 = {
	default = "gfx/models/portraits/koorivar/humanoid_02_male_hair_style_koorivar_01_blue.dds"

	game_setup = {#will run with a limited country scope. species and government is set but the country does not actually exist
		default = "gfx/models/portraits/koorivar/humanoid_02_male_hair_style_koorivar_01_blue.dds"
	}
	#species scope
	species = { #generic portrait for a species
		default = "gfx/models/portraits/koorivar/humanoid_02_male_hair_style_koorivar_01_blue.dds"
	}
	#pop scope
	pop = { #for a specific pop
		default = "gfx/models/portraits/koorivar/humanoid_02_male_hair_style_koorivar_01_blue.dds"
		random = {
			list = {
				"gfx/models/portraits/koorivar/humanoid_02_male_hair_style_koorivar_01_blue.dds"
			}
		}
	}
}

koorivar_male_hair_03 = {
	default = "gfx/models/portraits/koorivar/humanoid_02_male_hair_style_koorivar_01_green.dds"
	
	game_setup = {#will run with a limited country scope. species and government is set but the country does not actually exist
		default = "gfx/models/portraits/koorivar/humanoid_02_male_hair_style_koorivar_01_green.dds"
	}
	#species scope
	species = { #generic portrait for a species
		default = "gfx/models/portraits/koorivar/humanoid_02_male_hair_style_koorivar_01_green.dds"
	}	
	#pop scope
	pop = { #for a specific pop
		default = "gfx/models/portraits/koorivar/humanoid_02_male_hair_style_koorivar_01_green.dds"
		random = {
			list = {
				"gfx/models/portraits/koorivar/humanoid_02_male_hair_style_koorivar_01_green.dds"
			}
		}
	}
}