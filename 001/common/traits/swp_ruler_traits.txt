#Palpatine
trait_ruler_swp_palpatine = {
	leader_trait = { ruler }
	leader_class = { ruler }

	cost = 0
	modification = no
	initial = no
	randomized = no
	
	icon = "gfx/interface/icons/traits/leader_traits/leader_trait_sith_lord.dds"
	
	immortal_leaders = yes	
	hide_age = yes
}

#Palpatines 2. Trait
trait_ruler_swp_emperor = {
	leader_trait = { ruler }
	leader_class = { ruler }
	self_modifier = {
	}
	modifier = {
		ship_weapon_damage = 0.025
		army_damage_mult = 0.025
		diplo_weight_mult = 0.05
		country_base_influence_produces_add = 1
	}
	cost = 0
	modification = no
	initial = no
	randomized = no
	
	icon = "gfx/interface/icons/traits/leader_traits/leader_trait_emperor.dds"	
	hide_age = yes
}

trait_ruler_swp_faction_head = {
	cost = 0
	icon = "gfx/interface/icons/traits/leader_traits/leader_trait_ruler_faction_head.dds"
	modification = no
	self_modifier = {
	}
	modifier = {
	}
	leader_trait = { admiral scientist general governor ruler } 
	leader_class = { admiral scientist general governor ruler }
	immortal_leaders = yes	
	hide_age = yes

	initial = no
	randomized = no
}