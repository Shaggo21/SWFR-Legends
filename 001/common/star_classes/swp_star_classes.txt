###
### Star Wars Fallen Republic Star Classes
###

# Yellow-White Binary
sc_desev = {
	class = g_star
	icon = a_binary_star_g_a
	planet = {
		key = pc_g_star
		class = g_star
	}
	planet = {
		key = pc_a_star
		class = a_star
	}
	spawn_odds = 0
}

# Yellow-Yellow Binary
sc_tatooine = {
	class = g_star
	icon = a_binary_star_g_g
	planet = {
		key = pc_g_star
		class = g_star
	}
	planet = {
		key = pc_g_star
		class = g_star
	}
	spawn_odds = 0
}

# Yellow-Yellow Binary
sc_antar = {
	class = g_star
	icon = a_binary_star_g_g_2
	planet = {
		key = pc_g_star
		class = g_star
	}
	planet = {
		key = pc_g_star
		class = g_star
	}
	spawn_odds = 0
}

# Red-Red Binary
sc_red_twins = {
	class = k_star
	icon = a_binary_star_k_k
	planet = {
		key = pc_k_star
		class = k_star
	}
	planet = {
		key = pc_k_star
		class = k_star
	}
	spawn_odds = 0
}

# Blue-White Binary
sc_molavar = {
	class = b_star
	icon = a_binary_star_b_a
	planet = {
		key = pc_b_star
		class = b_star
	}
	planet = {
		key = pc_a_star
		class = a_star
	}
	spawn_odds = 0
}

# Yellow-Blue Binary
sc_ossus = {
	class = g_star
	icon = a_binary_star_g_b
	planet = {
		key = pc_g_star
		class = g_star
	}
	planet = {
		key = pc_b_star
		class = b_star
	}
	spawn_odds = 0
}

# Yellow-Orange-Blue Trinary
sc_cerea = {
	class = g_star
	icon = a_trinary_star_g_k_b
	planet = {
		key = pc_g_star
		class = g_star
	}
	planet = {
		key = pc_k_star
		class = k_star
	}
	planet = {
		key = pc_b_star
		class = b_star
	}
	spawn_odds = 0
}

# Red-Yellow-Orange Trinary
sc_knel_char = {
	class = m_star
	icon = a_trinary_star
	planet = {
		key = pc_m_star
		class = m_star
	}
	planet = {
		key = pc_g_star
		class = g_star
	}
	planet = {
		key = pc_k_star
		class = k_star
	}
	spawn_odds = 0
}

# Yellow-Yellow-Orange Trinary
sc_riflor = {
	class = g_star
	icon = a_trinary_star_g_g_k
	planet = {
		key = pc_g_star
		class = g_star
	}
	planet = {
		key = pc_g_star
		class = g_star
	}
	planet = {
		key = pc_k_star
		class = k_star
	}
	spawn_odds = 0
}

# Yellow-Yellow-Orange Quaternary
sc_opiteihr = {
	class = m_star
	icon = a_quaternary_star_m_m_m_m
	planet = {
		key = pc_m_star
		class = m_star
	}
	planet = {
		key = pc_m_star
		class = m_star
	}
	planet = {
		key = pc_m_star
		class = m_star
	}
	planet = {
		key = pc_m_star
		class = m_star
	}
	spawn_odds = 0
}

sc_star_cluster = { # Quintenary System
	class = g_star
	icon = a_quintenary_star_g_g_k_k_m
	planet = {
		key = pc_g_star
		class = g_star
	}
	planet = {
		key = pc_k_star
		class = k_star
	}
	planet = {
		key = pc_m_star
		class = m_star
	}
	planet = {
		key = pc_k_star
		class = k_star
	}
	planet = {
		key = pc_g_star
		class = g_star
	}
	spawn_odds = 0
}

sc_collapsar = {
	class = collapsar
	planet = { key = pc_collapsar }
	spawn_odds = 0
	num_planets = { min = 1 max = 4 }

	icon_scale = 2.8
	
	pc_desert = { spawn_odds = 0 }
	pc_tropical = { spawn_odds = 0 }	
	pc_arid = {	spawn_odds = 0 }
	pc_continental = { spawn_odds = 0 }
	pc_ocean = { spawn_odds = 0 }
	pc_tundra = { spawn_odds = 0 }
	pc_arctic = { spawn_odds = 0 }
	pc_gaia = { spawn_odds = 0 }
	pc_alpine = { spawn_odds = 0 }
	pc_savannah = { spawn_odds = 0 }
	
	is_environmental_hazard = yes
	modifier = {
		ship_speed_reduction = 0.5
		ship_shield_reduction = 0.5
	}
}

sc_nebula_purple = {
	class = nebula_purple
	planet = {
		key = pc_nebula_purple
		class = pc_nebula_purple
	}
	spawn_odds = 0
	num_planets = { min = 1 max = 4 }
	
	icon_scale = 3.2
	
	pc_continental = { spawn_odds = 0.0 }
	pc_desert = { spawn_odds = 0.0 }
	pc_tropical = { spawn_odds = 0.0 }
	pc_arid = { spawn_odds = 0.0 }
	pc_ocean = { spawn_odds = 0.0 }
	pc_tundra = { spawn_odds = 0.0 }
	pc_arctic = { spawn_odds = 0.1 }
	pc_alpine = { spawn_odds = 0.0 }
	pc_savannah = { spawn_odds = 0.0 }
	
	is_environmental_hazard = yes
	modifier = {
		ship_speed_reduction = 0.25
		ship_shield_reduction = 0.25
	}
}

sc_nebula_green = {
	class = nebula_green
	planet = {
		key = pc_nebula_green
		class = pc_nebula_green
	}
	spawn_odds = 0
	num_planets = { min = 1 max = 4 }
	
	icon_scale = 3.2
	
	pc_continental = { spawn_odds = 0.0 }
	pc_desert = { spawn_odds = 0.0 }
	pc_tropical = { spawn_odds = 0.0 }
	pc_arid = { spawn_odds = 0.0 }
	pc_ocean = { spawn_odds = 0.0 }
	pc_tundra = { spawn_odds = 0.0 }
	pc_arctic = { spawn_odds = 0.1 }
	pc_alpine = { spawn_odds = 0.0 }
	pc_savannah = { spawn_odds = 0.0 }
	
	is_environmental_hazard = yes
	modifier = {
 		ship_speed_reduction = 0.25
		ship_shield_reduction = 0.25
	}
}

sc_nebula_red = {
	class = nebula_red
	planet = {
		key = pc_nebula_red
		class = pc_nebula_red
	}
	spawn_odds = 0
	num_planets = { min = 1 max = 4 }
	
	icon_scale = 3.2
	
	pc_continental = { spawn_odds = 0.0 }
	pc_desert = { spawn_odds = 0.0 }
	pc_tropical = { spawn_odds = 0.0 }
	pc_arid = { spawn_odds = 0.0 }
	pc_ocean = { spawn_odds = 0.0 }
	pc_tundra = { spawn_odds = 0.0 }
	pc_arctic = { spawn_odds = 0.1 }
	pc_alpine = { spawn_odds = 0.0 }
	pc_savannah = { spawn_odds = 0.0 }

	is_environmental_hazard = yes
	modifier = {
 		ship_speed_reduction = 0.25
		ship_shield_reduction = 0.25
	}
}

# Nova 1
sc_nova_1 = {
	class 			= nova_1
	planet			= { key = pc_nova_1 }
	spawn_odds 		= 1.2
	num_planets 	= { min = 1 max = 4 }

	icon_scale		= 1.65
	
	pc_continental 	= { spawn_odds = 0.0 }
	pc_desert		= { spawn_odds = 0.1 }
	pc_tropical		= { spawn_odds = 0.0 }
	pc_arid 		= { spawn_odds = 0.1 }
	pc_ocean		= { spawn_odds = 0.0 }
	pc_tundra		= { spawn_odds = 0.0 }
	pc_arctic		= { spawn_odds = 0.0 }
	pc_alpine		= { spawn_odds = 0.0 }
	pc_savannah		= { spawn_odds = 0.0 }
	pc_gaia			= { spawn_odds = 0.0 }
	
	is_environmental_hazard = yes
	modifier = {
		ship_weapon_range_mult = -0.15
		ship_fire_rate_mult = -0.25
		ship_evasion_mult = -0.25
	}	
}

# Nova 2
sc_nova_2 = {
	class 			= nova_2
	planet			= { key = pc_nova_2 }
	spawn_odds 		= 1.2
	num_planets 	= { min = 1 max = 4 }

	icon_scale		= 1.65
	
	pc_continental 	= { spawn_odds = 0.0 }
	pc_desert		= { spawn_odds = 0.1 }
	pc_tropical		= { spawn_odds = 0.0 }
	pc_arid 		= { spawn_odds = 0.1 }
	pc_ocean		= { spawn_odds = 0.0 }
	pc_tundra		= { spawn_odds = 0.0 }
	pc_arctic		= { spawn_odds = 0.0 }
	pc_alpine		= { spawn_odds = 0.0 }
	pc_savannah		= { spawn_odds = 0.0 }
	pc_gaia			= { spawn_odds = 0.0 }
	
	is_environmental_hazard = yes
	modifier = {
		ship_weapon_range_mult = -0.15
		ship_fire_rate_mult = -0.25
		ship_evasion_mult = -0.25
	}
}

# Protostar
sc_protostar = {
	class 			= protostar
	planet			= { key = pc_protostar }
	spawn_odds 		= 0.9
	num_planets 	= { min = 1 max = 4 }

	icon_scale		= 2.5
	
	pc_desert 		= { spawn_odds = 0 }
	pc_tropical 	= { spawn_odds = 0 }	
	pc_arid 		= {	spawn_odds = 0 }
	pc_continental 	= { spawn_odds = 0 }
	pc_ocean 		= { spawn_odds = 0 }
	pc_tundra 		= { spawn_odds = 0 }
	pc_arctic 		= { spawn_odds = 0 }
	pc_gaia			= { spawn_odds = 0 }
	pc_alpine		= { spawn_odds = 0 }
	pc_savannah		= { spawn_odds = 0 }

	#is_environmental_hazard = no
	#modifier = {
	#	tile_resource_minerals_mult = 0.25
	#}
}

# Magnetar
sc_magnetar = {
	class 			= magnetar
	planet			= { key = pc_magnetar }
	spawn_odds 		= 1.2
	num_planets 	= { min = 1 max = 4 }

	icon_scale		= 1.1
	
	pc_desert 		= { spawn_odds = 0 }
	pc_tropical 	= { spawn_odds = 0 }	
	pc_arid 		= {	spawn_odds = 0 }
	pc_continental 	= { spawn_odds = 0 }
	pc_ocean 		= { spawn_odds = 0 }
	pc_tundra 		= { spawn_odds = 0 }
	pc_arctic 		= { spawn_odds = 0 }
	pc_gaia			= { spawn_odds = 0 }
	pc_alpine		= { spawn_odds = 0 }
	pc_savannah		= { spawn_odds = 0 }
	
	is_environmental_hazard = yes
	modifier = {
		ship_speed_reduction = 0.7
		ship_shield_reduction = 1.0
	}
}

random_list = {
	name = "rl_credit_stars"
	stars = {
		"sc_a"
		"sc_b"
		"sc_f"
		"sc_g"
		"sc_k"
		"sc_m"
		"sc_m_giant"
		"sc_t"
		"sc_neutron_star"
		"sc_pulsar"
		"sc_magnetar"
	}
}