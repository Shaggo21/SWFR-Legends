create_imperial_defectors_pirate_country = {
	event_target:pirate_system = {
		random_system_planet = {
			save_event_target_as = pirate_home
			create_country = {
				name = "NAME_Pirates"
				type = pirate
				species = event_target:owner_species
				name_list = "PRT1"
				flag = {
					background = {
						category = "backgrounds"
						file = "00_solid.dds"
					}
					colors ={
						"black"
						"black"
						"null"
						"null"
					}
				}
				effect = {
					save_event_target_as = pirate_band
				}
			}
			event_target:pirate_band = {
				set_name = "The Pure Empire"
				randomize_flag_symbol = "pirate"
				every_country = {
					establish_communications_no_message = prev
				}
			}
		}
	}
}

create_imperial_defectors_fleet = {
	event_target:pirate_band = {
		create_fleet = {
			name = "NAME_Pirate_Fleet"
			effect = {
				set_owner = event_target:pirate_band
				create_imperial_defectors_ships = yes
				set_leader = imperial_defector
				set_location = event_target:pirate_home.star
				set_fleet_stance = aggressive
				set_aggro_range_measure_from = self
				set_aggro_range = 8000
				set_fleet_flag = imperial_defectors_fleet
			}
		}
	}
}

create_imperial_criminals_fleet = {
	event_target:pirate_band = {
		create_fleet = {
			name = "NAME_Pirate_Fleet"
			effect = {
				set_owner = event_target:pirate_band
				create_imperial_criminals_ships = yes
				set_location = event_target:pirate_home.star
				set_fleet_stance = aggressive
				set_aggro_range_measure_from = self
				set_aggro_range = 8000
			}
		}
	}
}

create_cantina_tales_pirate_fleet = {
	event_target:pirate_band = {
		set_name = "Bane's Wrath"
		create_fleet = {
			name = "NAME_Pirate_Fleet"
			effect = {
				set_owner = event_target:pirate_band
				create_ship = {
					name = random
					design = NAME_Pirate_Providence
					graphical_culture = misc_01
				}
				set_location = event_target:pirate_home.star
				set_fleet_stance = aggressive
				set_aggro_range_measure_from = self
				set_aggro_range = 8000
			}
		}
	}
}

create_imperial_defectors_ships = {
	while = {
		count = 2
		create_ship = {
			name = random
			design = NAME_Event_Imperial_Virgil_Corvette_Tier1_Tier2
		}
	}
	create_ship = {
		name = random
		design = NAME_Event_Imperial_VictoryII_Cruiser_Tier1_Tier2
		effect = {
			set_ship_flag = traitor_ship
			set_disable_at_health = 0.05
		}
	}
}

create_imperial_criminals_ships = {
	while = {
		count = 5
		create_ship = {
			name = random
			design = NAME_Event_Imperial_Virgil_Corvette_Tier1_Tier2
		}
	}
}