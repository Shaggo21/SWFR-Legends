### Light Guns
swp_default_light_turret = {
	size = point_defence
	component = weapon
}

cr90_single_anti_ship = {
	size = point_defence
	component = weapon
}

cr90_single_point_defence = {
	size = point_defence
	component = weapon
}

afql_point_defence_turret = {
	size = point_defence
	component = weapon
}

rh8_point_defence_turret = {
	size = point_defence
	component = weapon
}

lthlc_point_defence_turret = {
	size = point_defence
	component = weapon
}

dltl_point_defence_turret = {
	size = point_defence
	component = weapon
}

### Medium Guns
swp_default_medium_turret = {
	size = small
	component = weapon
}

cr90_dual_medium_turret = {
	size = small
	component = weapon
}

dltl_medium_turret = {
	size = small
	component = weapon
}

xi7_medium_turret = {
	size = small
	component = weapon
}

mqtl_medium_turret = {
	size = small
	component = weapon
}

mdtlv_medium_turret = {
	size = small
	component = weapon
}

mttl_medium_turret = {
	size = small
	component = weapon
}

dmd_medium_turret = {
	size = small
	component = weapon
}

dby827_medium_turret = {
	size = small
	component = weapon
}

hdtl_medium_turret = {
	size = small
	component = weapon
}

### Large Guns
swp_default_large_turret = {
	size = medium
	component = weapon
}

mdtl_large_turret = {
	size = medium
	component = weapon
}

mqtl_large_turret = {
	size = medium
	component = weapon
}

mlrtl_large_turret = {
	size = medium
	component = weapon
}

dby827_large_turret = {
	size = medium
	component = weapon
}

hdtl_large_turret = {
	size = medium
	component = weapon
}

hqtl_otl_large_turret = {
	size = medium
	component = weapon
}

lqtl_large_turret = {
	size = medium
	component = weapon
}

qbtl_large_turret = {
	size = medium
	component = weapon
}

dmd_large_turret = {
	size = medium
	component = weapon
}

### Ion Guns
swp_default_ion_turret = {
	size = extra_large
	component = weapon
}

nk7_ion_turret = {
	size = extra_large
	component = weapon
}

nk7_isd_ion_turret = {
	size = extra_large
	component = weapon
}

mhic_ion_turret = {
	size = extra_large
	component = weapon
}

### Missile Slots
swp_missile_slot = {
	size = torpedo
	component = weapon
}

swp_torpedo_slot = {
	size = torpedo
	component = weapon
}

### Vong Slots
swp_light_yarek_kor_slot = {
	size = point_defence
	component = weapon
}

swp_light_beam_slot = {
	size = point_defence
	component = weapon
}

swp_crystal_launcher_slot = {
	size = point_defence
	component = weapon
}

swp_medium_yarek_kor_slot = {
	size = small
	component = weapon
}

swp_medium_beam_slot = {
	size = small
	component = weapon
}

swp_heavy_yarek_kor_slot = {
	size = medium
	component = weapon
}

swp_heavy_beam_slot = {
	size = medium
	component = weapon
}

swp_melting_beam_slot = {
	size = large
	component = weapon
}

swp_grutchin_pod_slot = {
	size = large
	component = weapon
}

swp_spore_launcher_slot = {
	size = torpedo
	component = weapon
}

swp_rock_spitter_slot = {
	size = torpedo
	component = weapon
}