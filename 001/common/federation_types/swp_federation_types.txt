### Coalition
swp_coalition_federation = {
	icon = "GFX_swp_coalition_federation"
	potential = { always = yes  }
	allow = { 
		always = yes
	#    custom_tooltip = { 
	# 	  fail_text = requires_actor_not_barbaric_despoilers
	# 	  NOT = { has_valid_civic = civic_barbaric_despoilers }
	#    }
	}
	levels = {
	   level_1 = {
		  experience = 1200
		  perks = { default_federation_passive }
	   }
	   level_2 = {
		  experience = 2400
		  perks = { swp_envoy_unity_1 diplomacy_upkeep_1 cohesion_failed_vote_1 }
	   }
	   level_3 = {
		  experience = 4800
		  perks = { unity_boost_1 extra_envoy_1 swp_naval_cap_1 }
	   }
	   level_4 = {
		  experience = 9600
		  perks = { swp_envoy_unity_2 swp_diplo_weight_1 trade_protection_1 }
	   }
	   level_5 = {
		  perks = { unity_boost_2 swp_fleet_upkeep_1 extra_influence_1 }
	   }
	}
	on_create = {
	   remove_federation_flag = enable_federation_cooldowns
	   set_federation_law = centralization_minimal
	   set_federation_law = fleet_contribution_none
	   set_federation_law = succession_type_rotation
	   set_federation_law = succession_term_years_20
	   set_federation_law = allow_subjects_to_join_no
	   set_federation_law = vote_weight_equal
	   set_federation_law = declare_war_unanimous_vote
	   set_federation_law = invite_members_unanimous_vote
	   set_federation_law = kick_members_majority_vote
	   set_federation_law = free_migration_no
	   set_federation_law = treaties_separate_yes
	   set_federation_law = annexation_no
	}
	ai_weight = {
	   base = 0
	   modifier = {
		  desc = alert_federation_low_cohesion_title
		  add = -100
		  from = { has_federation = yes } exists = federation federation = { federation_cohesion <= -50 }
	   }
	   modifier = {
		  desc = alert_federation_low_cohesion_title
		  add = -50
		  from = { has_federation = yes } exists = federation federation = { federation_cohesion > -50 federation_cohesion <= 0 }
	   }
	   modifier = {
		  desc = COHESION_LABEL
		  add = 30
		  from = { has_federation = yes } exists = federation federation = { federation_cohesion >= 80 }
	   }
	   modifier = {
		  desc = federation_acceptance_compact
		  add = 20
		  from = { has_federation = yes NOT = { relative_power = { who = prev value > equivalent } } } exists = federation federation = { has_federation_type = swp_compact_federation }
	   }
	   modifier = {
		  desc = federation_acceptance_compact
		  add = 50
		  from = { has_federation = yes relative_power = { who = prev value > equivalent } } exists = federation federation = { has_federation_type = swp_compact_federation }
	   }
	   modifier = {
		  desc = federation_acceptance_swp_imperial_loyalists
		  add = -20
		  from = { OR = { has_ai_personality = galactic_imperialists has_ai_personality = imperial_loyalists } }
	   }
	   modifier = {
		  desc = federation_acceptance_swp_isolationists
		  add = -50
		  from = { OR = { has_ai_personality = militant_isolationists has_ai_personality = democratic_isolationists has_ai_personality = enigmatic_isolationists has_ai_personality = spiritual_isolationists has_ai_personality = hivemind_isolationists has_ai_personality = hutt_clans } }
	   }
	   modifier = {
		  desc = federation_acceptance_sw_rebels
		  add = 20
		  from = { OR = { has_ai_personality = republic_loyalists has_ai_personality = freedom_fighters } }
	   }
	   modifier = {
		  desc = federation_acceptance_sw_monarchists
		  add = 10
		  from = { has_ai_personality = monarchists }
	   }
	   modifier = {
		  desc = federation_acceptance_sw_mando_pacifists
		  add = 20
		  from = { has_ai_personality = mandalorian_pacifists }
	   }
	   modifier = {
		  desc = federation_acceptance_sw_corporatists
		  add = -10
		  from = { has_ai_personality = greedy_corporatists }
	   }
	   modifier = {
		  desc = federation_acceptance_sw_security_forces
		  add = 10
		  from = { has_ai_personality = security_forces }
	   }
	}
 }
 
 
### Compact
 swp_compact_federation = {
	icon = "GFX_swp_compact_federation"
	potential = { NOT = { is_galactic_empire = yes } }
	allow = { 
	   OR = { is_authoritarian = yes is_militarist = yes }
	}
	levels = {
	   level_1 = {
		  experience = 1200
		  perks = { compact_federation_passive }
	   }
	   level_2 = {
		  experience = 2400
		  perks = { swp_leader_job_output_1 president_diplo_weight_steal_1 president_hegemony_cb_1 }
	   }
	   level_3 = {
		  experience = 4800
		  perks = { swp_leader_job_output_2 swp_fleet_upkeep_1 swp_federation_fire_rate_1 }
	   }
	   level_4 = {
		  experience = 9600
		  perks = { swp_leader_job_output_3 admiral_level_cap_1 cohesion_join_1 }
	   }
	   level_5 = {
		  perks = { swp_leader_naval_cap_1 claim_cost_1 extra_influence_1 }
	   }
	}
	on_create = {
	   remove_federation_flag = enable_federation_cooldowns
	   set_federation_law = centralization_minimal
	   set_federation_law = fleet_contribution_low_compact
	   set_federation_law = succession_type_strongest_hegemony
	   set_federation_law = succession_term_status_change
	   set_federation_law = allow_subjects_to_join_no
	   set_federation_law = vote_weight_equal
	   set_federation_law = declare_war_majority_vote_hegemony
	   set_federation_law = invite_members_president_vote_hegemony
	   set_federation_law = kick_members_president_vote_hegemony
	   set_federation_law = free_migration_no
	   set_federation_law = treaties_separate_yes
	   set_federation_law = annexation_no
	}
	ai_weight = {
	   base = 0
	   modifier = {
		  desc = alert_federation_low_cohesion_title
		  add = -100
		  from = { has_federation = yes } exists = federation federation = { federation_cohesion <= -50 }
	   }
	   modifier = {
		  desc = alert_federation_low_cohesion_title
		  add = -50
		  from = { has_federation = yes } exists = federation federation = { federation_cohesion > -50 federation_cohesion <= 0 }
	   }
	   modifier = {
		  desc = COHESION_LABEL
		  add = 30
		  from = { has_federation = yes } exists = federation federation = { federation_cohesion >= 80 }
	   }
	   modifier = {
		  desc = federation_acceptance_compact
		  add = 20
		  from = { has_federation = yes NOT = { relative_power = { who = prev value > equivalent } } } exists = federation federation = { has_federation_type = swp_compact_federation }
	   }
	   modifier = {
		  desc = federation_acceptance_compact
		  add = 50
		  from = { has_federation = yes relative_power = { who = prev value > equivalent } } exists = federation federation = { has_federation_type = swp_compact_federation }
	   }
	   modifier = {
		  desc = federation_acceptance_swp_imperial_loyalists
		  add = -20
		  from = { OR = { has_ai_personality = galactic_imperialists has_ai_personality = imperial_loyalists } }
	   }
	   modifier = {
		  desc = federation_acceptance_swp_isolationists
		  add = -50
		  from = { OR = { has_ai_personality = militant_isolationists has_ai_personality = democratic_isolationists has_ai_personality = enigmatic_isolationists has_ai_personality = spiritual_isolationists has_ai_personality = hivemind_isolationists has_ai_personality = hutt_clans } }
	   }
	   modifier = {
		  desc = federation_acceptance_sw_warriors
		  add = 10
		  from = { OR = { has_ai_personality = mandalorian_warriors has_ai_personality = echani_warriors has_ai_personality = militant_slavers } }
	   }
	   modifier = {
		  desc = federation_acceptance_sw_mando_pacifists
		  add = -50
		  from = { has_ai_personality = mandalorian_pacifists }
	   }
	   modifier = {
		  desc = federation_acceptance_sw_corporatists
		  add = -10
		  from = { has_ai_personality = greedy_corporatists }
	   }
	}
 }
 
 
### Exchange
 swp_exchange_federation = {
	icon = "GFX_swp_exchange_federation"
	potential = { always = yes }
	allow = { 
	   custom_tooltip = { 
		  fail_text = requires_actor_not_barbaric_despoilers
		  NOT = { has_valid_civic = civic_barbaric_despoilers }
	   }
	   is_materialist = yes
	}
	levels = {
	   level_1 = {
		  experience = 1200
		  perks = { exchange_federation_passive }
	   }
	   level_2 = {
		  experience = 2400
		  perks = { trade_value_1 research_share_1 tech_diplo_weight_1 }
	   }
	   level_3 = {
		  experience = 4800
		  perks = { trade_value_2 research_share_2 rare_tech_boost }
	   }
	   level_4 = {
		  experience = 9600
		  perks = { trade_value_3 research_share_3 research_boost_1 }
	   }
	   level_5 = {
		  perks = { trade_value_4 research_share_4 president_option_1 }
	   }
	}
	on_create = {
	   remove_federation_flag = enable_federation_cooldowns
	   set_federation_law = centralization_minimal
	   set_federation_law = fleet_contribution_none
	   set_federation_law = succession_type_rotation
	   set_federation_law = succession_term_years_20
	   set_federation_law = allow_subjects_to_join_no
	   set_federation_law = vote_weight_equal
	   set_federation_law = declare_war_unanimous_vote
	   set_federation_law = invite_members_unanimous_vote
	   set_federation_law = kick_members_majority_vote
	   set_federation_law = free_migration_no
	   set_federation_law = treaties_separate_yes
	   set_federation_law = annexation_no
	}
	ai_weight = {
	   base = 0
	   modifier = {
		  desc = alert_federation_low_cohesion_title
		  add = -100
		  from = { has_federation = yes } exists = federation federation = { federation_cohesion <= -50 }
	   }
	   modifier = {
		  desc = alert_federation_low_cohesion_title
		  add = -50
		  from = { has_federation = yes } exists = federation federation = { federation_cohesion > -50 federation_cohesion <= 0 }
	   }
	   modifier = {
		  desc = COHESION_LABEL
		  add = 30
		  from = { has_federation = yes } exists = federation federation = { federation_cohesion >= 80 }
	   }
	   modifier = {
		  desc = federation_acceptance_compact
		  add = 20
		  from = { has_federation = yes NOT = { relative_power = { who = prev value > equivalent } } } exists = federation federation = { has_federation_type = swp_compact_federation }
	   }
	   modifier = {
		  desc = federation_acceptance_compact
		  add = 50
		  from = { has_federation = yes relative_power = { who = prev value > equivalent } } exists = federation federation = { has_federation_type = swp_compact_federation }
	   }
	   modifier = {
		  desc = federation_acceptance_swp_imperial_loyalists
		  add = -20
		  from = { OR = { has_ai_personality = galactic_imperialists has_ai_personality = imperial_loyalists } }
	   }
	   modifier = {
		  desc = federation_acceptance_swp_isolationists
		  add = -50
		  from = { OR = { has_ai_personality = militant_isolationists has_ai_personality = democratic_isolationists has_ai_personality = enigmatic_isolationists has_ai_personality = spiritual_isolationists has_ai_personality = hivemind_isolationists has_ai_personality = hutt_clans } }
	   }
	   modifier = {
		  desc = federation_acceptance_sw_corporatists
		  add = 30
		  from = { has_ai_personality = greedy_corporatists }
	   }
	   modifier = {
		  desc = federation_acceptance_sw_separatists
		  add = 10
		  from = { has_ai_personality = separatists }
	   }
	}
 }
 
 
### Cartel
 swp_cartel_federation = {
	icon = "GFX_swp_cartel_federation"
	potential = { always = yes }
	allow = { 
	   custom_tooltip = { 
		  fail_text = requires_actor_not_barbaric_despoilers
		  NOT = { has_valid_civic = civic_barbaric_despoilers }
	   }
	   custom_tooltip = { 
		  fail_text = requires_criminal_or_hutts
		  OR = { is_criminal_syndicate = yes has_country_flag = hutt_cartel }
	   }
	}
	levels = {
	   level_1 = {
		  experience = 1200
		  perks = { cartel_federation_passive }
	   }
	   level_2 = {
		  experience = 2400
		  perks = { trade_value_1 swp_fed_job_output_1 cohesion_join_1 }
	   }
	   level_3 = {
		  experience = 4800
		  perks = { trade_value_2 research_share_1 extra_influence_1 }
	   }
	   level_4 = {
		  experience = 9600
		  perks = { trade_value_3 swp_fed_job_output_2 diplomacy_upkeep_1 }
	   }
	   level_5 = {
		  perks = { trade_value_4 research_share_2 swp_federation_fire_rate_1 }
	   }
	}
	on_create = {
	   remove_federation_flag = enable_federation_cooldowns
	   set_federation_law = centralization_minimal
	   set_federation_law = fleet_contribution_none
	   set_federation_law = succession_type_strongest_hegemony
	   set_federation_law = succession_term_status_change
	   set_federation_law = allow_subjects_to_join_no
	   set_federation_law = vote_weight_equal
	   set_federation_law = declare_war_unanimous_vote
	   set_federation_law = invite_members_unanimous_vote
	   set_federation_law = kick_members_majority_vote
	   set_federation_law = free_migration_no
	   set_federation_law = treaties_separate_yes
	   set_federation_law = annexation_no
	}
	ai_weight = {
	   base = 0
	   modifier = {
		  desc = alert_federation_low_cohesion_title
		  add = -100
		  from = { has_federation = yes } exists = federation federation = { federation_cohesion <= -50 }
	   }
	   modifier = {
		  desc = alert_federation_low_cohesion_title
		  add = -50
		  from = { has_federation = yes } exists = federation federation = { federation_cohesion > -50 federation_cohesion <= 0 }
	   }
	   modifier = {
		  desc = COHESION_LABEL
		  add = 30
		  from = { has_federation = yes } exists = federation federation = { federation_cohesion >= 80 }
	   }
	   modifier = {
		  desc = federation_acceptance_compact
		  add = 20
		  from = { has_federation = yes NOT = { relative_power = { who = prev value > equivalent } } } exists = federation federation = { has_federation_type = swp_compact_federation }
	   }
	   modifier = {
		  desc = federation_acceptance_compact
		  add = 50
		  from = { has_federation = yes relative_power = { who = prev value > equivalent } } exists = federation federation = { has_federation_type = swp_compact_federation }
	   }
	   modifier = {
		  desc = federation_acceptance_swp_imperial_loyalists
		  add = -20
		  from = { OR = { has_ai_personality = galactic_imperialists has_ai_personality = imperial_loyalists } }
	   }
	   modifier = {
		  desc = federation_acceptance_swp_isolationists
		  add = -50
		  from = { OR = { has_ai_personality = militant_isolationists has_ai_personality = democratic_isolationists has_ai_personality = enigmatic_isolationists has_ai_personality = spiritual_isolationists has_ai_personality = hivemind_isolationists } }
	   }
	   modifier = {
		  desc = federation_acceptance_sw_criminal
		  add = 50
		  from = { OR = { has_ai_personality = hutt_clans has_ai_personality = illicit_traders } }
	   }
	   modifier = {
		  desc = federation_acceptance_sw_separatists
		  add = -20
		  from = { has_ai_personality = separatists }
	   }
	   modifier = {
		  desc = federation_acceptance_sw_corporatists
		  add = -10
		  from = { has_ai_personality = greedy_corporatists }
	   }
	}
 }
 
 
### Confederation
 swp_con_federation = {
	icon = "GFX_swp_con_federation"
	potential = { always = yes }
	allow = { 
	   custom_tooltip = { 
		  fail_text = requires_actor_not_barbaric_despoilers
		  NOT = { has_valid_civic = civic_barbaric_despoilers }
	   }
	   custom_tooltip = { 
		  fail_text = requires_confed_remnant
		  has_origin = origin_swp_confed_remnant
	   }
	}
	levels = {
	   level_1 = {
		  experience = 1200
		  perks = { con_federation_passive }
	   }
	   level_2 = {
		  experience = 2400
		  perks = { swp_leader_job_output_1 president_diplo_weight_steal_1 president_hegemony_cb_1 }
	   }
	   level_3 = {
		  experience = 4800
		  perks = { swp_leader_job_output_2 swp_fleet_upkeep_1 swp_leader_naval_cap_1 }
	   }
	   level_4 = {
		  experience = 9600
		  perks = { swp_leader_job_output_3 admiral_level_cap_1 swp_naval_cap_1 }
	   }
	   level_5 = {
		  perks = { claim_cost_1 extra_influence_1 swp_leader_naval_cap_2 }
	   }
	}
	on_create = {
	   remove_federation_flag = enable_federation_cooldowns
	   set_federation_law = centralization_minimal
	   set_federation_law = fleet_contribution_low_confed
	   set_federation_law = succession_type_strongest_hegemony
	   set_federation_law = succession_term_status_change
	   set_federation_law = allow_subjects_to_join_no
	   set_federation_law = vote_weight_equal
	   set_federation_law = declare_war_unanimous_vote
	   set_federation_law = invite_members_unanimous_vote
	   set_federation_law = kick_members_majority_vote
	   set_federation_law = free_migration_no
	   set_federation_law = treaties_separate_yes
	   set_federation_law = annexation_no
	}
	ai_weight = {
	   base = 0
	   modifier = {
		  desc = alert_federation_low_cohesion_title
		  add = -100
		  from = { has_federation = yes } exists = federation federation = { federation_cohesion <= -50 }
	   }
	   modifier = {
		  desc = alert_federation_low_cohesion_title
		  add = -50
		  from = { has_federation = yes } exists = federation federation = { federation_cohesion > -50 federation_cohesion <= 0 }
	   }
	   modifier = {
		  desc = COHESION_LABEL
		  add = 30
		  from = { has_federation = yes } exists = federation federation = { federation_cohesion >= 80 }
	   }
	   modifier = {
		  desc = federation_acceptance_compact
		  add = 20
		  from = { has_federation = yes NOT = { relative_power = { who = prev value > equivalent } } } exists = federation federation = { has_federation_type = swp_compact_federation }
	   }
	   modifier = {
		  desc = federation_acceptance_compact
		  add = 50
		  from = { has_federation = yes relative_power = { who = prev value > equivalent } } exists = federation federation = { has_federation_type = swp_compact_federation }
	   }
	   modifier = {
		  desc = federation_acceptance_swp_imperial_loyalists
		  add = -20
		  from = { OR = { has_ai_personality = galactic_imperialists has_ai_personality = imperial_loyalists } }
	   }
	   modifier = {
		  desc = federation_acceptance_swp_isolationists
		  add = -50
		  from = { OR = { has_ai_personality = militant_isolationists has_ai_personality = democratic_isolationists has_ai_personality = enigmatic_isolationists has_ai_personality = spiritual_isolationists has_ai_personality = hivemind_isolationists has_ai_personality = hutt_clans } }
	   }
	   modifier = {
		  desc = federation_acceptance_sw_separatists
		  add = 50
		  from = { has_ai_personality = separatists }
	   }
	   modifier = {
		  desc = federation_acceptance_sw_corporatists
		  add = 10
		  from = { has_ai_personality = greedy_corporatists }
	   }
	}
 }