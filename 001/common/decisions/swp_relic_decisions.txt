##########################################################################
# Relic Decisions
##########################################################################

decision_plant_sapling = {
	owned_planets_only = yes
	sound = event_administrative_work
	icon = decision_resources

	resources = {
		category = decisions
		cost = {
			influence = 50
		}
	}
	
	potential = {
		has_modifier = pm_has_sapling
	}
	
	effect = {
		hidden_effect = {
			owner = { country_event = { id = swp_relic_event.50 } }
		}
	}
	
	ai_weight = {
		weight = 0
		### swfr changes
		modifier = {
			weight = 10
		}
	}
}
