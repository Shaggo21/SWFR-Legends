##########################################################################
# Decisions
##########################################################################
decision_planet_food_boost = {
	owned_planets_only = yes
	sound = event_administrative_work
	icon = decision_resources
	
	resources = {
		category = decisions
		cost = {
			food = 500
		}
	}
	
	potential = {
		owner = {
			is_regular_empire = yes
			is_lithoid_empire = no
		}
		NOR = {
			has_modifier = planet_growth_discouraged
			has_modifier = planet_population_control
		}
	}
	
	allow = {
		NOT = { has_modifier = pm_food_boost }
	}	
	
	effect = {
		# swfr changes
		add_modifier = { modifier = "pm_food_boost" days = -1 }
		hidden_effect = {
			add_deposit = d_food_boost
		}
	}
	
	ai_weight = {
		# swfr changes
		weight = 5
		# don't encourage growth if there is no free housing
		modifier = {
			factor = 0
			free_housing < 1
		}
	}
}

decision_end_planet_food_boost = {
	owned_planets_only = yes
	sound = event_administrative_work
	icon = decision_resources

	resources = {
		category = decisions
	}
	
	potential = {
		has_modifier = pm_food_boost
	}
	
	effect = {
		remove_modifier = pm_food_boost
		hidden_effect = {
			remove_deposit = d_food_boost
		}
	}
	
	ai_weight = {
		weight = 0
		modifier = {
			weight = 10
			owner = {
				has_resource = { type = food amount < 10 }
			}
		}
	}
}

decision_planet_luxuries_boost = {
	owned_planets_only = yes
	sound = event_administrative_work
	icon = decision_luxuries

	resources = {
		category = decisions
		cost = {
			trigger = {
				num_pops < 10
			}
			consumer_goods = 50
		}
		cost = {
			trigger = {
				num_pops >= 10
				num_pops < 20
			}
			consumer_goods = 100
		}
		cost = {
			trigger = {
				num_pops >= 20
				num_pops < 30
			}
			consumer_goods = 150
		}	
		cost = {
			trigger = {
				num_pops >= 30
				num_pops < 40
			}
			consumer_goods = 200
		}	
		cost = {
			trigger = {
				num_pops >= 40
				num_pops < 50
			}
			consumer_goods = 250
		}			
		cost = {
			trigger = {
				num_pops >= 50
				num_pops < 60
			}
			consumer_goods = 300
		}
		cost = {
			trigger = {
				num_pops >= 60
				num_pops < 70
			}
			consumer_goods = 350
		}			
		cost = {
			trigger = {
				num_pops >= 70
				num_pops < 85
			}
			consumer_goods = 400
		}
		cost = {
			trigger = {
				num_pops >= 85
				num_pops < 100
			}
			consumer_goods = 450
		}
		cost = {
			trigger = {
				num_pops >= 100
			}
			consumer_goods = 500
		}				
	}
	
	potential = {
		owner = { is_regular_empire = yes }
	}
	
	allow = {
		NOT = { has_modifier = pm_luxury_boost }
	}	
	
	effect = {
		add_modifier = { modifier = "pm_luxury_boost" days = -1 }
		hidden_effect = {
			add_deposit = d_luxury_boost
		}
	}
	
	ai_weight = {
		weight = 0
		### swfr changes
		modifier = {
			weight = 10
			owner = {
				has_resource = { type = consumer_goods amount > 5000 }
			}
			planet = { free_district_slots = 0 free_building_slots = 0 }
		}
	}
}

decision_end_planet_luxuries_boost = {
	owned_planets_only = yes
	sound = event_administrative_work
	icon = decision_luxuries

	resources = {
		category = decisions
	}
	
	potential = {
		has_modifier = pm_luxury_boost
	}
	
	effect = {
		remove_modifier = pm_luxury_boost
		hidden_effect = {
			remove_deposit = d_luxury_boost
		}
	}
	
	ai_weight = {
		weight = 0
		modifier = {
			weight = 10
			owner = {
				has_resource = { type = consumer_goods amount < 20 }
			}
		}
	}
}