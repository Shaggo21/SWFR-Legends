﻿### Rebel names
SWP_Human3 = {
	randomized = no

	ship_names = {
		generic = {
			Mystic Naiad Nautilus Nebula Nimble "North Star" Nymph Obdurate Ocean Onward Oracle Paradox Paragon Pearl Peregrine Perseverance Marksman Rifleman Sniper Trident  Trumpet Truncheon Typhoon Undaunted Venture Versatile Vigorous Viper Vivacious Winger Wizard Wyvern Wolfhound Devastator Warhammer Retribution Senator Tornado Infinity Usurper Utmost Valorous Whirlwind Athena Artemis Vigilance Decimator Indomitable Goliath Sentinel Brilliant Blazer Audacity Gallant Nova Shadow Allegiance Challenger Fearless Protector Guardian Victorious Courageous Defiance Valorous Azrael
			"Adamantine" "Aralanni" "Ardent IV" "Armistice" "Avengeance" "Ballista" "Bloodstripe" "Borealis"
			"Caelus" "Caravan" "Chaf Envoy" "Commenor" "Corbantis" "Corellia" "Courteous" "Dauntless" "Defiant"
			"Deliverance" "Derapha" "Devout" "Empirical" "Empyrean" "Era Daimanos" "Eye of the Storm" "Farstar"
			"Fastblast" "Galaaron" "Garrote" "Gilagimar" "Halleck" "Hunter" "Intractable" "Ithor Lady"
			"Justice Crusader" "Kana's Steel" "Lathir" "Liberty" "Lillibanca" "Maelstrom" "Malachite Scent"
			"Mercy" "Myrkos" "Nebula Chaser" "Nova" "Nova Viper" "Obedience" "Olemp" "Pegasus" "Perspicacity"
			"Phalanx" "Plooriod Bodkin" "Predominance" "Prosperity" "Purity" "Recalcitrant" "Red Nova" "Redma"
			"Reliable" "Reliant" "Reluctant" "Republic" "Republic's Return" "Resolve" "Respectable" "Revenge"
			"Shadowblade" "Spritespray" "Star Dream" "Stark Wing" "Starveil" "Undying" "Vaathkree" "Vainglorious"
			"Valiant" "Valor" "Vanquisher" "Vinsor" "Visseon" "Wennis" "Yavin's Glory" "Moldy Crow" "Raven's Claw"
			Agile Archer Arrow Bruiser Cherub Chimera Claymore Broadsword Battleaxe Crossbow Cutlass Deterrent Errant Faithful Fierce Gladius Longbow 
			Grappler Hammer Vampire Velox Venator Vendetta Venerable Venomous Taciturn Supreme Success Shredder Seraph Serpent Spitfire Stalker Stratagem
			Scimitar Scorcher Scourge Scythe Revenant Redoubtable Raptor Reaver Prodigal Rapier Prudent Persistent Peerless Myrmidon Indignant Javelin Knight
			Starwolf Bonaventure Adventure Boxer Buccaneer Cavalier Celestial Chariot Comet Crucible Cyclone Decisive Dexterous Diadem Earnest Eclipse Forger Fortune 
			Gauntlet Griffin Horizon Hotspur Hurricane Hussar Jubilant Lancer Lightning Magic Mediator Mermaid Meteor Minstrel Miranda Mustang Sharpshooter
			Cyclops Centaur Adversary Assassin Dagger Daring Duty Havoc Hero Hunter Justice Impeccable Interceptor Infernal Loyal Matchless Mauler Mayhem Malice
			Menace Merciless Mischief Monarch Patriot Predator Powerful Punisher Ravager Reaper Savage Spiteful Templar Terminator Terror Thrasher Minotaur
			Manticore Interdictor Intruder Hydra Enfield Harasser Unbeaten Werewolf Zealous Petulant Phantom Phoenix Pike Pursuer Raider Rainbow Rapid Redemption 
			Relentless Rigorous Robust Sabre Salvation Sanguine Scarab Scepter "Sea Witch" Siren Sorceress Spearhead Specter Spirit Splendid Stallion Stalwart 
			Steadfast Strenuous Superb Supernova Surprise Swiftsure Talent Talisman Talon Taurus Tenacious "Thunder Child" Thunderous Tireless Trenchant Righteous
			Acheron Achilles Agamemnon Amphion Aquilon Argonaut Argus Ariel Aurora Avernus Bacchus Bucephalus Calliope Calypso Castor Charybdis Circe
			Cleopatra Constantine Cordelia Cyrus Damocles Daedalus Darius Diomede Dryad Electra Endymion Erebus Fenris Freya Gilgamesh Gorgon Halcyon Hecate
			Hector Hesperus Janus Jason Juno Jupiter Lancelot Lazarus Leander Leonidas Lysander Maenad Mars Medea Medusa Menestheus Mercury Merlin Minerva
			Minos Neptune Nessus Nestor Nimrod Niobe Oberon Odin Ophelia Orestes Orpheus Osiris Pandora Penelope Persephone Perseus Phoebe Pluto Poseidon
			Proteus Pylades Serapis Stygian Tantalus Telemachus Theseus Thor Trajan Triton Venus Adamant Ardent Assail Austere Archon Adroit Aegis Anchor 
			Corsair Banshee Enchantress Eradicator Tormentor Legion Reprisal Aggressor Assault Eviscerator Imperator Excellence Unrivalled Warlord
			Vengeance Triumphant Nemesis Avenger Champion Conqueror Crusader Glory Immortal Judicator Vanguard Renown Demolisher Dragon Executor Furious Gladiator
			Magnificent Ogre Onslaught Revenge Vanquisher Vehement Basilisk Tempest Overlord Warrior Titan Colossus Prometheus Nike Paladin Sovereign Repulse
			Majestic Avatar Behemoth Centurion Citadel Katana Kraken Cerberus Valkyrie
		}
		swp_bellator = {
			Dreadnought Harbinger Juggernaut Leviathan Hyperion Galatea Ambush Vindicator
		}
		science = {
			Crescent Diligent Zephyr Enigma Conquistador Scylla Arbiter Asperity Eminence Astute Alacrity Satyr Prospero Untiring Zenith Torch Remembrance Pilgrim
			Trailblazer Vagabond Wayfarer Ambassador Emissary Nomad Pathfinder Wanderer Traveler
		}
		colonizer = {
			Godspeed Prophecy "Manifest Destiny" Virtue Potential Expansion "Pioneer Spirit" Prosperity Summit Pinnacle Seeder Harvest Demeter Isis Ceres Exodus
			Foundation Scion Progeny Obligation Purpose Provider "Shining City" Acquirer Cultivator "Dawn of Civilization" Purity Community Brotherhood
		}
		sponsored_colonizer = { # needed when there are no generic names
			Godspeed Prophecy "Manifest Destiny" Virtue Potential Expansion "Pioneer Spirit" Prosperity Summit Pinnacle Seeder Harvest Isis Ceres Exodus
			Foundation Scion Progeny Obligation Purpose Provider "Shining City" Acquirer Cultivator "Dawn of Civilization" Purity Community Brotherhood
		}
		constructor = {
			Industry Assiduity Effort Laborer Builder Craftsman "Hard Hat" Progress Engineer Promise Service Shelter Beast Creator Developer Forger Foundry 
			Factory Anvil Pillar Architect Accommodator Ptah Ilmarinen Creidhne Gobannus Luchtaine Goibniu Vishvakarman Tvastar Ikenga Qaynan Ucuetis Ogoun
		}
		transport = {
			"Ground Transport"
		}
		

		military_station_xq1 = {
			"Outpost 1" "Outpost 2" "Outpost 3" "Outpost 4" "Outpost 5" "Outpost 6" "Outpost 7" "Outpost 8" "Outpost 9" "Outpost 10" "Outpost 11" "Outpost 12"
			"Outpost 13" "Outpost 14" "Outpost 15" "Outpost 16" "Outpost 17" "Outpost 18" "Outpost 19" "Outpost 20" "Outpost 21" "Outpost 22" "Outpost 23"
			"Outpost 24" "Outpost 25" "Outpost 26" "Outpost 27" "Outpost 28" "Outpost 29" "Outpost 30" "Outpost 31" "Outpost 32" "Outpost 33" "Outpost 34"
			"Outpost 35" "Outpost 36" "Outpost 37" "Outpost 38" "Outpost 39" "Outpost 40" "Outpost 41" "Outpost 42" "Outpost 43" "Outpost 44" "Outpost 45"
			"Outpost 46" "Outpost 48" "Outpost 49" "Outpost 50" "Outpost 51" "Outpost 52" "Outpost 53" "Outpost 54" "Outpost 55" "Outpost 56" "Outpost 57"
			"Outpost 58" "Outpost 59" "Outpost 60" "Outpost 61" "Outpost 62" "Outpost 63" "Outpost 64" "Outpost 65" "Outpost 66" "Outpost 67" "Outpost 68"
			"Outpost 69" "Outpost 70"
		}
		military_station_xq2 = {
			"Outpost 1" "Outpost 2" "Outpost 3" "Outpost 4" "Outpost 5" "Outpost 6" "Outpost 7" "Outpost 8" "Outpost 9" "Outpost 10" "Outpost 11" "Outpost 12"
			"Outpost 13" "Outpost 14" "Outpost 15" "Outpost 16" "Outpost 17" "Outpost 18" "Outpost 19" "Outpost 20" "Outpost 21" "Outpost 22" "Outpost 23"
			"Outpost 24" "Outpost 25" "Outpost 26" "Outpost 27" "Outpost 28" "Outpost 29" "Outpost 30" "Outpost 31" "Outpost 32" "Outpost 33" "Outpost 34"
			"Outpost 35" "Outpost 36" "Outpost 37" "Outpost 38" "Outpost 39" "Outpost 40" "Outpost 41" "Outpost 42" "Outpost 43" "Outpost 44" "Outpost 45"
			"Outpost 46" "Outpost 48" "Outpost 49" "Outpost 50" "Outpost 51" "Outpost 52" "Outpost 53" "Outpost 54" "Outpost 55" "Outpost 56" "Outpost 57"
			"Outpost 58" "Outpost 59" "Outpost 60" "Outpost 61" "Outpost 62" "Outpost 63" "Outpost 64" "Outpost 65" "Outpost 66" "Outpost 67" "Outpost 68"
			"Outpost 69" "Outpost 70"
		}
		military_station_golan1 = {
			"SpaceGun Victory" "SpaceGun Success" "SpaceGun Supremacy" "SpaceGun Superior" "SpaceGun Adventure"
			"SpaceGun Ascendancy" "SpaceGun Paramount" "SpaceGun Preponderous" "SpaceGun Exemplary" "SpaceGun Eximious" "SpaceGun Pertinacity"
			"SpaceGun Acclaim" "SpaceGun Honor" "SpaceGun Renown" "SpaceGun Glory" "SpaceGun Superb" "SpaceGun Magnificence"
			"SpaceGun Majestic" "SpaceGun Splendor" "SpaceGun Paradigm" "SpaceGun Flawless" "SpaceGun Consummate" "SpaceGun Merit"
			"SpaceGun Perfection" "SpaceGun Virtue" "SpaceGun Advantage" "SpaceGun Rectitude" "SpaceGun Incorruptible"
			"SpaceGun Integrity" "SpaceGun Justice" "SpaceGun Probity" "SpaceGun Prudence" "SpaceGun Purity" "SpaceGun Resolute"
			"SpaceGun Dignity" "SpaceGun Distinction" "SpaceGun Excellence" "SpaceGun Faithful" "SpaceGun Discipline" "SpaceGun Steadfast"
			"SpaceGun Recompense" "SpaceGun Requiem" "SpaceGun Courage" "SpaceGun Valor" "SpaceGun Gallant"
			"SpaceGun Daring" "SpaceGun Constancy" "SpaceGun Perseverence" "SpaceGun Tenacity" "SpaceGun Sedulity" "SpaceGun Diligence"
		}
		military_station_golan2 = {
			"SpaceGun Victory" "SpaceGun Success" "SpaceGun Supremacy" "SpaceGun Superior" "SpaceGun Adventure"
			"SpaceGun Ascendancy" "SpaceGun Paramount" "SpaceGun Preponderous" "SpaceGun Exemplary" "SpaceGun Eximious" "SpaceGun Pertinacity"
			"SpaceGun Acclaim" "SpaceGun Honor" "SpaceGun Renown" "SpaceGun Glory" "SpaceGun Superb" "SpaceGun Magnificence"
			"SpaceGun Majestic" "SpaceGun Splendor" "SpaceGun Paradigm" "SpaceGun Flawless" "SpaceGun Consummate" "SpaceGun Merit"
			"SpaceGun Perfection" "SpaceGun Virtue" "SpaceGun Advantage" "SpaceGun Rectitude" "SpaceGun Incorruptible"
			"SpaceGun Integrity" "SpaceGun Justice" "SpaceGun Probity" "SpaceGun Prudence" "SpaceGun Purity" "SpaceGun Resolute"
			"SpaceGun Dignity" "SpaceGun Distinction" "SpaceGun Excellence" "SpaceGun Faithful" "SpaceGun Discipline" "SpaceGun Steadfast"
			"SpaceGun Recompense" "SpaceGun Requiem" "SpaceGun Courage" "SpaceGun Valor" "SpaceGun Gallant"
			"SpaceGun Daring" "SpaceGun Constancy" "SpaceGun Perseverence" "SpaceGun Tenacity" "SpaceGun Sedulity" "SpaceGun Diligence"
		}
		military_station_golan3 = {
			"SpaceGun Victory" "SpaceGun Success" "SpaceGun Supremacy" "SpaceGun Superior" "SpaceGun Adventure"
			"SpaceGun Ascendancy" "SpaceGun Paramount" "SpaceGun Preponderous" "SpaceGun Exemplary" "SpaceGun Eximious" "SpaceGun Pertinacity"
			"SpaceGun Acclaim" "SpaceGun Honor" "SpaceGun Renown" "SpaceGun Glory" "SpaceGun Superb" "SpaceGun Magnificence"
			"SpaceGun Majestic" "SpaceGun Splendor" "SpaceGun Paradigm" "SpaceGun Flawless" "SpaceGun Consummate" "SpaceGun Merit"
			"SpaceGun Perfection" "SpaceGun Virtue" "SpaceGun Advantage" "SpaceGun Rectitude" "SpaceGun Incorruptible"
			"SpaceGun Integrity" "SpaceGun Justice" "SpaceGun Probity" "SpaceGun Prudence" "SpaceGun Purity" "SpaceGun Resolute"
			"SpaceGun Dignity" "SpaceGun Distinction" "SpaceGun Excellence" "SpaceGun Faithful" "SpaceGun Discipline" "SpaceGun Steadfast"
			"SpaceGun Recompense" "SpaceGun Requiem" "SpaceGun Courage" "SpaceGun Valor" "SpaceGun Gallant"
			"SpaceGun Daring" "SpaceGun Constancy" "SpaceGun Perseverence" "SpaceGun Tenacity" "SpaceGun Sedulity" "SpaceGun Diligence"
		}
		military_station_fleetop = {
			Victory Success Supremacy Superior Adventure Ascendancy Paramount Preponderous Exemplary Eximious
			Acclaim Honor Renown Glory Superb Magnificence Majestic Splendor Paradigm Flawless Consummate Merit
			Perfection Virtue Advantage Rectitude Incorruptible Integrity Justice Probity Prudence Purity Resolute
			Dignity Distinction Excellence Faithful Discipline Recompense Requiem Courage Valor Gallant
			Daring Constancy Perseverence Tenacity Sedulity Diligence Pertinacity Steadfast Impregnable Impenetrable
			Unbreakable Implacable Impervious Unswerving Unyielding
		}
		military_station_small = {
			Citadel Albacore Gateway Vincent Babylon Caernavon Acre Pembroke Acheron Spithead Halifax Victoria Chatham Anchorage "Scapa Flow" "Hampton Roads" Brooklyn Norfolk
			Newport Guam Pensacola Vesuvius McKinley Everest Kilimanjaro Aconcagua Elbrus "St. Helens" Fuji Olympus Sinai Ambrose Bermuda Farpoint "Blue Star" "Olympus Mons"
			Tharsis Syrtis Blackham Fletcher Perry Butakov Morris Moorsom Hart Hargood Spruance Tanaka Halsey Turner Doria Tirpitz Cunningham Jellicoe Hipper Beatty Potemkin
			Durham Codrington Milne Laforey Tyler Hawke Yonai Pound King Crutchley Mountbatten Kountouriotis Kuznetsov Essen Kornilov Suffren Treville Villeneuve Grasse Byng Lezo
		}
		ion_cannon = {
			Bombard Ballista Catapult Harpoon Polybolos Culverin
		}
	}
	ship_class_names = {
		swp_cr90_corvette = { "CR-90" }
		swp_jehaveyir_corvette = { "Jehavey'ir" }
		swp_lupus_corvette = { Lupus }
		swp_early_asdroni_corvette = { Asdroni }
		swp_customs_corvette = { Customs }
		swp_hammerhead_corvette = { Hammerhead }
		swp_scurge_corvette = { Scurge }
		swp_krayt_corvette = { Krayt }
		swp_diamond_corvette = { Diamond }
		swp_etti_corvette = { Etti }
		swp_asdroni_corvette = { "Asdroni II" }
		swp_vigil_corvette = { Vigil }
		swp_dp20_corvette = { DP20 }
		swp_adv_asdroni_corvette = { "Asdroni III" }
		swp_raider_corvette = { Raider }
		swp_dornean_corvette = { Dornean }
		swp_crusader_corvette = { Crusader }
		swp_marauder_corvette = { Marauder }
		swp_arquitens_frigate = { Arquitens }
		swp_kiltirin_frigate = { Kiltirin }
		swp_pelta_frigate = { Pelta }
		swp_pinnance_frigate = { Pinnace }
		swp_nova_frigate = { Nova }
		swp_vigilance_frigate = { Vigilance }
		swp_neb_b_frigate = { "Nebulon-B" }
		swp_cg273_frigate = { "CG-273" }
		swp_kariek_frigate = { Kariek }
		swp_escort_carrier_frigate = { "Ton-Falk" }
		swp_quasar_frigate = { "Quasar Fire" }
		swp_lancer_frigate = { Lancer }
		swp_teroch_frigate = { Teroch }
		swp_neb_b2_frigate = { "Nebulon-B2" }
		swp_adv_nova_frigate = { "Nova II" }
		swp_adv_vigilance_frigate = { "Vigilance II" }
		swp_velox_frigate = { Velox }
		swp_mc30c_frigate = { MC30c }
		swp_vengeance_frigate = { Vengeance }
		swp_adv_kariek_frigate = { "Kariek II" }
		swp_cc7700_frigate = { "CC-7700" }
		swp_victory2_cruiser = { "Victory Mark 2" }
		swp_wavecrest_cruiser = { Wavecrest }
		swp_early_battledragon_cruiser = { "Battle Dragon" }
		swp_nuruodo_cruiser = { Nuruodo }
		swp_sabaoth_cruiser = { Sabaoth }
		swp_kontos_cruiser = { Kontos }
		swp_mc40_cruiser = { MC40 }
		swp_battledragon_cruiser = { "Battle Dragon II" }
		swp_broadside_cruiser = { Broadside }
		swp_protector_cruiser = { Protector }
		swp_warlord_cruiser = { Warlord }
		swp_gladiator_cruiser = { Gladiator }
		swp_beviin_cruiser = { Beviin }
		swp_liberator_cruiser = { Liberator }
		swp_adv_battledragon_cruiser = { "Battle Dragon III" }
		swp_adv_nuruodo_cruiser = { "Nuruodo II" }
		swp_immobilizer418_cruiser = { "Immobilizer 418" }
		swp_int_battledragon_cruiser = { "Battle Dragon IV" }
		swp_massias_cruiser = { Massias }
		swp_dreadnought_heavy_cruiser = { Dreadnought }
		swp_kyramud_heavy_cruiser = { Kyramud }
		swp_munificent_heavy_cruiser = { Munificent }
		swp_prosvoli_heavy_cruiser = { Prosvoli }
		swp_acclamator_heavy_cruiser = { Acclamator }
		swp_lictor_heavy_cruiser = { Lictor }
		swp_bulwark_heavy_cruiser = { Bulwark }
		swp_vsd1_heavy_cruiser = { "Victory I" }
		swp_assault1_heavy_cruiser = { "Assault Frigate I" }
		swp_bulk_heavy_cruiser = { "Bulk" }
		swp_vindicator_heavy_cruiser = { Vindicator }
		swp_dauntless_heavy_cruiser = { Dauntless }
		swp_captor_heavy_cruiser = { Captor }
		swp_auriette_heavy_cruiser = { "Au'riette" }
		swp_mc55_heavy_cruiser = { MC55 }
		swp_afthonia_heavy_cruiser = { Afthonia }
		swp_vsd2_heavy_cruiser = { "Victory II" }
		swp_beroya_heavy_cruiser = { Beroya }
		swp_assault2_heavy_cruiser = { "Assault Frigate II" }
		swp_hapan_vsd2_heavy_cruiser = { "HRN Victory II" }
		swp_adv_prosvoli_heavy_cruiser = { "Prosvoli II" }
		swp_venator_star_destroyer = { Venator }
		swp_centurion_star_destroyer = { Centurion }
		swp_mc75_star_cruiser = { MC75 }
		swp_kandosii_destroyer = { Kandosii }
		swp_corellian_destroyer = { Corellian }
		swp_providence_destroyer = { Providence }
		swp_syndic_destroyer = { Syndic }
		swp_recusant_destroyer = { Recusant }
		swp_invincible_destroyer = { Invincible }
		swp_imperial1_star_destroyer = { "Imperial I" }
		swp_hapan_imperial1_star_destroyer = { "HRN Imperial I" }
		swp_procursator_star_destroyer = { Procursator }
		swp_mc80a_star_cruiser = { MC80a }
		swp_dh_omni_destroyer = { "DH-Omni" }
		swp_aggressor_destroyer = { Aggressor }
		swp_adv_syndic_destroyer = { "Syndic II" }
		swp_mc80b_star_cruiser = { "MC80 Liberty" }
		swp_keldabe_destroyer = { Keldabe }
		swp_adv_corellian_destroyer = { "Corellian II" }
		swp_chaf_destroyer = { Chaf }
		swp_imperial2_star_destroyer = { "Imperial II" }
		swp_hapan_imperial2_star_destroyer = { "HRN Imperial II" }
		swp_ascendancy_star_destroyer = { Ascendancy }
		swp_tector_star_destroyer = { Tector }
		swp_interdictor_sd = { Dominator }
		swp_allegiance_battlecruiser = { Allegiance }
		swp_lucrehulk_battlecruiser = { Lucrehulk }
		swp_phalanx_battlecruiser = { Phalanx }
		swp_titan_battlecruiser = { Titan }
		swp_mc80_home1_battlecruiser = { "MC80 Independence" }
		swp_star_home_battlecruiser = { "Star Home" }
		swp_subjugator = { Subjugator }
		swp_bellator = { Bellator }
		swp_solyc_verd = { "Sol'yc Verd" }
		swp_executor = { Executor }
	}
	fleet_names = {
		sequential_name = "%O% Armada" 
	}
	army_names = {
		defense_army = {
			sequential_name = "%O% Planetary Guard"
		}
		assault_army = {
			sequential_name = "%O% Expeditionary Force"
		}
		slave_army = {
			sequential_name = "%O% Indentured Rifles"
		}
		clone_army = {
			sequential_name = "%O% Clone Army"
		}
		robotic_army = {
			sequential_name = "%O% Hunter-Killer Group"
		}
		robotic_defense_army = {
			sequential_name = "%O% Ground Defense Matrix"
		}
		psionic_army = {
			sequential_name = "%O% Psi Commando"
		}
		xenomorph_army = {
			sequential_name = "%O% Bio-Warfare Division"
		}
		gene_warrior_army = {
			random_names = {
				"SARC-A 'Gladiators'" "SARC-B 'Widowmakers'" "SARC-C 'Immortals'" "SARC-D 'Berserkers'" "SARC-E 'Assassins'"
				"SARC-F 'Reapers'" "SARC-G 'Nighthawks'" "SARC-H 'Desperados'" "SARC-I 'Grey Knights'" "SARC-J 'Roughnecks'" 
			}
			sequential_name = "%O% Bio-Engineered Squadron"
		}
		occupation_army = {
			sequential_name = "%O% Garrison Force"
		}
		robotic_occupation_army = {
			sequential_name = "%O% Mechanized Garrison"
		}
		primitive_army = {
			sequential_name = "Primitive Army %C%"
		}
		industrial_army = {
			sequential_name = "Industrial Army %C%"
		}
		postatomic_army = {
			sequential_name = "Post-Atomic Army %C%"
		}
		imp_garrison_01 = { sequential_name = "%O% Stormtrooper Garrison" }
		reb_garrison_01 = { sequential_name = "%O% Rebel Trooper Garrison" }
		hut_garrison_01 = { sequential_name = "%O% Hutt Garrison" }
		man_garrison_01 = { sequential_name = "%O% Mandalorian Garrison" }
		cor_garrison_01 = { sequential_name = "%O% Corellian Garrison" }
		cis_garrison_01 = { sequential_name = "%O% CIS Garrison" }
		csa_garrison_01 = { sequential_name = "%O% CSA Garrison" }
		hap_garrison_01 = { sequential_name = "%O% Hapes Garrison" }
		oth_garrison_01 = { sequential_name = "%O% Militia Garrison" }
		dro_garrison_01 = { sequential_name = "%O% Droid Garrison" }
		imp_infantry_1 = { sequential_name = "%O% Stormtroopers" }
		imp_infantry_2 = { sequential_name = "%O% Shock Troopers" }
		imp_infantry_3 = { sequential_name = "%O% Dark Troopers" }
		reb_infantry_1 = { sequential_name = "%O% Rebel Troopers" }
		reb_infantry_2 = { sequential_name = "%O% Assault Troopers" }
		reb_infantry_3 = { sequential_name = "%O% Heavy Assault Troopers" }
		hut_infantry_1 = { sequential_name = "%O% Hutt Cartel Gang" }
		hut_infantry_2 = { sequential_name = "%O% Hutt Elite Mercenaries" }
		hut_infantry_3 = { sequential_name = "%O% Hutt Deshade Mercenaries" }
		man_infantry_1 = { sequential_name = "%O% Mandalorian Warriors" }
		man_infantry_2 = { sequential_name = "%O% Mandalorian Jet Warriors" }
		man_infantry_3 = { sequential_name = "%O% Mandalorian Elite " }
		cor_infantry_1 = { sequential_name = "%O% CorSec Troopers" }
		cis_infantry_1 = { sequential_name = "%O% Corporate Enforcers" }
		csa_infantry_1 = { sequential_name = "%O% Espos Troopers" }
		csa_infantry_2 = { sequential_name = "%O% Espos Elite Troopers" }
		csa_infantry_3 = { sequential_name = "%O% Espos State Guard" }
		hap_infantry_1 = { sequential_name = "%O% Hapes Militia" }
		hap_infantry_2 = { sequential_name = "%O% Fountain Gunners" }
		hap_infantry_3 = { sequential_name = "%O% Fountain Elite" }
		oth_infantry_1 = { sequential_name = "%O% Mercenaries" }
		oth_infantry_2 = { sequential_name = "%O% Heavy Mercenary Battalion" }
		oth_infantry_3 = { sequential_name = "%O% Mandalorian Mercenaries" }
		clo_infantry_1 = { sequential_name = "%O% Clone Troopers" }
		dro_infantry_1 = { sequential_name = "%O% B1 Battle Droids " }
		dro_infantry_2 = { sequential_name = "%O% B2 Super Battle Droids " }
		dro_infantry_3 = { sequential_name = "%O% Droideka Detachment" }
		imp_support_1 = { sequential_name = "%O% Light Walkers Detachment" }
		imp_support_3 = { sequential_name = "%O% Adv. Light Walkers Detachment" }
		reb_support_1 = { sequential_name = "%O% T-2 Tank Detachment" }
		hut_support_1 = { sequential_name = "%O% Hutt Barges Detachment" }
		man_support_1 = { sequential_name = "%O% Mandalorian Auxilary Detachment" }
		hap_support_1 = { sequential_name = "%O% Fountain Support Vehicles" }
		oth_support_1 = { sequential_name = "%O% Infantry Support Vehicles" }
		oth_support_2 = { sequential_name = "%O% HAVw A6 Juggernaut" }
		dro_support_1 = { sequential_name = "%O% DSD1 Droid Detachment" }
		imp_specops_2 = { sequential_name = "%O% Storm Commando" }
		imp_specops_3 = { sequential_name = "%O% Shadow Commando" }
		reb_specops_2 = { sequential_name = "%O% Rebel Commando" }
		reb_specops_3 = { sequential_name = "%O% Rebel Elite Commando" }
		hut_specops_2 = { sequential_name = "%O% Cartel Sabateurs" }
		man_specops_2 = { sequential_name = "%O% Mandalorian Commando" }
		man_specops_3 = { sequential_name = "%O% Mandalorian Elite Commando" }
		cor_specops_2 = { sequential_name = "%O% Corellian Infiltrators" }
		csa_specops_2 = { sequential_name = "%O% Espos SpecOps" }
		hap_specops_2 = { sequential_name = "%O% Fountain Snipers" }
		hap_specops_3 = { sequential_name = "%O% Her Majesty's Select Commandos" }
		oth_specops_2 = { sequential_name = "%O% Bounty Hunters" }
		oth_specops_3 = { sequential_name = "%O% Clone Commandos" }
		dro_specops_2 = { sequential_name = "%O% BX Droid Commando" }
		dro_specops_3_0 = { sequential_name = "%O% SD-4K Assasin Droids" }
		dro_specops_3_1 = { sequential_name = "%O% IG-series Assasin Droids" }
		imp_armor_1 = { sequential_name = "%O% 2-M Hovertank Detachment" }
		imp_armor_2 = { sequential_name = "%O% AT-AT Detachment" }
		imp_armor_3 = { sequential_name = "%O% AT-AT Advanced Detachment" }
		reb_armor_1 = { sequential_name = "%O% T-3B Tank Detachment" }
		reb_armor_2 = { sequential_name = "%O% Legacy AT-TE Detachment" }
		reb_armor_3 = { sequential_name = "%O% T-4B Modified Tank Detachment" }
		hut_armor_1 = { sequential_name = "%O% WLO-5 Tank Detachment" }
		man_armor_1 = { sequential_name = "%O% MHT-1 Detachment" }
		man_armor_2 = { sequential_name = "%O% MHT-2 Detachment" }
		cis_armor_1 = { sequential_name = "%O% AAT-1 Tank Detachment" }
		hap_armor_1 = { sequential_name = "%O% Fountain Armor Detachment" }
		oth_armor_1 = { sequential_name = "%O% TX-225 Assualt Tank Detachment" }
		oth_armor_2 = { sequential_name = "%O% Salvaged AT-TE Detachment" }
		oth_armor_3 = { sequential_name = "%O% Salvaged AT-HE Detachment" }
		dro_armor_2 = { sequential_name = "%O% OG-9 Spider Droid Detachment" }
		dro_armor_3 = { sequential_name = "%O% Super Droid Tank Detachment" }
		imp_arty_2 = { sequential_name = "%O% SPMA Artillery Detachment" }
		imp_arty_3 = { sequential_name = "%O% SPHA Artillery Detachment" }
		reb_arty_2 = { sequential_name = "%O% MPTL Artillary Detachment" }
		reb_arty_3 = { sequential_name = "%O% HMPTL Artillary Detachment" }
		hut_arty_1 = { sequential_name = "%O% Artillery Barge Detachment" }
		man_arty_2 = { sequential_name = "%O% MHA Artillery Detachment" }
		hap_arty_2 = { sequential_name = "%O% MLC-4 Artillery Tank" }
		oth_arty_1 = { sequential_name = "%O% MLC-3 Light Artillery Tank" }
		oth_arty_3 = { sequential_name = "%O% SPHA-T Artillery Detachment" }
		dro_arty_2 = { sequential_name = "%O% J-1 Proton Cannon Detachment" }
		dro_arty_3 = { sequential_name = "%O% Hellfire Droid Detachment" }
		imp_arial_1 = { sequential_name = "%O% TIE Squadron" }
		imp_arial_2 = { sequential_name = "%O% Lambda Shuttle Gunship Squadron" }
		reb_arial_1 = { sequential_name = "%O% T-47 Airspeeder Squadron" }
		reb_arial_2 = { sequential_name = "%O% Tachyon Airspeeder Squadron" }
		hut_arial_1 = { sequential_name = "%O% Sandspeeder Squadron" }
		man_arial_1 = { sequential_name = "%O% Mandalorian SkyFighter Squadron" }
		cor_arial_2 = { sequential_name = "%O% VCX-series Starfighter Squadron" }
		cis_arial_2 = { sequential_name = "%O% Mechanized Assault Flyer" }
		csa_arial_2 = { sequential_name = "%O% R-41 Starchaser Squadron" }
		hap_arial_1 = { sequential_name = "%O% My'til Fighter Squadron" }
		oth_arial_2_0 = { sequential_name = "%O% Turbostorm-class Gunship" }
		oth_arial_2_1 = { sequential_name = "%O% Legacy LAAT Gunship" }
		oth_arial_1 = { sequential_name = "%O% Storm IV Twin-Pod Cloud Car" }
		dro_arial_1 = { sequential_name = "%O% Vulture Droid Airwing" }
		dro_arial_3 = { sequential_name = "%O% HMP Droid Gunship" }
		imp_homorguard = { sequential_name = "%O% Honor Guard" }
		reb_bothan = { sequential_name = "%O% Bothan Operatives" }
		reb_wookie = { sequential_name = "%O% Wookie Battlegroup" }
		cis_magnaguard = { sequential_name = "%O% Magnaguard" }
		hap_royalguard = { sequential_name = "%O% Hapan Royal Guard" }
		oth_geonos = { sequential_name = "%O% Geonosian" }
		fortification_1 = { sequential_name = "%O% DF9 Cannon Fortifications" }
		fortification_3 = { sequential_name = "%O% Turbolaser Cannon Fortifications" }
		cre_rancor = { sequential_name = "%O% Rancor" }
		cre_ancientrancor = { sequential_name = "%O% Ancient Rancor" }
		cre_acklay = { sequential_name = "%O% Acklay " }
		cre_primalbeasts = { sequential_name = "%O% Primal Beasts" }
		cre_dusturbedbeasts = { sequential_name = "%O% Disturbed Beasts" }
		imp_occupation_01 = { sequential_name = "%O% Stormtrooper Occupation Garrison" }
		reb_occupation_01 = { sequential_name = "%O% Rebel Trooper Occupation Garrison" }
		hut_occupation_01 = { sequential_name = "%O% Hutt Occupation Garrison" }
		man_occupation_01 = { sequential_name = "%O% Mandalorian Occupation Garrison" }
		cor_occupation_01 = { sequential_name = "%O% Corellian Occupation Garrison" }
		cis_occupation_01 = { sequential_name = "%O% CIS Occupation Garrison" }
		csa_occupation_01 = { sequential_name = "%O% CSA Occupation Garrison" }
		hap_occupation_01 = { sequential_name = "%O% Hapes Occupation Garrison" }
		oth_occupation_01 = { sequential_name = "%O% Militia Occupation Garrison" }
		dro_occupation_01 = { sequential_name = "%O% Droid Occupation Garrison" }
	}

	### PLANETS
	planet_names = {
		generic = {
			names = {
			}
		}
	}

	### CHARACTERS
	character_names = {
		names1 = {			
			first_names_male = {
				Bail Ylenic Kel Gunn Gregory  Abraxis Jonas Xerda Carth Kir Mebba Dagos Adam  Kyle Forren Malcolm Jeho 
				Lee  Griggs Milo Aeron  Byrec Strata Lancer Zeak Niuk Lavidean Tanizandar Japla  Aran Rail Zeven Yakown 
				Sibar Sair Klai Jonas  Imay Traest Simo Carth Riyan Magnus Boceta Agnook Silus Rowan Coren Jett Linth Annor
				Ulas Fenn Tal Plo Torin Imay Boone Davith Jorgen Bertos Ress Malius
				Kale Cregan Galen Zekk Jinn-Tu Jaryn Gentti Yu-Sien Akela Arslan
				Sidias Haken Biran Vidown Ares Ithan Cul-utaan Thoja Cron Mossi
				Abric Darg Gaman Naiad Herub Marius Torkin Caton Kane Versita
				Cric Foxdi Montacca Jospi Baz Kam Erek Arslan Lerak Morla Baxter
				Talan Thion Kannon Corsair Caster Elden Faelgrom Jack Raltharan
				Flash Waseem Bendak Kaz Montileu Gonoga Silus Galven Vincenzo Ric
				Rebar Jebediah Aramil Abraxis Delmi Caalin Rassth Adorn Cyrus
				Zhar-Khan Adam Predor Tarlson Boaz Bradley Gunn Raltharan Zerrid Bobecc
				Jelantos Jephego Svante Tesvalo Aeon Dekar Jean-Francois Elen Han Owen Biggs 
				Rune Lott Daultay Lok Mar Gilramos Siv Zill Hath Aruteous Jull Sil Hallion Gap Palfa Sib
				Aito Sentepeth Kund Harro Hask Paw Brehg Reya Gode Nap Lufa Yee Geph Lik Gunaray
				Igren Dif Klewer Kuto Smeer Helb Toat Teek Jeek Orrin Veor Dak'on Nozabuiba Gama Budmle Proh'ven
				Salitt Vigro Rers Desson Cathan Dru Tunjas Borlo Krett Ablund Aach
				Aagon Aalto Aambler Abadaner Aban Abbus-Kelo Acan Ace Adame Adar Arvel
				Addi'shon Addle Adelhard Aden Adon-larr Adraas Adrenas Adrick Adun Aeron
				Agareth Aggadeen Alric Airten Agrippa Ajaan Ajolin Akaste'vel Akobi Alain  
				Alaric Albathius Alde Alec Alekam Alethios Alger Algrunar Ali-Alann Alif Alim'Gen
				Alin Alix Alkhara Allia Almak Almec Alon Alozen Alph Altaca Althon Alwynn Amberson Ament Amis
				Ammon Amzartho Anabar Anager Anarine Anathemos Andan Anders Ando Andra Andrew Andrid
				Andrik Andru Ar'al Angber Angral Anlen Anod Anselm Anskow Anson Adem Antone Antonin 
				Anuli Appleton Arac Arakkus Aramis Arana Armen  Arastide Arber Arcann Ardan Ardax Ardiff
				Ardle Ardmore Ardot Ardran Avinoam Ars Argona Argor Argos Arias Ariavos Arigo Aris Arkan
				Arkeld Arl Arljack Arlo Arlos Armatan Armitage Arnama Arnet Arno Arnothian Arnth Arnus
				Arrad Arratan Arrax Arric Artano Artok Arturo Arzanon Ash Ashaad Asirr Atali Atar Atmino
				Atren Augatta Avan Avanj Avarik Averdon Averen Averon Avers Aves Aviso Avrun Azkul Azrakel
				Ace B'arin Bail Beel Bix Blix Boz Braedon Bryn Brixus Cal Cassian Chik Cinto Cumberlayne Darrus
				Djinn Druth Det Ethan Galiir Gez Haemon Jae Jagged Jess Jesper Jon Gathal Gideon Jefand Kel Koffi
				Lazlo Laetin Lesk Maxiron Neet Osak Pabel Paddy Peita Peema Pellas Prewb Raymus Raar Rage Rel Reez
				Ras Rixes Ruceba Seraph Sim Sio Y'ull Tavion Tavid Tendis Tev Tiberus Toob Tor Turon Villis Wain Wedge
				Wes Zeen Baboo Bachenkall Bafor Bagitt Bailey Bailie Bairn Bala-Tik Balankin Balaro Balcomb Baldwin
				Baldy Balek Balisk Balkin Balko Ballala Ballard Ballax Bendodi Balog Balto Bams Bandor Boss Banley
				Banna Bannos Bant Banz Baradis Barak Baramak Barcel Bardo Barduun Barev Barich Barik Barkale Barksy
				Barleos Barlow Barney Barnley Baro Barry Barstead Bartam Barth Barthis Bas Bas-Ton Bascome Basile
				Basso Bastian Bathens Batono Bator Billal Battz Baxton Baysden Baz Be Beau Beckar Becker Beckman Bedareux
				Bedoch Beebe Beez Beldiss Belkan Beller Bellis Belos Byzal Bzorn Biggs 
				Bels Bemos Benay Benjo Benkin Benn Benni Benok Benthar Benton Bentro Boar Benx Bequesh Berandis 
				Berethon Bergan Berix Bernie Bertrum Berusal Bewil Bex Billan Biril Birket Birok Black
				Blagotine Blaide Blainekie Bledsoe Bleeker Blleen Bloodory Bloodworthy Blount Blyes
				Boakie Boaz Bob Bol Bon Bonze Boonan Boran Boraso Borgin Borm Bornae Bort Bortrek BoShek
				Bourom Bouyard Braden Brady Bragan Braynor Brayso Breckon Breehld Brinn Brinnie Brint Brinton
				Brogon Brohmer Brolis Broll Brom Brooser Brothic Brovto Broysc Bruan Brucce Bruckman
				Bryint Brylin Bry BrysonBular Bulger Bun'DelBur Burne Burque Burr Bursk Bursthed Byrga Byten
				Trey Lando Caaldra Cabaril Cabbel Caeleb Caerbellak Cafian Cag Cagaran Cam Camaran Camas Cardacs
				Cardy Careem Cargeloch Carhart Calron Calson Calum Calypho Cal Caladane Calaverous Carli Carlon
				Carlton Carn Carner Carness Carnor Carns Callef Callen Callow Calders Caleb Calo Calphayus
				Cathers Caton Cauley Cav Cav'Saran Cavin Cryle Cavwol Cawelyn Cecius Cede Cemma Ceoloe Cerasi Cerik
				Charny Charol Charpp Charnoq Charnso Cestus Ch'arb Ch'gally Ch'lessan Cha Chael Challer Chambe Crlyn Chang
				Chappral Chef Chentu Chell Chessiq Chet Chiann Chicora Chatty Chayloss Chedaki Chiraneau Chiron Chorlen Chornarov
				Cody Clorr Clovis Clydno Clynn Cnorec Coburn Cobus Cogon Colin Collus Colsan Colten Colton Combo
				Conar Coorta Corin Coris Corlac Corso Corvan Corvin Corvis Corwin Corwyth Cote Cothron Cotin
				Coy Cravel Cravus Creb Creeve Corlis Crest Crimpler Crimson Crin Crion Criss Cristoff Crix
				Cronal Cronnin Cronus D'Ashewl D'krn D'Lew D'Vox D'vrosh Daal Dado Daek Dael Dahar Dahm Daiman
				Dainsom Daiv Dak Dakron Daksh Dalton Damarcus Damaris Damarus Dao Dapp Daquinn Dar Eesh
				Darda Dardan Darebì Dareg Darek Daren Dargus Daggath Darnov Daro Darok Darone Darr
				Darrik Darrok Darrst Darsin Darthyn Daruula Dash Datoo Dauren Davis Davon Davonn Davos Davrill
				Davrin Davu Dayja Daz Ramsis Bren Tan Jan Porro Cin Garven Cornelius Galen Dannl Ecclessis
				Debbikin Debeli Deegan Deerian Deevis Defain Deflaros Degoren Dehno Dekay Delevar Delfaros Dellin Dellocon
				Delpho Delquis Delsa Delund Delurin Demtri Den Denbar Dender Dendro Dendu Denetrus Denev Dengar Dengril
				Denid Denimoor Denin Denna Denner Dennik Deppo Derdram Derec Derish Derliff Dermeg Dermian Dern Dernas
				Derral Derrin Dervis Desanne Devoris Devotek Devron Devry Devsh DeWolf Dex Dezsetes Dhaiv Dhal Dindau Dindo
				Ding Dinge Dobbatek Dobberty Dobbs Dolan Dolgis Dolmen Dolo Dominaire Domm Donovan Donovarr Donsull
				Dorel-Kan Dorgat Dorian Dorja Dorn Dorotsech Dorrick Dortho Downey Draado Draahg Dracen Drage Dragen Drake
				Drayk Drazin Drek Duno Dree Dreed Drefin Dregg Drelosyn Drexel Dreyf Drezzel Drizan Dro Droman Dromath
				Drowl Drufeys Drummond Dubeh Dunari Dunaub Deadeye Duncan Durhant Durmat Durpin Durrei Dyshotto Dzoun
				Eagle Eanjer Earl Earsh Eben Eddon Edease Edern Edgar Edikar Egoreg Egron Eistern Ejai Eker Ekh Ekisp
				Ekov Elarles Eriquel Eris Erl Erloi Esano Escalion Ezil Ezrim Ezra Ein Eislo Jake Jango
				Eli Eliasb Eligyn Elior Elis Elizhis Elkin Ellsworth Elmel Elmyo ElrodElsar Elsek Emil Emir Emmens
				Endicott Enfield Enger Engret Emmett Emory Eoaq Eph Erdan Erelut Ergast Ergel Ergeron Eric Erik 
				Estopatrin Esture Estus Eth Ethen Etnam Etta Evad Evan Evax Evlen Evran Evun Evvin Ewane Exar Eyvind 
				Falltower Falrey Falsta Falch Falco Faarl Fable Faedral Far Faradin Faraire Fardi Farn Farng Farnis Farsal
				Farseam Fatulaine Fauler Feldon Feldron Feleea Fenn Fenris Fenson Feras Ferav Ferdoin Fermin Ferob Ferren
				Ferrid Ferrig Ferrin Ferron Ferrus Falesh Falk Fardi Fassio Fatulaine Dannl Februs Janus Fixer Florn Grizz Frobb Fruun		 
			}
			first_names_female = {
				Charratha Edaara Bella Callista Mara Seena Areta Cord Cyrinity Nile 
				Kiyfi Yasmyn Satra Shehish Sarna Krava Roxis Jessa Aqua Maryna Essaj Looriw 
				Teeana Raena Yula Vifi Vania Jeanida Aerena Nataya Bernarda Geela Lya Sirra Teris Terral Nile
				Riema Erri Shanna Lyra Pranda Luna Cally Viqi Hadriel Lora Selena Zana
				Tuija Railin Odona Shyon Walas Rylla Gara Delos Loretta Luhon Delinia
				Aama Shorya Melfina Davessi Larana Galatea Nola Syna Jauhzmynn Anen Orona
				Yana Zoras Ree Grace Tantema Myn Una Dalharil Losa Sovi Elaiza Vucora
				Ava Joi Stryka Tantema Shandra Kely Caileta Taia Siala Reki Maydla Bot
				Kiandra Xilaren Zulara Ren Karissa Roxis Haruko Jenna Liana Rale Megara
				Aria Akiva Nalah Erlem Nali Fallah Kotrana Aayla Kalira Arani Fama Lyyr
				Arcas Rane Nayru Zylas Vexa Kana Beru
			}
			second_names = { 
				Elysar Tisa Wahia Wezz Unic Tiovata Nakcohh Vitaan Mefrid Daklan Kenzee Joben Thegas Moryne Holgor 
				Ijaaz Tromans Sprax Knesos Magnus Lasek Artis Pax Corso Fallon Gundo Ruana Fremlin Thont 
				Creel Rar Dusat Kessler Shaia El-Theriam VanMeer Vrieska Calway Yamaguchi Riggers Brower Fash Nalto
				Stryder Dubreas Colsan Terrek Solon Merquise Varn Basai Froud
				Addacess Elysar Durgen Slahlvo Remex Madowki Hayes Ne Dago Greenback
				Stithta Ker Driet Klakk Reed Lefex Minn Irokini Roye Danakar Forin
				Glopio Shenuri Corcer Ordo Wan Tagratt Golladio Adramcsia Jetberg Noth
				Taske Komo Styles Angavel Tyrnith Yavoog Kelbier Racto Degor Tisa Magre
				Soylon Lerann Chro Finn Blackmoor Hwang Vekarr Thegas Amon Thiddex Durgen
				Selkayim Gilder Dhuramav Kodd Morin Kobylarz Tade Solman Bintaghr Marclonus
				Precore Solborne Bailer Katarn Anjek Dalgas Kast Airerith Halo Tade Clash
				Jalba Benn Haruhana Roice Lokar Korpil Turon Cavi Deece Braitano Garnik
				Tyandas Ka'Taramas Nuwest Forte Noshey Gyyll Tallon Lionne Adras Fremlin
				Peoly Damiel Chup-Pau Drayven Zaposug Bela-Trepada Defelice Monsula Baralein
				Denive Hodrren Xergo Sarat Vercet Ossaki Kappa Zerga Kopos Aldrecht Bohme
				Tull Eldrel Marh Darksword Krandor Montag Dimodan Starfallen Artis Fett
				Mohebbi Runalian Binette Laurent Mavr Ryra Truden Solo Lars
				Amedda Amidala Andrell Annon Antilles Atanna Baab Barc Bonteri
				Breemu Bremack Bronk Burtoni Cadaman Callen Clovis Ledwellow Dio
				Organa
			}
			regnal_first_names_male = {
				Kam Erek Arslan Lerak Morla Mas Wain Bail Gabrial Dewell Tanner Vi Rush Danry
			}
			regnal_first_names_female = {
				Callista Mara Seena Areta Padmé Fema Mina Bana Tyreca Halle Lexi Leia Alyssa Leanna Arya 
			}
			regnal_second_names = {
				Amedda Amidala Andrell Annon Antilles Atanna Baab Barc Bonteri
				Breemu Bremack Bronk Burtoni Cadaman Callen Clovis Ledwellow Dio						
				Panteer Alde Organa Frayus Cortess Killesa
			}
		}
	}	
}