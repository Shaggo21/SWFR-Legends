﻿### Republic names
Zygerrian = {
	randomized = no

	ship_names = {
		generic = {
			"False Manumission" Thralltaker Widowmaker "Helot's Brand" "Branding Iron" Abductor Kidnapper Ransom Skinner Separator Auctioneer "First Pick"
			Whelper Subjugator "Name Giver" "Whipping Post" "Serf Watcher" "Back Striper" "Slave Catcher" Enslaver Slavemaker Overseer "Mongrel's Lash"
			Lashbearer "Scarlet Scourge" Flagellant Strapper "Bloody Ruin" "Only Choice" "Scourge Queen" "Ninth Tail" Dominator Taskmaster Taskmistress
			Disciplinarian Discipliner Stickler "Wind of Zygerria" "Red Hand" Brander "Chattel Watcher" Disenfranchiser Hierarch "Middle Passage" Oppressor
			Segregator Subordinator Voyager Zong Flayer Excoriator Terrorizer Choiceless Hopeless Taker "Zygerria's  Due" Mantracker Dominant Alpha Silverback
			Snatcher "Pound of Flesh" Undertaker "Grave Warden" "Chain Maker" Shackler Gouger Pursuer Gaoler Stalker Trapper Huntsman Chaser Flusher Hunter
			"Hounding One" Broodmare "Leash Holder" Lasher "Collared King" Slavekin "Painful Lesson" Restrainer Controller "Chain of Algernon" Roper Suppressor
			Hamperer "Thunder Kit" Powerless Weeper Screamer Wicked Malignant "Natural Order" Ruiner Damane "Salt-Wife" Taken Downfall "Fallen One" "Tear Drop"
			"Slave Hewer" "Belt Hand" Master Flogger "Hide Tanner" Thrasher Cancer Mortuary Morbid Sadist Owner Locust Pillager "Lady of Raids" Barbarous Corsair
			"Brass Bull" Victimizer Suffocant Repellant "Second Cull" Brezak "Doom of Guandura" Occupier Harasser Tormentor Persecutor "Cruel Fate" Tracker
			"Ruin of Ryloth" "Binding Light" Manhunt Inflicter Nemesis Abuser Grappler Bondage Inescapable Imminent Compeller Compulsory Foregone Menace Splinterer
			Outfoxed Molec Scintel Gemen "Faith of D'Nar" "M'rch's Targe" Thanda Scourge Listehol Armin Pr'ollerg Tynemark Tecora "Lek-Snapper" Cannibal Apex
			Pinnacle Dominant Undisputed Fleshmonger "Hand Taker" "Servus Custode" Desolator Decimator Punishment Punisher "Collective Punishment" Torturer
			Bondmaker Longfang Howler Yowler Burdener Possessor Retribution Discipliner "Harsh Truth" "First Warning" Reaver Marauder "Void Wolf" "Void Rat"
			Plunderer Quarterer Gallows Antagonizer Encager "Fated Cage" "Iron Cell" "Cagebringer" Confinement Imprisoner Mourner Violator Detainer Exploiter
			Poacher Handler Constrainer Constrictor Strangler "Iron Bridle" Arago Rabid 
		}
		swp_subjugator = {
			"Helot's Brand" Thralltaker
		}
		science = {
			Crescent Diligent Zephyr Enigma Enquirer Scylla Arbiter Asperity Eminence Astute Alacrity Satyr Prospero Untiring Zenith Torch Remembrance Pilgrim
			Trailblazer Vagabond Wayfarer Ambassador Emissary Nomad Pathfinder Wanderer Traveler
		}
		colonizer = {
			Godspeed Prophecy "Manifest Destiny" Virtue Potential Expansion "Queenly Spirit" Prosperity Summit Pinnacle Seeder Harvest Creator Beholder Exodus
			Foundation Scion Progeny Obligation Purpose Provider "Shining City" Acquirer Cultivator "Dawn of Civilization" Purity Community Brotherhood
		}
		sponsored_colonizer = { # needed when there are no generic names
			Godspeed Prophecy "Manifest Destiny" Virtue Potential Expansion "Pioneer Spirit" Prosperity Summit Pinnacle Seeder Harvest Creator Beholder Exodus
			Foundation Scion Progeny Obligation Purpose Provider "Shining City" Acquirer Cultivator "Dawn of Civilization" Purity Community Brotherhood
		}
		constructor = {
			Industry Helot Serf Laborer Builder Craftsman Peasant Engineer Promise Service Shelter Beast Creator Developer Forger Foundry
			Factory Anvil Pillar Architect Accommodator
		}
		transport = {
			"Landing Craft"
		}
		

		military_station_xq1 = {
			"Outpost 1" "Outpost 2" "Outpost 3" "Outpost 4" "Outpost 5" "Outpost 6" "Outpost 7" "Outpost 8" "Outpost 9" "Outpost 10" "Outpost 11" "Outpost 12"
			"Outpost 13" "Outpost 14" "Outpost 15" "Outpost 16" "Outpost 17" "Outpost 18" "Outpost 19" "Outpost 20" "Outpost 21" "Outpost 22" "Outpost 23"
			"Outpost 24" "Outpost 25" "Outpost 26" "Outpost 27" "Outpost 28" "Outpost 29" "Outpost 30" "Outpost 31" "Outpost 32" "Outpost 33" "Outpost 34"
			"Outpost 35" "Outpost 36" "Outpost 37" "Outpost 38" "Outpost 39" "Outpost 40" "Outpost 41" "Outpost 42" "Outpost 43" "Outpost 44" "Outpost 45"
			"Outpost 46" "Outpost 48" "Outpost 49" "Outpost 50" "Outpost 51" "Outpost 52" "Outpost 53" "Outpost 54" "Outpost 55" "Outpost 56" "Outpost 57"
			"Outpost 58" "Outpost 59" "Outpost 60" "Outpost 61" "Outpost 62" "Outpost 63" "Outpost 64" "Outpost 65" "Outpost 66" "Outpost 67" "Outpost 68"
			"Outpost 69" "Outpost 70"
		}
		military_station_xq2 = {
			"Outpost 1" "Outpost 2" "Outpost 3" "Outpost 4" "Outpost 5" "Outpost 6" "Outpost 7" "Outpost 8" "Outpost 9" "Outpost 10" "Outpost 11" "Outpost 12"
			"Outpost 13" "Outpost 14" "Outpost 15" "Outpost 16" "Outpost 17" "Outpost 18" "Outpost 19" "Outpost 20" "Outpost 21" "Outpost 22" "Outpost 23"
			"Outpost 24" "Outpost 25" "Outpost 26" "Outpost 27" "Outpost 28" "Outpost 29" "Outpost 30" "Outpost 31" "Outpost 32" "Outpost 33" "Outpost 34"
			"Outpost 35" "Outpost 36" "Outpost 37" "Outpost 38" "Outpost 39" "Outpost 40" "Outpost 41" "Outpost 42" "Outpost 43" "Outpost 44" "Outpost 45"
			"Outpost 46" "Outpost 48" "Outpost 49" "Outpost 50" "Outpost 51" "Outpost 52" "Outpost 53" "Outpost 54" "Outpost 55" "Outpost 56" "Outpost 57"
			"Outpost 58" "Outpost 59" "Outpost 60" "Outpost 61" "Outpost 62" "Outpost 63" "Outpost 64" "Outpost 65" "Outpost 66" "Outpost 67" "Outpost 68"
			"Outpost 69" "Outpost 70"
		}
		military_station_golan1 = {
			"SpaceGun Victory" "SpaceGun Success" "SpaceGun Supremacy" "SpaceGun Superior" "SpaceGun Adventure"
			"SpaceGun Ascendancy" "SpaceGun Paramount" "SpaceGun Preponderous" "SpaceGun Exemplary" "SpaceGun Eximious" "SpaceGun Pertinacity"
			"SpaceGun Acclaim" "SpaceGun Honor" "SpaceGun Renown" "SpaceGun Glory" "SpaceGun Superb" "SpaceGun Magnificence"
			"SpaceGun Majestic" "SpaceGun Splendor" "SpaceGun Paradigm" "SpaceGun Flawless" "SpaceGun Consummate" "SpaceGun Merit"
			"SpaceGun Perfection" "SpaceGun Virtue" "SpaceGun Advantage" "SpaceGun Rectitude" "SpaceGun Incorruptible"
			"SpaceGun Integrity" "SpaceGun Justice" "SpaceGun Probity" "SpaceGun Prudence" "SpaceGun Purity" "SpaceGun Resolute"
			"SpaceGun Dignity" "SpaceGun Distinction" "SpaceGun Excellence" "SpaceGun Faithful" "SpaceGun Discipline" "SpaceGun Steadfast"
			"SpaceGun Recompense" "SpaceGun Requiem" "SpaceGun Courage" "SpaceGun Valor" "SpaceGun Gallant"
			"SpaceGun Daring" "SpaceGun Constancy" "SpaceGun Perseverence" "SpaceGun Tenacity" "SpaceGun Sedulity" "SpaceGun Diligence"
		}
		military_station_golan2 = {
			"SpaceGun Victory" "SpaceGun Success" "SpaceGun Supremacy" "SpaceGun Superior" "SpaceGun Adventure"
			"SpaceGun Ascendancy" "SpaceGun Paramount" "SpaceGun Preponderous" "SpaceGun Exemplary" "SpaceGun Eximious" "SpaceGun Pertinacity"
			"SpaceGun Acclaim" "SpaceGun Honor" "SpaceGun Renown" "SpaceGun Glory" "SpaceGun Superb" "SpaceGun Magnificence"
			"SpaceGun Majestic" "SpaceGun Splendor" "SpaceGun Paradigm" "SpaceGun Flawless" "SpaceGun Consummate" "SpaceGun Merit"
			"SpaceGun Perfection" "SpaceGun Virtue" "SpaceGun Advantage" "SpaceGun Rectitude" "SpaceGun Incorruptible"
			"SpaceGun Integrity" "SpaceGun Justice" "SpaceGun Probity" "SpaceGun Prudence" "SpaceGun Purity" "SpaceGun Resolute"
			"SpaceGun Dignity" "SpaceGun Distinction" "SpaceGun Excellence" "SpaceGun Faithful" "SpaceGun Discipline" "SpaceGun Steadfast"
			"SpaceGun Recompense" "SpaceGun Requiem" "SpaceGun Courage" "SpaceGun Valor" "SpaceGun Gallant"
			"SpaceGun Daring" "SpaceGun Constancy" "SpaceGun Perseverence" "SpaceGun Tenacity" "SpaceGun Sedulity" "SpaceGun Diligence"
		}
		military_station_golan3 = {
			"SpaceGun Victory" "SpaceGun Success" "SpaceGun Supremacy" "SpaceGun Superior" "SpaceGun Adventure"
			"SpaceGun Ascendancy" "SpaceGun Paramount" "SpaceGun Preponderous" "SpaceGun Exemplary" "SpaceGun Eximious" "SpaceGun Pertinacity"
			"SpaceGun Acclaim" "SpaceGun Honor" "SpaceGun Renown" "SpaceGun Glory" "SpaceGun Superb" "SpaceGun Magnificence"
			"SpaceGun Majestic" "SpaceGun Splendor" "SpaceGun Paradigm" "SpaceGun Flawless" "SpaceGun Consummate" "SpaceGun Merit"
			"SpaceGun Perfection" "SpaceGun Virtue" "SpaceGun Advantage" "SpaceGun Rectitude" "SpaceGun Incorruptible"
			"SpaceGun Integrity" "SpaceGun Justice" "SpaceGun Probity" "SpaceGun Prudence" "SpaceGun Purity" "SpaceGun Resolute"
			"SpaceGun Dignity" "SpaceGun Distinction" "SpaceGun Excellence" "SpaceGun Faithful" "SpaceGun Discipline" "SpaceGun Steadfast"
			"SpaceGun Recompense" "SpaceGun Requiem" "SpaceGun Courage" "SpaceGun Valor" "SpaceGun Gallant"
			"SpaceGun Daring" "SpaceGun Constancy" "SpaceGun Perseverence" "SpaceGun Tenacity" "SpaceGun Sedulity" "SpaceGun Diligence"
		}
		military_station_fleetop = {
			Victory Success Supremacy Superior Adventure Ascendancy Warren Preponderous Exemplary Eximious
			Acclaim Manbreaker Renown Glory Superb Magnificence Majestic Splendor Paradigm Flawless Consummate Merit
			Perfection Virtue Advantage Rectitude Server Integrity Justice Probity Prudence Purity Resolute
			Dignity Distinction Excellence Faithful Discipline Recompense Requiem Courage Valor Gallant
			Daring Constancy Perseverence Tenacity Sedulity Diligence Pertinacity Steadfast Impregnable Impenetrable
			Unbreakable Implacable Impervious Unswerving Unyielding
		}
		military_station_small = {
			Citadel Savage Gateway Watchpost Tower Garrison Paraphet Manbroke Severed Spithead Spiteful Royal Oulsbo Liverlet "Void Flow" "Starlight Roads" Stardust Wails
			Mournharbor Blackwatch "Tyrant's View" Blockhouse Jepooni Tahala Kilitwijaro Awroot Eberet "Champion Hall" Fujett Poeympus Sinal Roseamb Roombar Farpoint "Red Star" "Mount Forlorn"
			Tharsis Syrial Redhett Bowyer Perrit Kovabro Moorete Moorsom Heartrend "Grey Gables" Takala Tanaka Tojoro Miranda Hootsport Kimicum Sklallam Brettbridge Hipalla Votalia Poremkon
			Zarana Hyattskill Vilne Foreyla Tefle Hawke Hermakk "Half Pound" King Queen Empress Crown Chichian Essen Kornilov Monarch Triville Vohort Suppro Gambitin Lezzovo
		}
		ion_cannon = {
			Bombard Ballista Catapult Harpoon Polybolos Culverin
		}
	}
	ship_class_names = {
		swp_cr90_corvette = { "CR-90" }
		swp_jehaveyir_corvette = { "Jehavey'ir" }
		swp_lupus_corvette = { Lupus }
		swp_early_asdroni_corvette = { Asdroni }
		swp_customs_corvette = { Customs }
		swp_hammerhead_corvette = { Hammerhead }
		swp_scurge_corvette = { Scurge }
		swp_krayt_corvette = { Krayt }
		swp_diamond_corvette = { Diamond }
		swp_etti_corvette = { Etti }
		swp_asdroni_corvette = { "Asdroni II" }
		swp_vigil_corvette = { Vigil }
		swp_dp20_corvette = { DP20 }
		swp_adv_asdroni_corvette = { "Asdroni III" }
		swp_raider_corvette = { Raider }
		swp_dornean_corvette = { Dornean }
		swp_crusader_corvette = { Crusader }
		swp_marauder_corvette = { Marauder }
		swp_arquitens_frigate = { Arquitens }
		swp_kiltirin_frigate = { Kiltirin }
		swp_pelta_frigate = { Pelta }
		swp_pinnance_frigate = { Pinnace }
		swp_nova_frigate = { Nova }
		swp_vigilance_frigate = { Vigilance }
		swp_neb_b_frigate = { "Nebulon-B" }
		swp_cg273_frigate = { "CG-273" }
		swp_kariek_frigate = { Kariek }
		swp_escort_carrier_frigate = { "Ton-Falk" }
		swp_quasar_frigate = { "Quasar Fire" }
		swp_lancer_frigate = { Lancer }
		swp_teroch_frigate = { Teroch }
		swp_neb_b2_frigate = { "Nebulon-B2" }
		swp_adv_nova_frigate = { "Nova II" }
		swp_adv_vigilance_frigate = { "Vigilance II" }
		swp_velox_frigate = { Velox }
		swp_mc30c_frigate = { MC30c }
		swp_vengeance_frigate = { Vengeance }
		swp_adv_kariek_frigate = { "Kariek II" }
		swp_cc7700_frigate = { "CC-7700" }
		swp_victory2_cruiser = { "Victory Mark 2" }
		swp_wavecrest_cruiser = { Wavecrest }
		swp_early_battledragon_cruiser = { "Battle Dragon" }
		swp_nuruodo_cruiser = { Nuruodo }
		swp_sabaoth_cruiser = { Sabaoth }
		swp_kontos_cruiser = { Kontos }
		swp_mc40_cruiser = { MC40 }
		swp_battledragon_cruiser = { "Battle Dragon II" }
		swp_broadside_cruiser = { Broadside }
		swp_protector_cruiser = { Protector }
		swp_warlord_cruiser = { Warlord }
		swp_gladiator_cruiser = { Gladiator }
		swp_beviin_cruiser = { Beviin }
		swp_liberator_cruiser = { Liberator }
		swp_adv_battledragon_cruiser = { "Battle Dragon III" }
		swp_adv_nuruodo_cruiser = { "Nuruodo II" }
		swp_immobilizer418_cruiser = { "Immobilizer 418" }
		swp_int_battledragon_cruiser = { "Battle Dragon IV" }
		swp_massias_cruiser = { Massias }
		swp_dreadnought_heavy_cruiser = { Dreadnought }
		swp_kyramud_heavy_cruiser = { Kyramud }
		swp_munificent_heavy_cruiser = { Munificent }
		swp_prosvoli_heavy_cruiser = { Prosvoli }
		swp_acclamator_heavy_cruiser = { Acclamator }
		swp_lictor_heavy_cruiser = { Lictor }
		swp_bulwark_heavy_cruiser = { Bulwark }
		swp_vsd1_heavy_cruiser = { "Victory I" }
		swp_assault1_heavy_cruiser = { "Assault Frigate I" }
		swp_bulk_heavy_cruiser = { "Bulk" }
		swp_vindicator_heavy_cruiser = { Vindicator }
		swp_dauntless_heavy_cruiser = { Dauntless }
		swp_captor_heavy_cruiser = { Captor }
		swp_auriette_heavy_cruiser = { "Au'riette" }
		swp_mc55_heavy_cruiser = { MC55 }
		swp_afthonia_heavy_cruiser = { Afthonia }
		swp_vsd2_heavy_cruiser = { "Victory II" }
		swp_beroya_heavy_cruiser = { Beroya }
		swp_assault2_heavy_cruiser = { "Assault Frigate II" }
		swp_hapan_vsd2_heavy_cruiser = { "HRN Victory II" }
		swp_adv_prosvoli_heavy_cruiser = { "Prosvoli II" }
		swp_venator_star_destroyer = { Venator }
		swp_centurion_star_destroyer = { Centurion }
		swp_mc75_star_cruiser = { MC75 }
		swp_kandosii_destroyer = { Kandosii }
		swp_corellian_destroyer = { Corellian }
		swp_providence_destroyer = { Providence }
		swp_syndic_destroyer = { Syndic }
		swp_recusant_destroyer = { Recusant }
		swp_invincible_destroyer = { Invincible }
		swp_imperial1_star_destroyer = { "Imperial I" }
		swp_hapan_imperial1_star_destroyer = { "HRN Imperial I" }
		swp_procursator_star_destroyer = { Procursator }
		swp_mc80a_star_cruiser = { MC80a }
		swp_dh_omni_destroyer = { "DH-Omni" }
		swp_aggressor_destroyer = { Aggressor }
		swp_adv_syndic_destroyer = { "Syndic II" }
		swp_mc80b_star_cruiser = { "MC80 Liberty" }
		swp_keldabe_destroyer = { Keldabe }
		swp_adv_corellian_destroyer = { "Corellian II" }
		swp_chaf_destroyer = { Chaf }
		swp_imperial2_star_destroyer = { "Imperial II" }
		swp_hapan_imperial2_star_destroyer = { "HRN Imperial II" }
		swp_ascendancy_star_destroyer = { Ascendancy }
		swp_tector_star_destroyer = { Tector }
		swp_interdictor_sd = { Dominator }
		swp_allegiance_battlecruiser = { Allegiance }
		swp_lucrehulk_battlecruiser = { Lucrehulk }
		swp_phalanx_battlecruiser = { Phalanx }
		swp_titan_battlecruiser = { Titan }
		swp_mc80_home1_battlecruiser = { "MC80 Independence" }
		swp_star_home_battlecruiser = { "Star Home" }
		swp_subjugator = { Subjugator }
		swp_bellator = { Bellator }
		swp_solyc_verd = { "Sol'yc Verd" }
		swp_executor = { Executor }
	}
	fleet_names = {		
		sequential_name = "%O% War Pack" 
	}
	army_names = {
		defense_army = {
			sequential_name = "%O% Planetary Guard"
		}
		assault_army = {
			sequential_name = "%O% Raid Pack"
		}
		slave_army = {
			sequential_name = "%O% Indentured Rifles"
		}
		clone_army = {
			sequential_name = "%O% Clone Army"
		}
		robotic_army = {
			sequential_name = "%O% Hunter-Killer Group"
		}
		robotic_defense_army = {
			sequential_name = "%O% Ground Defense Matrix"
		}
		psionic_army = {
			sequential_name = "%O% Psi Commando"
		}
		xenomorph_army = {
			sequential_name = "%O% Bio-Warfare Division"
		}
		gene_warrior_army = {
			random_names = {
				"SARC-A 'Gladiators'" "SARC-B 'Widowmakers'" "SARC-C 'Immortals'" "SARC-D 'Berserkers'" "SARC-E 'Assassins'"
				"SARC-F 'Reapers'" "SARC-G 'Nighthawks'" "SARC-H 'Desperados'" "SARC-I 'Grey Knights'" "SARC-J 'Roughnecks'" 
			}
			sequential_name = "%O% Bio-Engineered Squadron"
		}
		occupation_army = {
			sequential_name = "%O% Garrison Force"
		}
		robotic_occupation_army = {
			sequential_name = "%O% Mechanized Garrison"
		}
		primitive_army = {
			sequential_name = "Primitive Army %C%"
		}
		industrial_army = {
			sequential_name = "Industrial Army %C%"
		}
		postatomic_army = {
			sequential_name = "Post-Atomic Army %C%"
		}
		imp_garrison_01 = { sequential_name = "%O% Stormtrooper Garrison" }
		reb_garrison_01 = { sequential_name = "%O% Rebel Trooper Garrison" }
		hut_garrison_01 = { sequential_name = "%O% Hutt Garrison" }
		man_garrison_01 = { sequential_name = "%O% Mandalorian Garrison" }
		cor_garrison_01 = { sequential_name = "%O% Corellian Garrison" }
		cis_garrison_01 = { sequential_name = "%O% CIS Garrison" }
		csa_garrison_01 = { sequential_name = "%O% CSA Garrison" }
		hap_garrison_01 = { sequential_name = "%O% Hapes Garrison" }
		oth_garrison_01 = { sequential_name = "%O% Militia Garrison" }
		dro_garrison_01 = { sequential_name = "%O% Droid Garrison" }
		imp_infantry_1 = { sequential_name = "%O% Stormtroopers" }
		imp_infantry_2 = { sequential_name = "%O% Shock Troopers" }
		imp_infantry_3 = { sequential_name = "%O% Dark Troopers" }
		reb_infantry_1 = { sequential_name = "%O% Rebel Troopers" }
		reb_infantry_2 = { sequential_name = "%O% Assault Troopers" }
		reb_infantry_3 = { sequential_name = "%O% Heavy Assault Troopers" }
		hut_infantry_1 = { sequential_name = "%O% Hutt Cartel Gang" }
		hut_infantry_2 = { sequential_name = "%O% Hutt Elite Mercenaries" }
		hut_infantry_3 = { sequential_name = "%O% Hutt Deshade Mercenaries" }
		man_infantry_1 = { sequential_name = "%O% Mandalorian Warriors" }
		man_infantry_2 = { sequential_name = "%O% Mandalorian Jet Warriors" }
		man_infantry_3 = { sequential_name = "%O% Mandalorian Elite " }
		cor_infantry_1 = { sequential_name = "%O% CorSec Troopers" }
		cis_infantry_1 = { sequential_name = "%O% Corporate Enforcers" }
		csa_infantry_1 = { sequential_name = "%O% Espos Troopers" }
		csa_infantry_2 = { sequential_name = "%O% Espos Elite Troopers" }
		csa_infantry_3 = { sequential_name = "%O% Espos State Guard" }
		hap_infantry_1 = { sequential_name = "%O% Hapes Militia" }
		hap_infantry_2 = { sequential_name = "%O% Fountain Gunners" }
		hap_infantry_3 = { sequential_name = "%O% Fountain Elite" }
		oth_infantry_1 = { sequential_name = "%O% Mercenaries" }
		oth_infantry_2 = { sequential_name = "%O% Heavy Mercenary Battalion" }
		oth_infantry_3 = { sequential_name = "%O% Mandalorian Mercenaries" }
		clo_infantry_1 = { sequential_name = "%O% Clone Troopers" }
		dro_infantry_1 = { sequential_name = "%O% B1 Battle Droids " }
		dro_infantry_2 = { sequential_name = "%O% B2 Super Battle Droids " }
		dro_infantry_3 = { sequential_name = "%O% Droideka Detachment" }
		imp_support_1 = { sequential_name = "%O% Light Walkers Detachment" }
		imp_support_3 = { sequential_name = "%O% Adv. Light Walkers Detachment" }
		reb_support_1 = { sequential_name = "%O% T-2 Tank Detachment" }
		hut_support_1 = { sequential_name = "%O% Hutt Barges Detachment" }
		man_support_1 = { sequential_name = "%O% Mandalorian Auxilary Detachment" }
		hap_support_1 = { sequential_name = "%O% Fountain Support Vehicles" }
		oth_support_1 = { sequential_name = "%O% Infantry Support Vehicles" }
		oth_support_2 = { sequential_name = "%O% HAVw A6 Juggernaut" }
		dro_support_1 = { sequential_name = "%O% DSD1 Droid Detachment" }
		imp_specops_2 = { sequential_name = "%O% Storm Commando" }
		imp_specops_3 = { sequential_name = "%O% Shadow Commando" }
		reb_specops_2 = { sequential_name = "%O% Rebel Commando" }
		reb_specops_3 = { sequential_name = "%O% Rebel Elite Commando" }
		hut_specops_2 = { sequential_name = "%O% Cartel Sabateurs" }
		man_specops_2 = { sequential_name = "%O% Mandalorian Commando" }
		man_specops_3 = { sequential_name = "%O% Mandalorian Elite Commando" }
		cor_specops_2 = { sequential_name = "%O% Corellian Infiltrators" }
		csa_specops_2 = { sequential_name = "%O% Espos SpecOps" }
		hap_specops_2 = { sequential_name = "%O% Fountain Snipers" }
		hap_specops_3 = { sequential_name = "%O% Her Majesty's Select Commandos" }
		oth_specops_2 = { sequential_name = "%O% Bounty Hunters" }
		oth_specops_3 = { sequential_name = "%O% Clone Commandos" }
		dro_specops_2 = { sequential_name = "%O% BX Droid Commando" }
		dro_specops_3_0 = { sequential_name = "%O% SD-4K Assasin Droids" }
		dro_specops_3_1 = { sequential_name = "%O% IG-series Assasin Droids" }
		imp_armor_1 = { sequential_name = "%O% 2-M Hovertank Detachment" }
		imp_armor_2 = { sequential_name = "%O% AT-AT Detachment" }
		imp_armor_3 = { sequential_name = "%O% AT-AT Advanced Detachment" }
		reb_armor_1 = { sequential_name = "%O% T-3B Tank Detachment" }
		reb_armor_2 = { sequential_name = "%O% Legacy AT-TE Detachment" }
		reb_armor_3 = { sequential_name = "%O% T-4B Modified Tank Detachment" }
		hut_armor_1 = { sequential_name = "%O% WLO-5 Tank Detachment" }
		man_armor_1 = { sequential_name = "%O% MHT-1 Detachment" }
		man_armor_2 = { sequential_name = "%O% MHT-2 Detachment" }
		cis_armor_1 = { sequential_name = "%O% AAT-1 Tank Detachment" }
		hap_armor_1 = { sequential_name = "%O% Fountain Armor Detachment" }
		oth_armor_1 = { sequential_name = "%O% TX-225 Assualt Tank Detachment" }
		oth_armor_2 = { sequential_name = "%O% Salvaged AT-TE Detachment" }
		oth_armor_3 = { sequential_name = "%O% Salvaged AT-HE Detachment" }
		dro_armor_2 = { sequential_name = "%O% OG-9 Spider Droid Detachment" }
		dro_armor_3 = { sequential_name = "%O% Super Droid Tank Detachment" }
		imp_arty_2 = { sequential_name = "%O% SPMA Artillery Detachment" }
		imp_arty_3 = { sequential_name = "%O% SPHA Artillery Detachment" }
		reb_arty_2 = { sequential_name = "%O% MPTL Artillary Detachment" }
		reb_arty_3 = { sequential_name = "%O% HMPTL Artillary Detachment" }
		hut_arty_1 = { sequential_name = "%O% Artillery Barge Detachment" }
		man_arty_2 = { sequential_name = "%O% MHA Artillery Detachment" }
		hap_arty_2 = { sequential_name = "%O% MLC-4 Artillery Tank" }
		oth_arty_1 = { sequential_name = "%O% MLC-3 Light Artillery Tank" }
		oth_arty_3 = { sequential_name = "%O% SPHA-T Artillery Detachment" }
		dro_arty_2 = { sequential_name = "%O% J-1 Proton Cannon Detachment" }
		dro_arty_3 = { sequential_name = "%O% Hellfire Droid Detachment" }
		imp_arial_1 = { sequential_name = "%O% TIE Squadron" }
		imp_arial_2 = { sequential_name = "%O% Lambda Shuttle Gunship Squadron" }
		reb_arial_1 = { sequential_name = "%O% T-47 Airspeeder Squadron" }
		reb_arial_2 = { sequential_name = "%O% Tachyon Airspeeder Squadron" }
		hut_arial_1 = { sequential_name = "%O% Sandspeeder Squadron" }
		man_arial_1 = { sequential_name = "%O% Mandalorian SkyFighter Squadron" }
		cor_arial_2 = { sequential_name = "%O% VCX-series Starfighter Squadron" }
		cis_arial_2 = { sequential_name = "%O% Mechanized Assault Flyer" }
		csa_arial_2 = { sequential_name = "%O% R-41 Starchaser Squadron" }
		hap_arial_1 = { sequential_name = "%O% My'til Fighter Squadron" }
		oth_arial_2_0 = { sequential_name = "%O% Turbostorm-class Gunship" }
		oth_arial_2_1 = { sequential_name = "%O% Legacy LAAT Gunship" }
		oth_arial_1 = { sequential_name = "%O% Storm IV Twin-Pod Cloud Car" }
		dro_arial_1 = { sequential_name = "%O% Vulture Droid Airwing" }
		dro_arial_3 = { sequential_name = "%O% HMP Droid Gunship" }
		imp_homorguard = { sequential_name = "%O% Honor Guard" }
		reb_bothan = { sequential_name = "%O% Bothan Operatives" }
		reb_wookie = { sequential_name = "%O% Wookie Battlegroup" }
		cis_magnaguard = { sequential_name = "%O% Magnaguard" }
		hap_royalguard = { sequential_name = "%O% Hapan Royal Guard" }
		oth_geonos = { sequential_name = "%O% Geonosian" }
		fortification_1 = { sequential_name = "%O% DF9 Cannon Fortifications" }
		fortification_3 = { sequential_name = "%O% Turbolaser Cannon Fortifications" }
		cre_rancor = { sequential_name = "%O% Rancor" }
		cre_ancientrancor = { sequential_name = "%O% Ancient Rancor" }
		cre_acklay = { sequential_name = "%O% Acklay " }
		cre_primalbeasts = { sequential_name = "%O% Primal Beasts" }
		cre_dusturbedbeasts = { sequential_name = "%O% Disturbed Beasts" }
		imp_occupation_01 = { sequential_name = "%O% Stormtrooper Occupation Garrison" }
		reb_occupation_01 = { sequential_name = "%O% Rebel Trooper Occupation Garrison" }
		hut_occupation_01 = { sequential_name = "%O% Hutt Occupation Garrison" }
		man_occupation_01 = { sequential_name = "%O% Mandalorian Occupation Garrison" }
		cor_occupation_01 = { sequential_name = "%O% Corellian Occupation Garrison" }
		cis_occupation_01 = { sequential_name = "%O% CIS Occupation Garrison" }
		csa_occupation_01 = { sequential_name = "%O% CSA Occupation Garrison" }
		hap_occupation_01 = { sequential_name = "%O% Hapes Occupation Garrison" }
		oth_occupation_01 = { sequential_name = "%O% Militia Occupation Garrison" }
		dro_occupation_01 = { sequential_name = "%O% Droid Occupation Garrison" }
	}

	### PLANETS
	planet_names = {
		generic = {
			names = {
			}
		}
	}

	### CHARACTERS
	character_names = {
		names1 = {			
			first_names_male = {
				Agruss Kadavo Rajim Darts Farzala Ikis Ingan Jorec Atai Sono Omus "M'rch" Mard Hordem Oksan Onyx Scroon
				Slego Teek Atlee Marko Zolghast Strata Lancer Zeak Niuk Lavidean Tanizandar Japla Aran Rail Zeven Yakown 
				Sibar Sair Klai Jonal Imay Traest Simo Kareth Riyan Maynus Boceta Agnook Silus Rowan Coren Jett Linth Annok
				Ulas Fenn Tal Vrok Torin Imay Boone Davith Jorgen Bertos Ress Malius
				Kale Cregan Galavo Zerk Jinn-Tu Jaryn Gentti Yu-Sien Akela Arslan
				Sidias Haken Biran Vidown Arejala Ithan Cul-utaan Thoja Cron Mossi
				Abric Darg Gaman Naiad Herub Marius Torkin Caton Kane Versita
				Cric Foxdi Montacca Jospi Baz Kam Erek Arslan Lerak Morla Baxter
				Talan Thion Kannon Salir Caster Elden Faelgrom Jorwoo Raltharan
				Flash Waseem Bendak Kaz Montileu Gonoga Silus Galven Vincenzo Ric
				Rebar Jebediah Aramil Abraxis Delmi Caalin Rassth Adorn Syrus
				Zhar-Khan Adam Predor Tarlson Boaz Ingtar Gunn Raltharan Zerrid Bobecc
				Jelantos Jephego Svante Tesvalo Aeon Dekar Morech Melinjar Myt Murek				 
				}
			first_names_female = {
				Miraj MaDall Hask Tosan Mara Seena Areta Cord Cyrinity Nile 
				Kiyfi Masera Satra Shehish Sarna Krava Roxis Zarana Aqua Maryna Essaj Looriw 
				Teeana Raena Yula Vifi Vania Jeanida Aerena Nataya Bernarda Geela Lya Sirra Teris Terral Nihla
				Riema Erri Shanna Lyra Pranda Valya Cally Viqi Hadriel Lora Selena Zana
				Tuija Railin Odona Shyon Walas Rylla Gara Delos Loretta Luhon Delinia
				Aama Shorya Melfina Davessi Larana Galatea Nola Syna Jauhzmynn Anen Orona
				Yana Zoras Ree Grace Tantema Myn Una Dalharil Losa Sovi Elaiza Vucora
				Ava Joi Stryka Tantema Shandra Kely Caileta Taia Siala Reki Maydla Bota
				Kiandra Xilaren Zulara Ren Karissa Roxis Haruko Jenna Liana Rale Megala
				Aria Akiva Nalanna Erlem Nali Fallah Kotrana Aayla Kalira Arani Fama Lyyr
				Nomilla Rane Nayru Zylas Vexa Kana Meru
			}
			second_names = { 
				Armin "Pr'ollerg" "D'Nar" Stane Gemen Molec "Molec II" "Molec III" "Molec IV" Scintel Krill Tolla Daklan Taff Thanda Thegas Tyne Holgor 
				Ijaaz Acklej Sprax Knesos "Throll'ek" Lasek Artis Paxiyen Corso Fallon Gundo Ruana Fremlin Thont 
				Creel Rar Dusat Kessek Redback Al-Theriam VanZeer Vrieska Calwhip Yartok Yarous Brower Fash Nalto
				Stryder Stryken Kolek Velen Solon Merquise Varn Basai Froud
				Addacess Elysar Durgen Slahlvo Remex Madowki Hayes Ne Dago Stripeback
				Stithta Ker Driet Klakk Reed Lefex Minn Irokini Roye Danakar Forin
				Glopio Shenuri Corcer Or'ven Wan Tagratt Golladio Adramcsia Golandrik Noth
				Taske Komo Barntar Angavel Tyrnith Yavoog Kelbier Racto Degor Tisa Magre
				Soylon Lerann Chro Finn Blackmoor Hwang Vekarr Thegas Amon Thiddex Durgen
				Selkayim Gilder Dhuramav Kodd Morin Kobylarz Tade Solman Bintaghr Marclonus
				Precore Solborne Bailer Katarn Anjek Dalgas Kast Airerith Halord Tade Varlash
				Jalba Benn Haruhana Roice Lokar Korpil Turon Cavi Deece Braitano Garnik
				Tyandas Ka'Taramas Nuwest Forte Noshey Gyyll Tallon Liorre Adras Fremlin
				Peoly Damiel Pau Drayvek Zaposug Molec-Stane Bela-Trepada Defelice Monsula Baralein
				Denive Hodrren Xergo Sarat Vercet Ossaki Kappa Zerga Kopos Aldrecht Bohme
				Tull Eldrel Marh Darkwin Krandor Montag Dimodan Starkist Artis Veloor
				Mohebbi Runalian Binette Laurent Mavr Ryra Truden Molec-Tade Starkholov
				Amekka Avara Andrell Annon Anteron Atanna Baab Barc Bonteri
				Breemu Bremack Bronk Burtoni Cadaman Callen Ilisin Ledwellow Dio
			}
			regnal_first_names_male = {
				Atai Jorec Mard Hordem Sono D'var Agruss
			}
			regnal_first_names_female = {
				Miraj Miralla Miyanna Areta Milizeth Milerrena Millara
			}
			regnal_second_names = {
				Molec Scintel Alkema
			}
		}
	}	
}