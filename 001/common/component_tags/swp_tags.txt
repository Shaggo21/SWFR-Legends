# Component Tags
#
# Component Tags can be given scripted in component_templates and is used to group components together for the use of common modifiers
#

weapon_type_turbolaser
weapon_type_maser
weapon_type_ion_cannon
weapon_type_laser_cannon
weapon_type_maser_cannon
weapon_type_fighter
weapon_type_bomber
weapon_type_vong
weapon_role_fighter
weapon_role_bomber