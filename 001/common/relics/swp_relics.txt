@activation_cost = 150
@triumph_duration = 3600
@triumph_duration_short = 1800

############################################################################
#                                                                          #
#                          Force Relics                                    #
#                                                                          #
############################################################################

r_jedi_holocron = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_jedi_holocron"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		country_unity_produces_mult = 0.10
	}

	score = 1000

	active_effect = {
		custom_tooltip = jedi_holocron_tooltip
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration
			}	
			country_event = { id = swp_relic_event.30 }	
		}
	}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

r_sith_holocron = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_sith_holocron"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		country_unity_produces_mult = 0.10
	}

	score = 1000

	active_effect = {
		custom_tooltip = sith_holocron_tooltip
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration
			}	
			country_event = { id = swp_relic_event.40 }	
		}
	}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

r_nihilus_mask = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_nihilus_mask"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}

	triggered_country_modifier = {
		potential = {
			always = yes
		}
		country_unity_produces_mult = 0.05
	}

	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
	}

	score = 5000

	active_effect = {
		custom_tooltip = nihilus_mask_tooltip
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration
			}
			add_modifier = {
				modifier = "nihilus_bombardment" 
				days = @triumph_duration
			}	
		}
	}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

r_jedi_database = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_jedi_database"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		country_sr_focus_produces_mult = 0.05
	}

	score = 1000

	active_effect = {
		add_modifier = {
			modifier = "em_jedi_database" 
			days = @triumph_duration
		}
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration
			}		
		}
	}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

r_muur_talisman = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_muur_talisman"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		pop_growth_speed = 0.10
	}

	score = 1000

	active_effect = {
		custom_tooltip = muur_talisman_tooltip
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration
			}
			add_modifier = {
				modifier = "em_muur_talisman" 
				days = @triumph_duration
			}	
		}
	}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

r_mortis_dagger = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_mortis_dagger"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		country_unity_produces_mult = 0.075
	}

	score = 1000

	active_effect = {
		custom_tooltip = mortis_dagger_tooltip
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration
			}
			country_event = { id = swp_relic_event.90 }	
		}
	}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

r_uneti_sapling = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_uneti_sapling"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		country_sr_focus_produces_mult = 0.1
	}

	score = 1000

	active_effect = {
		
			custom_tooltip = uneti_sapling_tooltip
			custom_tooltip = relic_triumph_cooldown
			hidden_effect = {
				add_modifier = {
					modifier = "relic_activation_cooldown" 
					days = @triumph_duration
				}
				country_event = { id = swp_relic_event.50 }		
			}
		}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

# Ancient Jedi Texts
r_jedi_books = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_jedi_books"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		leader_skill_levels = 1
		species_leader_exp_gain = 0.25
	}

	score = 1000

	active_effect = {
		custom_tooltip = jedi_books_tooltip
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration
			}	
			country_event = { id = swp_relic_event.100 }
		}
	}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}
############################################################################
#                                                                          #
#                          General Relics                                  #
#                                                                          #
############################################################################

r_mandalore_mask = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_mandalore_mask"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		ship_weapon_damage = 0.05
		pop_ethic_militarist_attraction_mult = 0.15
		#Diplomacy with Mandos#
	}

	score = 1000

	active_effect = {
		custom_tooltip = mandalore_mask_tooltip
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration
			}
			country_event = { id = swp_relic_event.20 }	
		}
	}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

r_crystal_ball = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_crystal_ball"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		science_ship_survey_speed = 0.25
		ship_sensor_range_add = 2

	}

	score = 1000

	active_effect = {
		add_modifier = {
			modifier = "em_crystal_ball" 
			days = @triumph_duration
		}
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration
			}		
		}
	}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

r_coordinates_Katana = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_coordinates_Katana"
	sound = "relic_activation_the_surveyor"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		country_engineering_tech_research_speed = 0.1
	}

	score = 1000

	active_effect = {
		custom_tooltip = coordinates_Katana_tooltip
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration
			}
			country_event = { id = swp_relic_event.10 }	
		}
	}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

r_twilek_ark = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_twilek_ark"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		planet_pops_consumer_goods_upkeep_mult = -0.1
		#Twilek Happiness? WIP#
	}

	score = 1000

	active_effect = {
		add_modifier = {
			modifier = "em_twilek_ark" 
			days = @triumph_duration
		}
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration
			}		
		}
	}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

r_solo_dice = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_solo_dice"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		country_unity_produces_mult = 0.05
	}

	score = 1000
	
	active_effect = {
		custom_tooltip = solo_dice_tooltip
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration
			}	
			country_event = { id = swp_relic_event.60 }		
		}
	}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

r_zillo_scale = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_zillo_scale"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		pop_happiness = 0.05
	}

	score = 1000

	active_effect = {
		add_modifier = {
			modifier = "em_zillo_scales" 
			days = @triumph_duration
		}
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration
			}		
		}
	}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

r_power_gem = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_power_gem"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		ship_shield_mult = 0.10
	}

	score = 1000

	active_effect = {
		add_modifier = {
			modifier = "em_power_gem" 
			days = @triumph_duration
		}
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration
			}		
		}
	}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

r_wraith_box = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_wraith_box"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		all_technology_research_speed = 0.05
	}

	score = 1000

	active_effect = {
		add_modifier = {
			modifier = "em_wraith_box" 
			days = @triumph_duration
		}
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration
			}		
		}
	}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

r_victory_plaque = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_victory_plaque"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		planet_stability_add = 5
	}

	score = 1000

	active_effect = {
		custom_tooltip = victory_plaque_tooltip
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration
			}
			country_event = { id = swp_relic_event.80 }			
		}
	}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

r_alderaan_jewels = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_alderaan_jewels"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		diplo_weight_mult = 0.1
		envoys_add = 1
	}

	score = 1000

	active_effect = {
		add_modifier = {
			modifier = "em_alderaan_jewels" 
			days = @triumph_duration
		}
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration
			}		
		}
	}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

r_bacca_blade = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_bacca_blade"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		country_trust_growth = 0.50
	    #WIP +Diplomacy with Wookies#
	}

	score = 1000

	active_effect = {
		add_modifier = {
			modifier = "em_bacca_blade" 
			days = @triumph_duration
		}
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration
			}		
		}
	}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

r_roon_stone = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_roon_stone"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		country_energy_produces_mult = 0.05
	}

	score = 1000

	active_effect = {
		add_modifier = {
			modifier = "em_roonstone_modifier" 
			days = @triumph_duration
		}
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration
			}		
		}
	}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

r_teta_jewels = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_teta_jewels"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		country_unity_produces_mult = 0.05
		species_leader_exp_gain = 0.25
		diplo_weight_mult = 0.05
	}

	score = 1000

	active_effect = {
		add_modifier = {
			modifier = "em_teta_jewels" 
			days = @triumph_duration
		}
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration
			}		
		}
	}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

r_emperor_cane = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_emperor_cane"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		country_unity_produces_mult = 0.05
	}

	score = 1000

	active_effect = {
		add_modifier = {
			modifier = "em_emperor_cane" 
			days = @triumph_duration
		}
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration
			}		
		}
	}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

r_peace_globe = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_peace_globe"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		country_unity_produces_mult = 0.05
		diplo_weight_mult = 0.1
	}

	score = 1000

	active_effect = {
		add_modifier = {
			modifier = "em_peace_globe" 
			days = @triumph_duration
		}
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration
			}		
		}
	}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

r_deathwatch_helmet = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_deathwatch_helmet"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		country_unity_produces_mult = 0.05
		country_edict_cap_add = 1
		country_admin_cap_mult = 0.10
	}

	score = 1000

	active_effect = {
		add_modifier = {
			modifier = "em_deathwatch_helmet" 
			days = @triumph_duration
		}
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration
			}		
		}
	}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

r_darksaber = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_darksaber"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		country_unity_produces_mult = 0.10
	}

	score = 1000

	active_effect = {
		add_modifier = {
			modifier = "em_darksaber" 
			days = @triumph_duration
		}
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration
			}		
		}
	}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

r_sharkanoor = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_sharkanoor"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		planet_clear_blocker_time_mult = -0.50
	}

	score = 1000

	active_effect = {
		random_owned_planet = {
			limit = { is_capital = yes }
			add_modifier = {
				modifier = "pm_sharkak_noor" 
				days = @triumph_duration
			}
		}
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration
			}		
		}
	}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

r_dream_rock = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_dream_rock"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		pop_ethic_spiritualist_attraction_mult = 0.5
	}

	score = 1000

	active_effect = {
		add_modifier = {
			modifier = "em_dream_rock" 
			days = @triumph_duration
		}
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration
			}		
		}
	}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

r_rur_crystall = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_rur_crystall"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		planet_jobs_robotic_produces_mult = 0.15
	}

	score = 1000

	active_effect = {
		custom_tooltip = rur_crystal_tooltip
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration
			}
			country_event = { id = swp_relic_event.7 }	
		}
	}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

r_phylanx_transmitter = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_phylanx_transmitter"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		planet_jobs_robotic_produces_mult = 0.1
	}

	score = 1000

	active_effect = {
		custom_tooltip = phylanx_transmitter_tooltip
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration
			}
			country_event = { id = swp_relic_event.70 }		
		}
	}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

r_sith_holocron_nox = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_sith_holocron"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		planet_jobs_worker_produces_mult = 0.1
	}

	score = 1000

	active_effect = {
		custom_tooltip = sith_holocron_nox_tooltip
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration
		  }

		  add_modifier = {
			modifier = "em_nox_holocron" 
			days = @triumph_duration
		}
	}
}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

r_ones_mosaic = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_ones_mosaic"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		pop_ethics_shift_speed_mult = 1
		country_influence_produces_add = 1
		pop_government_ethic_attraction = 1
	}

	score = 1000

	active_effect = {
		add_modifier = {
			modifier = "em_ones_mosaic" 
			days = @triumph_duration
		}
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration
			}		
		}
	}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

r_isotope_reactor = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_isotope_reactor"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		country_base_energy_produces_add = 30
	}

	score = 1000

	active_effect = {
		custom_tooltip = isotope_reactor_tooltip
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration	
		  }
		  add_modifier = {
			modifier = "em_isotope_reactor" 
			days = @triumph_duration
		}
	}
}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}

r_hammer_station = {
	activation_duration = @triumph_duration
	portrait = "GFX_relic_hammerstation"
	sound = "relic_activation_miniature_galaxy"

	resources = {
		category = relics
		# Activation cost
		cost = {
			influence = @activation_cost
		}
	}
	
	ai_weight = {
		weight = 100
	}
	
	triggered_country_modifier = {
		potential = {
			always = yes
		}
		country_resource_max_alloys_add = 15000
		country_resource_max_sr_laser_cells_add = 15000
		country_resource_max_sr_starfighter_parts_add = 15000
		country_resource_max_sr_plasma_cells_add = 15000
		country_resource_max_sr_ordnance_add = 15000
	}

	score = 1000

	active_effect = {
		custom_tooltip = hammer_station_tooltip
		custom_tooltip = relic_triumph_cooldown
		hidden_effect = {
			add_modifier = {
				modifier = "relic_activation_cooldown" 
				days = @triumph_duration	
		  }
		  country_event = { id = swp_relic_event.110 }		
	}
}

	# Possible check for activation
	possible = {
		custom_tooltip = {
			fail_text = "requires_relic_no_cooldown"
			NOT = { has_modifier = relic_activation_cooldown }
		}
	}
}