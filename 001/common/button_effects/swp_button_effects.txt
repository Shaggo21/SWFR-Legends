
#Scope type varies depending on what is selected
# This = selected object or player country
# From = player country

swp_force_menu_button_effect = {
	potential = {
		exists = from
		from = { has_country_flag = has_institution }
	}
	allow = {
		always = yes
	}
	effect = {
		hidden_effect = {
			from = {
				country_event = {
					id = empire_institutions.10
				}
			}
		}
		custom_tooltip = swp_menu.tooltip
	}
}

swp_annex_button_effect = {
	potential = {
		exists = from
		from = { 
			has_federation = yes 
			is_federation_leader = yes
			
			federation = {
				has_federation_law = annexation_yes
			}
		}
	}
	allow = {
		from = { NOT = { has_country_flag = ANNEXATION_IN_PROGRESS }}
	}
	effect = {
		hidden_effect = {
			from = {
				country_event = {
					id = swp_fedev.1
				}
			}
		}
		custom_tooltip = swp_menu.tooltip
	}
}

swp_museum_button_effect = {
	potential = {
		exists = from
	}
	allow = {
		exists = from
	}
	effect = {
		hidden_effect = {
			from = {
				country_event = {
					id = swp_legacy.2000
				}
			}
		}
		## Make new custom tooltip
		## custom_tooltip = swp_menu.tooltip
	}
}

swp_remove_shipyard_effect = {
	potential = {
		exists = from
	}
	allow = {
		always = yes
	}
	# effect = {
	# 	hidden_effect = {
	# 		from = {
	# 			country_event = {
	# 				id = swp_shipyard_remove.1
	# 			}
	# 		}
	# 	}
	# 	custom_tooltip = swp_menu.tooltip
	# }
}
