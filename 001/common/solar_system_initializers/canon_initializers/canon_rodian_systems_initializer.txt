
@base_planet_dist = 25
@base_moon_dist = 15

#Rodia
rodia_system_initializer = {
	name = "Tyrius"
	class = "rl_standard_stars"
	flags = { rodian_homeworld rodian_start_system canon_map canon_map_shipyard_system }
	init_effect = { generate_home_system_resources = yes log = "rodian homeworld" }
	usage = custom_empire
	max_instances = 1
	planet = {
		name = "Tyrius"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		name = "Mikak"
		class = pc_molten
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Rodia"
		class = pc_tropical
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 15 max = 20 }
		has_ring = no
		starting_planet = yes
		deposit_blockers = none
		modifiers = none
		init_effect = { prevent_anomaly = yes set_planet_flag = planet_rodia }
		init_effect = {
			set_global_flag = rodian_homeworld_spawned
			if = {
				limit = { NOT = { any_country = { has_country_flag = rodian_clans } } }
				create_species = { 
				    name = "Rodian" 
				    class = ROD
				    portrait = rodian 
				    homeworld = THIS 
				    traits = { 
				        trait = "trait_rapid_breeders" 
				        trait = "trait_nomadic"
				        trait = "trait_fleeting"
				        ideal_planet_class = "pc_tropical" 
					} 
				}
				last_created_species = { save_global_event_target_as = rodianSpecies }
				create_country = {
					name = "NAME_rodian_clans"
					ship_prefix = " "
					type = default
					origin = "origin_swp_default"
					ignore_initial_colony_error = yes
					civics = { civic = "civic_rodian_scouts" civic = "civic_cutthroat_politics" }
					authority = auth_dictatorial
					name_list = "Rodian"
					ethos = { ethic = "ethic_authoritarian" ethic = "ethic_materialist" ethic = "ethic_xenophile" }
					species = event_target:rodianSpecies
					flag = {
						icon = { category = "starwars" file = "RodianClans.dds" }
						background = { category = "backgrounds" file = "00_solid.dds" }
						colors = { "customcolor1467" "customcolor1467" "null" "null" }
					}
					effect = {
						set_graphical_culture = misc_01
						set_country_flag = rodian_clans
						set_country_flag = init_spawned
						set_country_flag = custom_start_screen
						set_country_flag = canon_map
						set_country_flag = sandbox_map
						save_global_event_target_as = rodian_clans
					}
				}
				#create_colony = { owner = event_target:rodian_clans species = event_target:rodianSpecies ethos = owner }
			}
			set_capital = yes
			random_country = {
				limit = { has_country_flag = rodian_clans }
				save_global_event_target_as = rodian_clans
				species = { save_global_event_target_as = rodianSpecies }
			}
			set_owner = event_target:rodian_clans
			generate_rodia_buildings = yes
			generate_23_main_pops = yes
			give_start_techs = yes
			set_name = "NAME_Rodia"
		}
		init_effect = {
			save_event_target_as = shipyard_site
			solar_system = {
				spawn_megastructure = {
					type = "swp_orbital_shipyard_0" 
					location = event_target:shipyard_site
					owner = event_target:rodian_clans
					graphical_culture = event_target:rodian_clans
				}
			}
		}
		change_orbit = @base_moon_dist
		moon = {
			name = "Enak"
			class = pc_barren
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Yasooska"
			class = pc_barren
			orbit_distance = 0
			orbit_angle = 120
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Eiska"
			class = pc_asteroid
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 1 max = 1 }
		}
		moon = {
			name = "Rodia d"
			class = pc_asteroid
			orbit_distance = 0
			orbit_angle = 120
			size = { min = 1 max = 1 }
		}
	}
	planet = {
		name = "Pirdia"
		class = pc_gas_giant
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 25 max = 30 }
		has_ring = no
		change_orbit = @base_moon_dist
		moon = {
			name = "Pirdia a"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Pirdia b"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Pirdia c"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Pirdia d"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
	}
	planet = {
		name = "Toosma"
		class = pc_gas_giant
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 25 max = 30 }
		has_ring = no
		change_orbit = @base_moon_dist
		moon = {
			name = "Toosma a"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Toosma b"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Toosma c"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Toosma d"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Toosma e"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Toosma f"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Toosma g"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Toosma h"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
	}
	planet = {
		name = "Taoska"
		class = pc_frozen
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
}

#Utaruun
canon_utaruun_system_initializer = {
	name = "Utaruun"
	class = "rl_standard_stars"
	flags = { rodian_start_system canon_map }
	planet = {
		name = "Utaruun"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		count = { min = 2 max = 3 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		name = "Utaruun"
		class = pc_tropical
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = { set_planet_flag = planet_utaruun } 
		init_effect = {
			random_country = { 
				limit = { has_country_flag = rodian_clans } 
				save_global_event_target_as = rodian_clans
			}
			if = {
				limit = { exists = event_target:rodian_clans }
				set_owner = event_target:rodian_clans
				generate_colony_buildings = yes
				generate_10_main_pops = yes
			}
		}
		change_orbit = @base_moon_dist
	}
	planet = {
		count = { min = 3 max = 4 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 2 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		class = pc_asteroid
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
	}
}
