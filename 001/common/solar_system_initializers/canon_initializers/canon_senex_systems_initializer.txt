
@base_planet_dist = 25
@base_moon_dist = 15

#Senex
senex_system_initializer = {
	name = "Senex"
	class = "rl_standard_stars"
	flags = { senex_homeworld senex_start_system canon_map canon_map_shipyard_system }
	init_effect = { generate_home_system_resources = yes log = "senex homeworld" }
	usage = custom_empire
	max_instances = 1
	planet = {
		name = "Senex"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		count = { min = 2 max = 3 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		name = "Senex"
		class = pc_continental
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 15 max = 20 }
		has_ring = no
		starting_planet = yes
		deposit_blockers = none
		modifiers = none
		init_effect = { prevent_anomaly = yes set_planet_flag = planet_senex }
		init_effect = {
			set_global_flag = senex_homeworld_spawned
			if = {
				limit = { NOT = { any_country = { has_country_flag = senex_sector } } }
				create_species = { 
				    name = "Human" 
				    class = HUMAN3
				    portrait = human 
				    homeworld = THIS 
				    traits = { 
						trait="trait_adaptive"
						trait="trait_nomadic"
						trait="trait_wasteful"
				        ideal_planet_class = "pc_continental" 
					} 
				}
				last_created_species = { save_global_event_target_as = senexSpecies }
				create_country = {
					name = "NAME_senex_sector"
					ship_prefix = " "
					type = default
					origin = "origin_swp_default"
					ignore_initial_colony_error = yes
					civics = { civic = "civic_shadow_council" civic = "civic_aristocratic_elite" }
					authority = auth_oligarchic
					name_list = "SWP_Human3"
					ethos = { ethic = "ethic_fanatic_xenophile" ethic = "ethic_materialist" }
					species = event_target:senexSpecies
					flag = {
						icon = { category = "starwars" file = "SenexSector.dds" }
						background = { category = "backgrounds" file = "00_solid.dds" }
						colors = { "customcolor1027" "customcolor1027" "null" "null" }
					}
					effect = {
						set_graphical_culture = misc_03
						set_country_flag = senex_sector
						set_country_flag = init_spawned
						set_country_flag = custom_start_screen
						set_country_flag = canon_map
						set_country_flag = sandbox_map
						save_global_event_target_as = senex_sector
					}
				}
				#create_colony = { owner = event_target:senex_sector species = event_target:senexSpecies ethos = owner }
			}
			set_capital = yes
			random_country = {
				limit = { has_country_flag = senex_sector }
				save_global_event_target_as = senex_sector
				species = { save_global_event_target_as = senexSpecies }
			}
			set_owner = event_target:senex_sector
			generate_senex_buildings = yes
			generate_17_main_6_misc_pops = yes
			give_start_techs = yes
			set_name = "NAME_Senex"
		}
		init_effect = {
			save_event_target_as = shipyard_site
			solar_system = {
				spawn_megastructure = {
					type = "swp_orbital_shipyard_0" 
					location = event_target:shipyard_site
					owner = event_target:senex_sector
					graphical_culture = event_target:senex_sector
				}
			}
		}
		change_orbit = @base_moon_dist
	}
	planet = {
		count = { min = 3 max = 4 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 2 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		class = pc_asteroid
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
	}
}

#Caltinia
canon_caltinia_system_initializer = {
	name = "Caltinia"
	class = "rl_standard_stars"
	flags = { senex_start_system canon_map }
	planet = {
		name = "Caltinia"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		count = { min = 2 max = 3 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		name = "Caltinia"
		class = pc_continental
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = { set_planet_flag = planet_caltinia } 
		init_effect = {
			random_country = { 
				limit = { has_country_flag = senex_sector } 
				save_global_event_target_as = senex_sector
			}
			if = {
				limit = { exists = event_target:senex_sector }
				set_owner = event_target:senex_sector
				generate_colony_cg_buildings = yes
				generate_7_main_3_misc_pops = yes
			}
		}
		change_orbit = @base_moon_dist
	}
	planet = {
		count = { min = 3 max = 4 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 2 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		class = pc_asteroid
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
	}
}

#Karfeddion
canon_karfeddion_system_initializer = {
	name = "Karfeddion"
	class = "rl_standard_stars"
	flags = { senex_start_system canon_map }
	planet = {
		name = "Karfeddion"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		count = { min = 2 max = 3 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		name = "Karfeddion"
		class = pc_lake
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		deposit_blockers = none
		init_effect = { 
			prevent_anomaly = yes
			set_planet_flag = planet_karfeddion
			add_modifier = { modifier = lush_planet days = -1 }
		}
		init_effect = {
			random_country = { 
				limit = { has_country_flag = senex_sector } 
				save_global_event_target_as = senex_sector
			}
			if = {
				limit = { exists = event_target:senex_sector }
				set_owner = event_target:senex_sector
				generate_colony_buildings = yes
				generate_7_main_3_misc_pops = yes
			}
		}
		change_orbit = @base_moon_dist
	}
	planet = {
		count = { min = 3 max = 4 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 2 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		class = pc_asteroid
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
	}
}

#Tibrin
canon_tibrin_system_initializer = {
	name = "Tibrin"
	class = "rl_standard_stars"
	flags = { senex_start_system canon_map }
	planet = {
		name = "Tibrin"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		count = { min = 2 max = 3 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		name = "Tibrin"
		class = pc_desertislands
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = { set_planet_flag = planet_tibrin } 
		init_effect = {
			random_country = { 
				limit = { has_country_flag = senex_sector } 
				save_global_event_target_as = senex_sector
			}
			if = {
				limit = { exists = event_target:senex_sector }
				set_owner = event_target:senex_sector
				generate_colony_buildings = yes
				generate_10_IST_pops = yes
			}
		}
		change_orbit = @base_moon_dist
		moon = {
			name = "Plah"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
	}
	planet = {
		count = { min = 3 max = 4 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 2 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		class = pc_asteroid
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
	}
}

#Mussubir
canon_mussubir_system_initializer = {
	name = "Mussubir"
	class = "rl_standard_stars"
	flags = { senex_start_system canon_map }
	planet = {
		name = "Mussubir"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		name = "Mussubir I"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Mussubir II"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Mussubir III"
		class = pc_ecumenopolis_light
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = { set_planet_flag = planet_mussubir_ii } 
		init_effect = {
			random_country = { 
				limit = { has_country_flag = senex_sector } 
				save_global_event_target_as = senex_sector
			}
			if = {
				limit = { exists = event_target:senex_sector }
				set_owner = event_target:senex_sector
				generate_ecu_buildings = yes
				generate_14_main_5_misc_pops = yes
			}
		}
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Mussubir IV"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Mussubir V"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
}

#Kalgo
canon_kalgo_system_initializer = {
	name = "Kalgo"
	class = "rl_standard_stars"
	flags = { senex_start_system canon_map }
	planet = {
		name = "Kalgo"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		name = "Kalgo I"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Kalgo II"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Kalgo III"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Kalgo IV"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
		moon = {
			name = "Kalgo IVa"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
	}
	planet = {
		name = "Kalgo V"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Kalgo VI"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Kalgo VII"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
		moon = {
			name = "Kalgo VIIa"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
	}
	planet = {
		name = "Kalgo VIII"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Kalgo IX"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Kalgo X"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Kalgo XI"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
		moon = {
			name = "Kalgo XIa"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
	}
	planet = {
		name = "Kalgo XII"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Kalgo XIII"
		class = pc_cloud_city
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = {
			random_country = { 
				limit = { has_country_flag = senex_sector } 
				save_global_event_target_as = senex_sector
			}
			if = {
				limit = { exists = event_target:senex_sector }
				set_owner = event_target:senex_sector
				generate_cloud_buildings = yes
				generate_6_main_pops = yes
			}
		}
		change_orbit = @base_moon_dist
		moon = {
			name = "Kalgo XIIIa"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Kalgo XIIIb"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Kalgo XIIIc"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Kalgo XIIId"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Kalgo XIIIe"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Kalgo XIIIf"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Kalgo XIIIg"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Kalgo XIIIh"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Kalgo XIIIi"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Kalgo XIIIj"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Kalgo XIIIk"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Kalgo XIIIl"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
	}
}

#Usnia
canon_usnia_system_initializer = {
	name = "Usnia"
	class = "rl_standard_stars"
	flags = { senex_start_system canon_map }
	planet = {
		name = "Usnia"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		count = { min = 2 max = 3 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		name = "Usnia"
		class = pc_forest
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = { set_planet_flag = planet_usnia } 
		init_effect = {
			random_country = { 
				limit = { has_country_flag = senex_sector } 
				save_global_event_target_as = senex_sector
			}
			if = {
				limit = { exists = event_target:senex_sector }
				set_owner = event_target:senex_sector
				generate_colony_buildings = yes
				generate_7_main_3_misc_pops = yes
			}
		}
		change_orbit = @base_moon_dist
	}
	planet = {
		count = { min = 3 max = 4 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 2 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		class = pc_asteroid
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
	}
}

#Kamur
canon_kamur_system_initializer = {
	name = "Kamur"
	class = "rl_standard_stars"
	flags = { senex_start_system canon_map }
	planet = {
		name = "Kamur"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		count = { min = 2 max = 3 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		name = "Kamur"
		class = pc_cascadian
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = { set_planet_flag = planet_kamur } 
		init_effect = {
			random_country = { 
				limit = { has_country_flag = senex_sector } 
				save_global_event_target_as = senex_sector
			}
			if = {
				limit = { exists = event_target:senex_sector }
				set_owner = event_target:senex_sector
				generate_colony_buildings = yes
				generate_7_main_3_misc_pops = yes
			}
		}
		change_orbit = @base_moon_dist
	}
	planet = {
		count = { min = 3 max = 4 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 2 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		class = pc_asteroid
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
	}
}

#Denebia
canon_denebia_system_initializer = {
	name = "Denebia"
	class = "rl_standard_stars"
	flags = { senex_start_system canon_map }
	planet = {
		name = "Denebia"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		count = { min = 2 max = 3 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		name = "Denebia"
		class = pc_lake
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = { set_planet_flag = planet_denebia } 
		init_effect = {
			random_country = { 
				limit = { has_country_flag = senex_sector } 
				save_global_event_target_as = senex_sector
			}
			if = {
				limit = { exists = event_target:senex_sector }
				set_owner = event_target:senex_sector
				generate_colony_buildings = yes
				generate_10_main_pops = yes
			}
		}
		change_orbit = @base_moon_dist
	}
	planet = {
		count = { min = 3 max = 4 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 2 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		class = pc_asteroid
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
	}
}
