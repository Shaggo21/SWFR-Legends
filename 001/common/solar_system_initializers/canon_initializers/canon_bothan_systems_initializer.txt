
@base_planet_dist = 25
@base_moon_dist = 15

#Bothawui
bothawui_system_initializer = {
	name = "Both"
	class = "sc_g"
	flags = { bothan_homeworld bothan_start_system canon_map canon_map_shipyard_system }
	init_effect = { generate_home_system_resources = yes log = "bothan homeworld" }
	usage = custom_empire
	max_instances = 1
	planet = {
		name = "Both"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		name = "Yeltha"
		class = pc_molten
		orbit_distance = { min = 20 max = 20 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Taboth"
		class = pc_barren
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
		moon = {
			name = "Taboth a"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Taboth b"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 120
			size = { min = 2 max = 4 }
		}
	}
	asteroid_belt = {
		type = rocky_asteroid_belt
		radius = 105
	}
	planet = {
		class = pc_asteroid
		orbit_distance = 30
		orbit_angle = { min = 40 max = 180 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		count = { min = 1 max = 2 }
		class = pc_asteroid
		orbit_distance = 0
		orbit_angle = { min = 40 max = 180 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Bothawui"
		class = pc_continental
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 20 max = 25 }
		has_ring = no
		starting_planet = yes
		deposit_blockers = none
		modifiers = none
		init_effect = { prevent_anomaly = yes set_planet_flag = planet_bothawui }
		init_effect = {
			set_global_flag = bothan_homeworld_spawned
			if = {
				limit = { NOT = { any_country = { has_country_flag = bothan_council } } }
				create_species = { 
				    name = "Bothan" 
				    class = BOT
				    portrait = bothan 
				    homeworld = THIS 
				    traits = { 
				        trait = "trait_intelligent" 
				        trait = "trait_communal" 
				        trait = "trait_fleeting"
				        ideal_planet_class = "pc_continental" 
					} 
				}
				last_created_species = { save_global_event_target_as = bothanSpecies }
				create_country = {
					name = "NAME_bothan_council"
					ship_prefix = " "
					type = default
					origin = "origin_swp_restoration"
					ignore_initial_colony_error = yes
					civics = { civic = "civic_bothan_spies" civic = "civic_environmentalist" }
					authority = auth_democratic
					name_list = "Bothan"
					ethos = { ethic = "ethic_egalitarian" ethic = "ethic_pacifist" ethic = "ethic_xenophile" }
					species = event_target:bothanSpecies
					flag = {
						icon = { category = "starwars" file = "BothanCouncil.dds" }
						background = { category = "backgrounds" file = "00_solid.dds" }
						colors = { "customcolor1704" "customcolor1704" "null" "null" }
					}
					effect = {
						set_graphical_culture = misc_03
						set_country_flag = bothan_council
						set_country_flag = init_spawned
						set_country_flag = custom_start_screen
						set_country_flag = canon_map
						set_country_flag = sandbox_map
						save_global_event_target_as = bothan_council
					}
				}
				#create_colony = { owner = event_target:bothan_council species = event_target:bothanSpecies ethos = owner }
			}
			set_capital = yes
			random_country = {
				limit = { has_country_flag = bothan_council }
				save_global_event_target_as = bothan_council
				species = { save_global_event_target_as = bothanSpecies }
			}
			set_owner = event_target:bothan_council
			generate_bothawui_buildings = yes
			generate_23_main_pops = yes
			give_start_techs = yes
			set_name = "NAME_Bothawui"
		}
		init_effect = {
			save_event_target_as = shipyard_site
			solar_system = {
				spawn_megastructure = {
					type = "swp_orbital_shipyard_0" 
					location = event_target:shipyard_site
					owner = event_target:bothan_council
					graphical_culture = event_target:bothan_council
				}
			}
		}
		change_orbit = @base_moon_dist
		moon = {
			name = "Bothawui a"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Bothawui b"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 120
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Bothawui c"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
	}
	planet = {
		name = "Golm"
		class = pc_gas_giant
		orbit_distance = { min = 40 max = 40 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 25 max = 30 }
		has_ring = yes
		change_orbit = @base_moon_dist
		moon = {
			name = "Golm a"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Golm b"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Golm c"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Golm d"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Golm e"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Golm f"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
	}
	planet = {
		name = "Ganash"
		class = pc_gas_giant
		orbit_distance = { min = 40 max = 40 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 25 max = 30 }
		has_ring = no
		change_orbit = @base_moon_dist
		moon = {
			name = "Ganash a"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Ganash b"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Ganash c"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Ganash d"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
	}
	planet = {
		name = "Hoppawui"
		class = pc_barren
		orbit_distance = { min = 20 max = 20 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
		moon = {
			name = "Hoppawui a"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Hoppawui b"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Hoppawui c"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Hoppawui d"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
	}
	planet = {
		name = "Jentawui"
		class = pc_frozen
		orbit_distance = { min = 10 max = 10 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
}

#Thoran
canon_thoran_system_initializer = {
	name = "Thoran"
	class = "rl_standard_stars"
	flags = { bothan_start_system canon_map }
	planet = {
		name = "Thoran"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		count = { min = 2 max = 3 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		name = "Thoran"
		class = pc_subarctic
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = {
			random_country = { 
				limit = { has_country_flag = bothan_council } 
				save_global_event_target_as = bothan_council
			}
			if = {
				limit = { exists = event_target:bothan_council }
				set_owner = event_target:bothan_council
				generate_colony_buildings = yes
				generate_10_main_pops = yes
			}
		}
		change_orbit = @base_moon_dist
	}
	planet = {
		count = { min = 3 max = 4 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 2 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		class = pc_asteroid
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
	}
}

#Manda
canon_manda_system_initializer = {
	name = "Manda"
	class = "sc_g"
	flags = { bothan_start_system canon_map }
	planet = {
		name = "Manda Prime"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		count = { min = 2 max = 3 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		name = "Manda"
		class = pc_ecumenopolis_light
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = {
			random_country = { 
				limit = { has_country_flag = bothan_council } 
				save_global_event_target_as = bothan_council
			}
			if = {
				limit = { exists = event_target:bothan_council }
				set_owner = event_target:bothan_council
				generate_ecu_buildings = yes
				generate_19_HUM3_pops = yes
			}
		}
		change_orbit = @base_moon_dist
	}
	planet = {
		count = { min = 3 max = 4 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 2 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		class = pc_asteroid
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
	}
}

#Kothlis
canon_kothlis_system_initializer = {
	name = "Kothlis"
	class = "sc_m"
	flags = { bothan_start_system canon_map }
	planet = {
		name = "Koth'lar"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		name = "Sessa"
		class = pc_molten
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Kuk'tar"
		class = pc_molten
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
		moon = {
			name = "Kuk'tar a"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
	}
	planet = {
		name = "Dwi'kar"
		class = pc_gas_giant
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 25 max = 30 }
		has_ring = no
		change_orbit = @base_moon_dist
		moon = {
			name = "Dwi'kar 1"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Dwi'kar 2"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Dwi'kar 3"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Dwi'kar 4"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Dwi'kar 5"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Dwi'kar 6"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Dwi'kar 7"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Dwi'kar 8"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Dwi'kar 9"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 120
			size = { min = 2 max = 4 }
		}
	}
	planet = {
		name = "Kothlis"
		class = pc_desertislands
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = yes
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = {
			random_country = { 
				limit = { has_country_flag = bothan_council } 
				save_global_event_target_as = bothan_council
			}
			if = {
				limit = { exists = event_target:bothan_council }
				set_owner = event_target:bothan_council
				generate_colony_cg_buildings = yes
				generate_5_main_5_HUM3_pops = yes
			}
		}
		change_orbit = @base_moon_dist
		moon = {
			name = "Kothlis a"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Kothlis b"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 120
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Kothlis c"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
	}
	planet = {
		name = "Dwi'lar"
		class = pc_gas_giant
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 25 max = 30 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Mar'ta I"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Mar'ta II"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
}

#Nexus Ortai
canon_nexus_ortai_system_initializer = {
	name = "Nexus Ortai"
	class = "rl_standard_stars"
	flags = { bothan_start_system canon_map }
	planet = {
		name = "Nexus Ortai"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		count = { min = 2 max = 3 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		name = "Nexus Ortai"
		class = pc_continental
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = {
			random_country = { 
				limit = { has_country_flag = bothan_council } 
				save_global_event_target_as = bothan_council
			}
			if = {
				limit = { exists = event_target:bothan_council }
				set_owner = event_target:bothan_council
				generate_colony_buildings = yes
				generate_10_misc_pops = yes
			}
		}
		change_orbit = @base_moon_dist
	}
	planet = {
		count = { min = 3 max = 4 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 2 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		class = pc_asteroid
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
	}
}

#Void Station
canon_void_station_system_initializer = {
	name = "Void Station"
	class = "rl_standard_stars"
	flags = { bothan_start_system canon_map }
	planet = {
		name = "Void Station"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		name = "Random"
		class = random_non_colonizable
		orbit_distance = { min = 50 max = 50 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Void Station"
		class = pc_polis_massa
		orbit_distance = { min = 50 max = 50 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		entity = "pc_void_station"
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = {
			random_country = { 
				limit = { has_country_flag = bothan_council } 
				save_global_event_target_as = bothan_council
			}
			if = {
				limit = { exists = event_target:bothan_council }
				set_owner = event_target:bothan_council
				generate_station_buildings = yes
				generate_2_main_2_HUM3_pops = yes
			}
		}
		change_orbit = @base_moon_dist
	}
	asteroid_belt = {
		type = rocky_asteroid_belt
		radius = 125
	}
	planet = {
		class = pc_asteroid
		orbit_distance = 0
		orbit_angle = { min = 40 max = 180 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		count = { min = 1 max = 2 }
		class = pc_asteroid
		orbit_distance = 0
		orbit_angle = { min = 40 max = 180 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	asteroid_belt = {
		type = rocky_asteroid_belt
		radius = 175
	}
	planet = {
		class = pc_asteroid
		orbit_distance = 50
		orbit_angle = { min = 40 max = 180 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		count = { min = 1 max = 2 }
		class = pc_asteroid
		orbit_distance = 0
		orbit_angle = { min = 40 max = 180 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
}

#Spirador
canon_spirador_system_initializer = {
	name = "Spirador"
	class = "rl_standard_stars"
	flags = { bothan_start_system canon_map }
	planet = {
		name = "Spirador"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		count = { min = 2 max = 3 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		name = "Spirador"
		class = pc_subarctic
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = {
			random_country = { 
				limit = { has_country_flag = bothan_council } 
				save_global_event_target_as = bothan_council
			}
			if = {
				limit = { exists = event_target:bothan_council }
				set_owner = event_target:bothan_council
				generate_colony_buildings = yes
				generate_10_HUM3_pops = yes
			}
		}
		change_orbit = @base_moon_dist
	}
	planet = {
		count = { min = 3 max = 4 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 2 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		class = pc_asteroid
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
	}
}

#Dressel
canon_dressel_system_initializer = {
	name = "Dressel"
	class = "rl_standard_stars"
	flags = { bothan_start_system canon_map }
	planet = {
		name = "Dressel"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		name = "Random"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Random"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Dressel"
		class = pc_continental
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes set_planet_flag = planet_dressel }
		init_effect = {
			random_country = { 
				limit = { has_country_flag = bothan_council } 
				save_global_event_target_as = bothan_council
			}
			if = {
				limit = { exists = event_target:bothan_council }
				set_owner = event_target:bothan_council
				generate_colony_buildings = yes
				generate_10_DRE_pops = yes
			}
		}
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Random"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	asteroid_belt = {
		type = rocky_asteroid_belt
		radius = 175
	}
	planet = {
		class = pc_asteroid
		orbit_distance = 30
		orbit_angle = { min = 40 max = 180 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		count = { min = 1 max = 2 }
		class = pc_asteroid
		orbit_distance = 0
		orbit_angle = { min = 40 max = 180 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Bothan Colony"
		class = pc_polis_massa
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
}

#Sarkhai
canon_sarkhai_system_initializer = {
	name = "Sarkhai"
	class = "rl_standard_stars"
	flags = { bothan_start_system canon_map }
	planet = {
		name = "Sarkhai"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		count = { min = 2 max = 3 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		name = "Sarkhai"
		class = pc_forest
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes set_planet_flag = planet_sarkhai }
		init_effect = {
			random_country = { 
				limit = { has_country_flag = bothan_council } 
				save_global_event_target_as = bothan_council
			}
			if = {
				limit = { exists = event_target:bothan_council }
				set_owner = event_target:bothan_council
				generate_colony_buildings = yes
				generate_10_SAR_pops = yes
			}
		}
		change_orbit = @base_moon_dist
	}
	planet = {
		count = { min = 3 max = 4 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 2 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		class = pc_asteroid
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
	}
}
