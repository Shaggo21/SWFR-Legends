
@base_planet_dist = 25
@base_moon_dist = 15

#Enarc
enarc_system_initializer = {
	name = "Enarc"
	class = "rl_standard_stars"
	flags = { cis_homeworld cis_start_system canon_map canon_map_shipyard_system }
	init_effect = { generate_home_system_resources = yes log = "cis homeworld" add_modifier = { modifier = sy_triellus_trade_route } }
	usage = custom_empire
	max_instances = 1
	planet = {
		name = "Enarc"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		count = { min = 2 max = 3 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		name = "Enarc"
		class = pc_continental
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 15 max = 20 }
		has_ring = no
		starting_planet = yes
		deposit_blockers = none
		modifiers = none
		init_effect = { prevent_anomaly = yes set_planet_flag = planet_enarc }
		init_effect = {
			set_global_flag = cis_homeworld_spawned
			if = {
				limit = { NOT = { any_country = { has_country_flag = confederate_remnant } } }
				create_species = { 
				    name = "Neimoidian" 
				    class = NEI
				    portrait = neimoidian 
				    homeworld = THIS 
				    traits = { 
				        trait = "trait_thrifty" 
				        trait = "trait_communal" 
				        trait = "trait_weak"
				        ideal_planet_class = "pc_continental" 
					} 
				}
				last_created_species = { save_global_event_target_as = confederateSpecies }
				create_country = {
					name = "NAME_confederate_remnant"
					ship_prefix = " "
					type = default
					origin = "origin_swp_confed_remnant"
					ignore_initial_colony_error = yes
					civics = { civic = "civic_confederate_remnant" civic = "civic_clone_wars_loser" }
					authority = auth_oligarchic
					name_list = "Neimoidian"
					ethos = { ethic = "ethic_authoritarian" ethic = "ethic_militarist" ethic = "ethic_materialist" }
					species = event_target:confederateSpecies
					flag = {
						icon = { category = "starwars" file = "ConfederateRemnant.dds" }
						background = { category = "backgrounds" file = "00_solid.dds" }
						colors = { "customcolor1583" "customcolor1583" "null" "null" }
					}
					effect = {
						set_graphical_culture = cis_01
						set_country_flag = confederate_remnant
						set_country_flag = init_spawned
						set_country_flag = custom_start_screen
						set_country_flag = canon_map
						set_country_flag = sandbox_map
						save_global_event_target_as = confederate_remnant
					}
				}
				#create_colony = { owner = event_target:confederate_remnant species = event_target:confederateSpecies ethos = owner }
			}
			set_capital = yes
			random_country = {
				limit = { has_country_flag = confederate_remnant }
				save_global_event_target_as = confederate_remnant
				species = { save_global_event_target_as = confederateSpecies }
			}
			set_owner = event_target:confederate_remnant
			generate_enarc_buildings = yes
			generate_11_main_12_HUM2_pops = yes
			give_start_techs = yes
			set_name = "NAME_Enarc"
		}
		init_effect = {
			save_event_target_as = shipyard_site
			solar_system = {
				spawn_megastructure = {
					type = "swp_orbital_shipyard_0" 
					location = event_target:shipyard_site
					owner = event_target:confederate_remnant
					graphical_culture = event_target:confederate_remnant
				}
			}
		}
		change_orbit = @base_moon_dist
	}
	planet = {
		count = { min = 3 max = 4 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 2 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		class = pc_asteroid
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
	}
}

#Terminus
canon_terminus_system_initializer = {
	name = "Terminus"
	class = "rl_standard_stars"
	flags = { cis_start_system canon_map canon_map_shipyard_system }
	init_effect = { add_modifier = { modifier = sy_hydian_way } }
	planet = {
		name = "Terminus"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		count = { min = 2 max = 3 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		name = "Terminus"
		class = pc_ecumenopolis_light
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = { set_planet_flag = planet_terminus }
		init_effect = {
			random_country = { 
				limit = { has_country_flag = confederate_remnant } 
				save_global_event_target_as = confederate_remnant
			}
			if = {
				limit = { exists = event_target:confederate_remnant }
				set_owner = event_target:confederate_remnant
				generate_ecu_buildings = yes
				generate_19_misc_pops = yes
			}
		}
		init_effect = {
			save_event_target_as = shipyard_site
			solar_system = {
				spawn_megastructure = {
					type = "swp_planetary_shipyard_0" 
					location = event_target:shipyard_site
					owner = event_target:confederate_remnant
					graphical_culture = event_target:confederate_remnant
				}
			}
		}
		change_orbit = @base_moon_dist
		moon = {
			name = "Terminus a"
			class = pc_toxic
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
			entity = "swp_toxic_planet_01_entity"
		}
		moon = {
			name = "Terminus b"
			class = pc_barren
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
	}
	planet = {
		count = { min = 3 max = 4 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 2 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		class = pc_asteroid
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
	}
}

#Sluis Van
canon_sluis_van_system_initializer = {
	name = "Sluis"
	class = "rl_standard_stars"
	flags = { cis_start_system canon_map canon_map_shipyard_system }
	init_effect = { add_modifier = { modifier = sy_rimma_trade_route } }
	planet = {
		name = "Sluis"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		count = { min = 2 max = 3 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		name = "Sluis Van"
		class = pc_continental
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = { set_planet_flag = planet_sluisvan }
		init_effect = {
			random_country = { 
				limit = { has_country_flag = confederate_remnant } 
				save_global_event_target_as = confederate_remnant
			}
			if = {
				limit = { exists = event_target:confederate_remnant }
				set_owner = event_target:confederate_remnant
				generate_sluis_van_buildings = yes
				generate_12_SLU_pops = yes
			}
		}
		init_effect = {
			save_event_target_as = shipyard_site
			solar_system = {
				spawn_megastructure = {
					type = "swp_planetary_shipyard_1" 
					location = event_target:shipyard_site
					owner = event_target:confederate_remnant
					graphical_culture = event_target:confederate_remnant
				}
			}
		}
		change_orbit = @base_moon_dist
	}
	planet = {
		count = { min = 3 max = 4 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 2 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		class = pc_asteroid
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
	}
}

#Darkknell
canon_darkknell_system_initializer = {
	name = "Knel'char"
	class = "sc_knel_char"
	flags = { cis_start_system canon_map }
	init_effect = { add_modifier = { modifier = sy_hydian_way } }
	planet = {
		name = "Knel'char I"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		name = "Knel'char II"
		class = star
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 30 max = 35 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Knel'char III"
		class = star
		orbit_distance = { min = 55 max = 55 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 30 max = 35 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Darkknell"
		class = pc_continental
		orbit_distance = { min = 60 max = 60 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 15 max = 20 }
		has_ring = no
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = {
			random_country = { 
				limit = { has_country_flag = confederate_remnant } 
				save_global_event_target_as = confederate_remnant
			}
			if = {
				limit = { exists = event_target:confederate_remnant }
				set_owner = event_target:confederate_remnant
				generate_darkknell_buildings = yes
				generate_4_HUM2_3_SLU_3_DUR_pops = yes
			}
		}
		change_orbit = @base_moon_dist
	}
}

#Bahalian
canon_bahalian_system_initializer = {
	name = "Bahalian"
	class = "rl_standard_stars"
	flags = { cis_start_system canon_map canon_map_shipyard_system }
	planet = {
		name = "Bahalian"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		name = "Bahalian Station"
		class = pc_deep_space_station
		orbit_distance = 50
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = {
			random_country = { 
				limit = { has_country_flag = confederate_remnant } 
				save_global_event_target_as = confederate_remnant
			}
			if = {
				limit = { exists = event_target:confederate_remnant }
				set_owner = event_target:confederate_remnant
				generate_station_buildings = yes
				generate_2_HUM2_2_DUR_pops = yes
			}
			set_planet_entity = {
				entity = "habitat_phase_01_entity"
				graphical_culture = cis_01
			}
			set_planet_flag = megastructure
			set_planet_flag = deep_space_station
		}
		change_orbit = @base_moon_dist
	}
}

#Christophsis
canon_christophsis_system_initializer = {
	name = "Christoph"
	class = "rl_standard_stars"
	flags = { cis_start_system canon_map }
	init_effect = { add_modifier = { modifier = sy_corellian_run } }
	planet = {
		name = "Christoph"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	asteroid_belt = {
		type = rocky_asteroid_belt
		radius = 75
	}
	planet = {
		class = pc_asteroid
		orbit_distance = 50
		orbit_angle = { min = 40 max = 180 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		count = { min = 1 max = 2 }
		class = pc_asteroid
		orbit_distance = 0
		orbit_angle = { min = 40 max = 180 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Christophsis"
		class = pc_crystalline
		orbit_distance = { min = 50 max = 50 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		entity = "crystalline_planet_01_entity"
		deposit_blockers = none
		init_effect = {
			prevent_anomaly = yes
			set_planet_flag = planet_christophsis
			add_deposit = d_kyber_vein
		}
		init_effect = {
			random_country = { 
				limit = { has_country_flag = confederate_remnant } 
				save_global_event_target_as = confederate_remnant
			}
			if = {
				limit = { exists = event_target:confederate_remnant }
				set_owner = event_target:confederate_remnant
				generate_christophsis_buildings = yes
				generate_7_HUM2_3_ROD_pops = yes
			}
		}
		change_orbit = @base_moon_dist
		moon = {
			name = "Christophsis' Moon"
			class = pc_barren
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
	}
	asteroid_belt = {
		type = rocky_asteroid_belt
		radius = 155
	}
	planet = {
		class = pc_asteroid
		orbit_distance = 30
		orbit_angle = { min = 40 max = 180 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		count = { min = 1 max = 2 }
		class = pc_asteroid
		orbit_distance = 0
		orbit_angle = { min = 40 max = 180 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Erodsis"
		class = pc_gas_giant
		orbit_distance = { min = 40 max = 40 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 25 max = 30 }
		has_ring = no
		change_orbit = @base_moon_dist
		moon = {
			name = "Erodsis a"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Erodsis b"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Erodsis c"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Erodsis d"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Erodsis e"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Erodsis f"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Erodsis g"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Erodsis h"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Erodsis i"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Erodsis j"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Erodsis k"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Erodsis l"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Erodsis m"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Erodsis n"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Erodsis o"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Erodsis p"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
	}
	asteroid_belt = {
		type = icy_asteroid_belt
		radius = 215
	}
	planet = {
		class = pc_ice_asteroid
		orbit_distance = 20
		orbit_angle = { min = 40 max = 180 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		count = { min = 1 max = 2 }
		class = pc_ice_asteroid
		orbit_distance = 0
		orbit_angle = { min = 40 max = 180 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
}

#Gamor
canon_gamor_system_initializer = {
	name = "Gamor"
	class = "rl_standard_stars"
	flags = { cis_start_system canon_map }
	init_effect = { add_modifier = { modifier = sy_corellian_run } }
	planet = {
		name = "Gamor"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		count = { min = 2 max = 3 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		name = "Gamor"
		class = pc_ecumenopolis_light
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = { set_planet_flag = planet_gamor }
		init_effect = {
			random_country = { 
				limit = { has_country_flag = confederate_remnant } 
				save_global_event_target_as = confederate_remnant
			}
			if = {
				limit = { exists = event_target:confederate_remnant }
				set_owner = event_target:confederate_remnant
				generate_ecu_buildings = yes
				generate_19_misc_pops = yes
			}
		}
		change_orbit = @base_moon_dist
	}
	planet = {
		count = { min = 3 max = 4 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 2 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		class = pc_asteroid
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
	}
}

#Milagro
canon_milagro_system_initializer = {
	name = "Milagro"
	class = "rl_standard_stars"
	flags = { cis_start_system canon_map }
	init_effect = { add_modifier = { modifier = sy_corellian_run } }
	planet = {
		name = "Milagro"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		count = { min = 2 max = 3 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		name = "Milagro"
		class = pc_ecumenopolis_light
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = { set_planet_flag = planet_milagro }
		init_effect = {
			random_country = { 
				limit = { has_country_flag = confederate_remnant } 
				save_global_event_target_as = confederate_remnant
			}
			if = {
				limit = { exists = event_target:confederate_remnant }
				set_owner = event_target:confederate_remnant
				generate_milagro_buildings = yes
				generate_19_misc_pops = yes
			}
		}
		change_orbit = @base_moon_dist
	}
	planet = {
		count = { min = 3 max = 4 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 2 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		class = pc_asteroid
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
	}
}

#Farstine
canon_farstine_system_initializer = {
	name = "Farstine"
	class = "rl_standard_stars"
	flags = { cis_start_system canon_map }
	init_effect = { add_modifier = { modifier = sy_triellus_trade_route } }
	planet = {
		name = "Farstine"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		count = { min = 2 max = 3 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		name = "Farstine"
		class = pc_desert
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		entity = "pc_toxic"
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = { set_planet_flag = planet_farstine }
		init_effect = {
			random_country = { 
				limit = { has_country_flag = confederate_remnant } 
				save_global_event_target_as = confederate_remnant
			}
			if = {
				limit = { exists = event_target:confederate_remnant }
				set_owner = event_target:confederate_remnant
				generate_desert_buildings = yes
				generate_10_SKA_pops = yes
			}
		}
		change_orbit = @base_moon_dist
	}
	planet = {
		count = { min = 3 max = 4 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 2 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		class = pc_asteroid
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
	}
}

#Bannistar Station
canon_bannistar_station_system_initializer = {
	name = "Bannistar Station"
	class = "rl_standard_stars"
	flags = { cis_start_system canon_map }
	planet = {
		name = "Bannistar Station"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		name = "Random"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Random"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Random"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Bannistar"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Bannistar Station"
		class = pc_deep_space_station
		orbit_distance = 0
		orbit_angle = 10
		size = { min = 10 max = 15 }
		has_ring = no
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = {
			random_country = { 
				limit = { has_country_flag = confederate_remnant } 
				save_global_event_target_as = confederate_remnant
			}
			if = {
				limit = { exists = event_target:confederate_remnant }
				set_owner = event_target:confederate_remnant
				generate_station_buildings = yes
				generate_4_HUM2_pops = yes
			}
			set_planet_entity = {
				entity = "habitat_phase_01_entity"
				graphical_culture = cis_01
			}
			set_planet_flag = megastructure
			set_planet_flag = deep_space_station
		}
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Random"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Random"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
}

#New Cov
canon_new_cov_system_initializer = {
	name = "Churba"
	class = "rl_standard_stars"
	flags = { cis_start_system canon_map }
	init_effect = { add_modifier = { modifier = sy_corellian_run } }
	planet = {
		name = "Churba"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		name = "Barhu"
		class = pc_molten
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Cov II"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "New Cov"
		class = pc_tropical
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = { set_planet_flag = planet_newcov }
		init_effect = {
			random_country = { 
				limit = { has_country_flag = confederate_remnant } 
				save_global_event_target_as = confederate_remnant
			}
			if = {
				limit = { exists = event_target:confederate_remnant }
				set_owner = event_target:confederate_remnant
				generate_minor_buildings = yes
				generate_4_main_4_HUM2_pops = yes
			}
		}
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Churba"
		class = pc_ecumenopolis
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = { set_planet_flag = planet_churba }
		init_effect = {
			random_country = { 
				limit = { has_country_flag = confederate_remnant } 
				save_global_event_target_as = confederate_remnant
			}
			if = {
				limit = { exists = event_target:confederate_remnant }
				set_owner = event_target:confederate_remnant
				generate_ecu_buildings = yes
				generate_19_HUM2_pops = yes
			}
		}
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Cov V"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Cov VI"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Cov VII"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Hurcha"
		class = pc_frozen
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
}

#Druckenwell
canon_druckenwell_system_initializer = {
	name = "Druckenwell"
	class = "rl_standard_stars"
	flags = { cis_start_system canon_map }
	init_effect = { add_modifier = { modifier = sy_corellian_run } }
	planet = {
		name = "Druckenwell"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		count = { min = 2 max = 3 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		name = "Druckenwell"
		class = pc_ecumenopolis_light
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = { set_planet_flag = planet_druckenwell }
		init_effect = {
			random_country = { 
				limit = { has_country_flag = confederate_remnant } 
				save_global_event_target_as = confederate_remnant
			}
			if = {
				limit = { exists = event_target:confederate_remnant }
				set_owner = event_target:confederate_remnant
				generate_ecu_buildings = yes
				generate_19_HUM2_pops = yes
			}
		}
		change_orbit = @base_moon_dist
	}
	planet = {
		count = { min = 3 max = 4 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 2 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		class = pc_asteroid
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
	}
}

#Falleen
canon_falleen_system_initializer = {
	name = "Falleen"
	class = "rl_standard_stars"
	flags = { cis_start_system canon_map }
	planet = {
		name = "Falleen"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		count = { min = 2 max = 3 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		name = "Falleen"
		class = pc_hajungle
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = { set_planet_flag = planet_falleen }
		init_effect = {
			random_country = { 
				limit = { has_country_flag = confederate_remnant } 
				save_global_event_target_as = confederate_remnant
			}
			if = {
				limit = { exists = event_target:confederate_remnant }
				set_owner = event_target:confederate_remnant
				generate_falleen_buildings = yes
				generate_10_FAL_pops = yes
			}
		}
		change_orbit = @base_moon_dist
	}
	planet = {
		count = { min = 3 max = 4 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 2 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		class = pc_asteroid
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
	}
}
