
@base_planet_dist = 25
@base_moon_dist = 15

#Bomis Koori
bomis_koori_system_initializer = {
	name = "Bomis Koori"
	class = "rl_standard_stars"
	flags = { corporate_homeworld koorivar_start_system canon_map canon_map_shipyard_system }
	init_effect = { generate_home_system_resources = yes log = "corporate homeworld" add_modifier = { modifier = sy_corellian_trade_spine } }
	usage = custom_empire
	max_instances = 1
	planet = {
		name = "Bomis Koori"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		name = "Bomis Koori I"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Bomis Koori II"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
		moon = {
			name = "Bomis Koori IIa"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
	}
	planet = {
		name = "Bomis Koori III"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Bomis Koori IV"
		class = pc_ecumenopolis_light
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 20 max = 25 }
		has_ring = no
		starting_planet = yes
		deposit_blockers = none
		modifiers = none
		init_effect = { prevent_anomaly = yes set_planet_flag = planet_bomis_koori_iv }
		init_effect = {
			set_global_flag = corporate_homeworld_spawned
			if = {
				limit = { NOT = { any_country = { has_country_flag = corporate_alliance } } }
				create_species = { 
				    name = "Koorivar" 
				    class = KOO
				    portrait = koorivar 
				    homeworld = THIS 
				    traits = { 
				        trait = "trait_thrifty"
				        ideal_planet_class = "pc_continental" 
					} 
				}
				last_created_species = { save_global_event_target_as = koorivarSpecies }
				create_country = {
					name = "NAME_corporate_alliance"
					ship_prefix = " "
					type = default
					origin = "origin_swp_confed_remnant"
					ignore_initial_colony_error = yes
					civics = { civic = "civic_private_military_companies" civic = "civic_naval_contractors" }
					authority = auth_corporate
					name_list = "Koorivar"
					ethos = { ethic = "ethic_authoritarian" ethic = "ethic_materialist" ethic = "ethic_militarist" }
					species = event_target:koorivarSpecies
					flag = {
						icon = { category = "starwars" file = "CorporateAlliance.dds" }
						background = { category = "backgrounds" file = "00_solid.dds" }
						colors = { "customcolor2009" "customcolor2009" "null" "null" }
					}
					effect = {
						set_graphical_culture = cis_01
						set_country_flag = corporate_alliance
						set_country_flag = init_spawned
						set_country_flag = custom_start_screen
						set_country_flag = canon_map
						set_country_flag = sandbox_map
						save_global_event_target_as = corporate_alliance
					}
				}
				#create_colony = { owner = event_target:corporate_alliance species = event_target:koorivarSpecies ethos = owner }
			}
			set_capital = yes
			random_country = {
				limit = { has_country_flag = corporate_alliance }
				save_global_event_target_as = corporate_alliance
				species = { save_global_event_target_as = koorivarSpecies }
			}
			set_owner = event_target:corporate_alliance
			generate_bomis_koori_iv_buildings = yes
			generate_29_main_pops = yes
			give_start_techs = yes
			set_name = "NAME_Bomis_Koori_IV"
		}
		init_effect = {
			save_event_target_as = shipyard_site
			solar_system = {
				spawn_megastructure = {
					type = "swp_orbital_shipyard_0" 
					location = event_target:shipyard_site
					owner = event_target:corporate_alliance
					graphical_culture = event_target:corporate_alliance
				}
			}
		}
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Bomis Koori V"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
		moon = {
			name = "Bomis Koori Va"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Bomis Koori Vb"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 120
			size = { min = 2 max = 4 }
		}
	}
	planet = {
		name = "Bomis Koori VI"
		class = pc_gas_giant
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 25 max = 30 }
		has_ring = no
		change_orbit = @base_moon_dist
		moon = {
			name = "Bomis Koori VIa"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Bomis Koori VIb"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Bomis Koori VIc"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Bomis Koori VId"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Bomis Koori VIe"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Bomis Koori VIf"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Bomis Koori VIg"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Bomis Koori VIh"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Bomis Koori VIi"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Bomis Koori VIj"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Bomis Koori VIk"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Bomis Koori VIl"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Bomis Koori VIm"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Bomis Koori VIn"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Bomis Koori VIo"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Bomis Koori VIp"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
	}
	planet = {
		name = "Bomis Koori VII"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
}

#Harrin
canon_harrin_system_initializer = {
	name = "Harrin"
	class = "rl_standard_stars"
	flags = { koorivar_start_system canon_map }
	init_effect = { add_modifier = { modifier = sy_corellian_trade_spine } }
	planet = {
		name = "Harrin"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		count = { min = 2 max = 3 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		name = "Harrin"
		class = pc_ecumenopolis_light
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = { set_planet_flag = planet_harrin } 
		init_effect = {
			random_country = { 
				limit = { has_country_flag = corporate_alliance } 
				save_global_event_target_as = corporate_alliance
			}
			if = {
				limit = { exists = event_target:corporate_alliance }
				set_owner = event_target:corporate_alliance
				generate_ecu_buildings = yes
				generate_19_HUM2_pops = yes
			}
		}
		change_orbit = @base_moon_dist
	}
	planet = {
		count = { min = 3 max = 4 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 2 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		class = pc_asteroid
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
	}
}

#Moorja
canon_moorja_system_initializer = {
	name = "Moorja"
	class = "rl_standard_stars"
	flags = { koorivar_start_system canon_map }
	init_effect = { add_modifier = { modifier = sy_corellian_trade_spine } }
	planet = {
		name = "Moorja"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		count = { min = 2 max = 3 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		name = "Moorja"
		class = pc_tundra
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		entity = "pc_moorja"
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = { set_planet_flag = planet_moorja } 
		init_effect = {
			random_country = { 
				limit = { has_country_flag = corporate_alliance } 
				save_global_event_target_as = corporate_alliance
			}
			if = {
				limit = { exists = event_target:corporate_alliance }
				set_owner = event_target:corporate_alliance
				generate_moorja_buildings = yes
				generate_11_HUM2_3_ITH_pops = yes
			}
		}
		change_orbit = @base_moon_dist
		moon = {
			name = "Moorja a"
			class = pc_barren
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
	}
	planet = {
		count = { min = 3 max = 4 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 2 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		class = pc_asteroid
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
	}
}

#Cyphar
canon_cyphar_system_initializer = {
	name = "Cyphar"
	class = "rl_standard_stars"
	flags = { koorivar_start_system canon_map }
	planet = {
		name = "Cyphar"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		count = { min = 2 max = 3 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		name = "Cyphar"
		class = pc_ecumenopolis_light
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		deposit_blockers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = { set_planet_flag = planet_cyphar } 
		init_effect = {
			random_country = { 
				limit = { has_country_flag = corporate_alliance } 
				save_global_event_target_as = corporate_alliance
			}
			if = {
				limit = { exists = event_target:corporate_alliance }
				set_owner = event_target:corporate_alliance
				generate_cyphar_buildings = yes
				generate_17_misc_pops = yes
			}
		}
		change_orbit = @base_moon_dist
	}
	planet = {
		count = { min = 3 max = 4 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 2 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		class = pc_asteroid
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
	}
}
