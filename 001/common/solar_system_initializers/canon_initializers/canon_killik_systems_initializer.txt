
@base_planet_dist = 25
@base_moon_dist = 15

#Yoggoy
yoggoy_system_initializer = {
	name = "Yoggoy"
	class = "rl_standard_stars"
	flags = { killik_homeworld killik_start_system canon_map canon_map_shipyard_system }
	init_effect = { generate_home_system_resources = yes log = "killik homeworld" add_modifier = { modifier = sy_unknown_regions } }
	usage = custom_empire
	max_instances = 1
	planet = {
		name = "Yoggoy"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		count = { min = 2 max = 3 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 1 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		name = "Yoggoy"
		class = pc_continental
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 20 max = 25 }
		has_ring = no
		starting_planet = yes
		deposit_blockers = none
		modifiers = none
		init_effect = { prevent_anomaly = yes set_planet_flag = planet_yoggoy }
		init_effect = {
			set_global_flag = killik_homeworld_spawned
			if = {
				limit = { NOT = { any_country = { has_country_flag = killik_colony } } }
				create_species = { 
				    name = "Killik" 
				    class = KIL
				    portrait = killik 
				    homeworld = THIS 
				    traits = { 
				        trait = "trait_hive_mind" 
				        trait = "trait_very_strong" 
				        trait = "trait_slow_breeders"
				        ideal_planet_class = "pc_continental" 
					} 
				}
				last_created_species = { save_global_event_target_as = killikSpecies }
				create_country = {
					name = "NAME_killik_colony"
					ship_prefix = " "
					type = default
					origin = "origin_swp_killik"
					ignore_initial_colony_error = yes
					civics = { civic = "civic_hive_subsumed_will" civic = "civic_hive_one_mind" }
					authority = auth_hive_mind
					name_list = "Killik"
					ethos = { ethic = "ethic_gestalt_consciousness" }
					species = event_target:killikSpecies
					flag = {
						icon = { category = "starwars" file = "KillikColony.dds" }
						background = { category = "backgrounds" file = "00_solid.dds" }
						colors = { "customcolor1758" "customcolor1758" "null" "null" }
					}
					effect = {
						set_graphical_culture = misc_01
						set_country_flag = killik_colony
						set_country_flag = init_spawned
						set_country_flag = custom_start_screen
						set_country_flag = canon_map
						set_country_flag = sandbox_map
						save_global_event_target_as = killik_colony
					}
				}
				#create_colony = { owner = event_target:killik_colony species = event_target:killikSpecies ethos = owner }
			}
			set_capital = yes
			random_country = {
				limit = { has_country_flag = killik_colony }
				save_global_event_target_as = killik_colony
				species = { save_global_event_target_as = killikSpecies }
			}
			set_owner = event_target:killik_colony
			generate_yoggoy_buildings = yes
			generate_30_main_pops = yes
			give_start_techs = yes
			set_name = "NAME_Yoggoy"
		}
		init_effect = {
			save_event_target_as = shipyard_site
			solar_system = {
				spawn_megastructure = {
					type = "swp_orbital_shipyard_0" 
					location = event_target:shipyard_site
					owner = event_target:killik_colony
					graphical_culture = event_target:killik_colony
				}
			}
		}
		change_orbit = @base_moon_dist
	}
	planet = {
		count = { min = 3 max = 4 }
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
		
		change_orbit = @base_moon_dist
		
		moon = {
			count = { min = 0 max = 2 }
			orbit_angle = { min = 90 max = 270 }
			orbit_distance = 2
		}
	}
	planet = {
		class = pc_asteroid
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 90 max = 270 }
	}
}
