
@base_planet_dist = 25
@base_moon_dist = 15

#Bakura
sandbox_bakura_system_initializer = {
	name = "Bakura"
	class = "sc_g"
	flags = { bakuran_homeworld bakuran_start_system sandbox_map canon_map_shipyard_system }
	init_effect = { generate_home_system_resources = yes log = "bakuran homeworld" }
	usage = custom_empire
	max_instances = 1
	planet = {
		name = "Bakura"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		name = "Bak"
		class = pc_molten
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Kur"
		class = pc_barren
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Bakura"
		class = pc_continental
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 15 max = 20 }
		has_ring = no
		starting_planet = yes
		deposit_blockers = none
		modifiers = none
		init_effect = { prevent_anomaly = yes set_planet_flag = planet_bakura }
		init_effect = {
			set_global_flag = bakuran_homeworld_spawned
			if = {
				limit = { NOT = { any_country = { has_country_flag = bakuran_senate } } }
				create_species = { 
				    name = "Human" 
				    class = HUMAN1
				    portrait = human 
				    homeworld = THIS 
				    traits = { 
				        trait = "trait_intelligent" #trait_adaptive
				        ideal_planet_class = "pc_continental" 
					} 
				}
				last_created_species = { save_global_event_target_as = bakuranSpecies }
				create_country = {
					name = "NAME_bakuran_senate"
					ship_prefix = " "
					type = default
					origin = "origin_swp_default"
					ignore_initial_colony_error = yes
					civics = { civic = "civic_droid_uprising" civic = "civic_parliamentary_system" }
					authority = auth_democratic
					name_list = "SWP_Human1"
					ethos = { ethic = "ethic_egalitarian" ethic = "ethic_materialist" ethic = "ethic_xenophobe" }
					species = event_target:bakuranSpecies
					flag = {
						icon = { category = "starwars" file = "BakuranSenate.dds" }
						background = { category = "backgrounds" file = "00_solid.dds" }
						colors = { "customcolor507" "customcolor507" "null" "null" }
					}
					effect = {
						set_graphical_culture = misc_01
						set_country_flag = bakuran_senate
						set_country_flag = init_spawned
						set_country_flag = custom_start_screen
						set_country_flag = sandbox_map
						save_global_event_target_as = bakuran_senate
					}
				}
				#create_colony = { owner = event_target:bakuran_senate species = event_target:bakuranSpecies ethos = owner }
			}
			set_capital = yes
			random_country = {
				limit = { has_country_flag = bakuran_senate }
				save_global_event_target_as = bakuran_senate
				species = { save_global_event_target_as = bakuranSpecies }
			}
			set_owner = event_target:bakuran_senate
			generate_bakura_buildings = yes
			generate_23_main_pops = yes
			give_start_techs = yes
			set_name = "NAME_Bakura"
		}
		init_effect = {
			save_event_target_as = shipyard_site
			solar_system = {
				spawn_megastructure = {
					type = "swp_orbital_shipyard_0" 
					location = event_target:shipyard_site
					owner = event_target:bakuran_senate
					graphical_culture = event_target:bakuran_senate
				}
			}
		}
		change_orbit = @base_moon_dist
		moon = {
			name = "Bakura a"
			class = pc_barren
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Bakura b"
			class = pc_barren
			orbit_distance = 0
			orbit_angle = 120
			size = { min = 2 max = 4 }
		}
	}
	planet = {
		name = "Arden"
		class = pc_gas_giant
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 25 max = 30 }
		has_ring = no
		change_orbit = @base_moon_dist
		moon = {
			name = "Arden a"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Arden b"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Arden c"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Arden d"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Arden e"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Arden f"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Arden g"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
	}
	planet = {
		name = "Bakura V"
		class = pc_barren
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
		moon = {
			name = "Bakura Va"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
	}
	planet = {
		name = "Bakura VI"
		class = pc_arctic
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Bakura VII"
		class = pc_frozen
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Bakura VIII"
		class = pc_frozen
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
		moon = {
			name = "Bakura VIIIa"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
	}
}