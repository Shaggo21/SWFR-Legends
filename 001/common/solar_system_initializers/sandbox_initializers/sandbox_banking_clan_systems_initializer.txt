
@base_planet_dist = 25
@base_moon_dist = 15

#Muunilinst
sandbox_muunilinst_system_initializer = {
	name = "Muunilinst"
	class = "sc_g"
	flags = { banking_clan_homeworld banking_start_system sandbox_map canon_map_shipyard_system }
	init_effect = { generate_home_system_resources = yes log = "banking_clan_homeworld" add_modifier = { modifier = sy_entralla_route } }
	usage = custom_empire
	max_instances = 1
	planet = {
		name = "Muunilinst"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		name = "Varhus"
		class = random_non_colonizable
		orbit_distance = { min = 20 max = 20 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Trinciuum"
		class = pc_gas_giant
		orbit_distance = { min = 40 max = 40 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 25 max = 30 }
		has_ring = no
		change_orbit = @base_moon_dist
		moon = {
			name = "Trinciuum a"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
	}
	planet = {
		name = "Muunilinst"
		class = pc_continental
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 20 max = 25 }
		has_ring = no
		starting_planet = yes
		deposit_blockers = none
		modifiers = none
		init_effect = { prevent_anomaly = yes set_planet_flag = planet_muunilinst }
		init_effect = {
			set_global_flag = banking_clan_homeworld_spawned
			if = {
				limit = { NOT = { any_country = { has_country_flag = banking_clan } } }
				create_species = { 
				    name = "Muun" 
				    class = MUN
				    portrait = muun 
				    homeworld = THIS 
				    traits = { 
				        trait = "trait_natural_physicists" 
				        trait = "trait_thrifty" 
				        trait = "trait_weak"
				        ideal_planet_class = "pc_continental" 
					} 
				}
				last_created_species = { save_global_event_target_as = muunSpecies }
				create_country = {
					name = "NAME_banking_clan"
					ship_prefix = " "
					type = default
					origin = "origin_swp_confed_remnant"
					ignore_initial_colony_error = yes
					civics = { civic = "civic_banking_clan" civic = "civic_franchising" }
					authority = auth_corporate
					name_list = "Muun"
					ethos = { ethic = "ethic_fanatic_materialist" ethic = "ethic_egalitarian" }
					species = event_target:muunSpecies
					flag = {
						icon = { category = "starwars" file = "InterGalacticBankingClan.dds" }
						background = { category = "backgrounds" file = "00_solid.dds" }
						colors = { "customcolor1698" "customcolor1698" "null" "null" }
					}
					effect = {
						set_graphical_culture = misc_01
						set_country_flag = banking_clan
						set_country_flag = init_spawned
						set_country_flag = custom_start_screen
						set_country_flag = sandbox_map
						save_global_event_target_as = banking_clan
					}
				}
				#create_colony = { owner = event_target:banking_clan species = event_target:muunSpecies ethos = owner }
			}
			set_capital = yes
			random_country = {
				limit = { has_country_flag = banking_clan }
				save_global_event_target_as = banking_clan
				species = { save_global_event_target_as = muunSpecies }
			}
			set_owner = event_target:banking_clan
			generate_muunilinst_buildings = yes
			generate_23_main_pops = yes
			give_start_techs = yes
			set_name = "NAME_Muunilinst"
		}
		init_effect = {
			save_event_target_as = shipyard_site
			solar_system = {
				spawn_megastructure = {
					type = "swp_orbital_shipyard_0" 
					location = event_target:shipyard_site
					owner = event_target:banking_clan
					graphical_culture = event_target:banking_clan
				}
			}
		}
		change_orbit = @base_moon_dist
		moon = {
			name = "Muunilinst a"
			class = pc_barren
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Muunilinst b"
			class = pc_barren
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
	}
	asteroid_belt = {
		type = rocky_asteroid_belt
		radius = 145
	}
	planet = {
		class = pc_asteroid
		orbit_distance = 30
		orbit_angle = { min = 40 max = 180 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		count = { min = 1 max = 2 }
		class = pc_asteroid
		orbit_distance = 0
		orbit_angle = { min = 40 max = 180 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Havath Prime"
		class = pc_gas_giant
		orbit_distance = { min = 40 max = 40 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 25 max = 30 }
		has_ring = no
		change_orbit = @base_moon_dist
		moon = {
			name = "Havath Prime a"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Havath Prime b"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Havath Prime c"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Havath Prime d"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Havath Prime e"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Havath Prime f"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Havath Prime g"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Havath Prime h"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Havath Prime i"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Havath Prime j"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Havath Prime k"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Havath Prime l"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
	}
	planet = {
		name = "Havath Minor"
		class = pc_gas_giant
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 25 max = 30 }
		has_ring = no
		change_orbit = @base_moon_dist
		moon = {
			name = "Havath Minor a"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Havath Minor b"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Havath Minor c"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Havath Minor d"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
	}
}