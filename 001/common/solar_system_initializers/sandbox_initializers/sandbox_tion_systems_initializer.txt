
@base_planet_dist = 25
@base_moon_dist = 15

#Desevro
sandbox_desevro_system_initializer = {
	name = "Desevro"
	class = "sc_desev"
	flags = { tion_homeworld tion_start_system sandbox_map canon_map_shipyard_system }
	init_effect = { generate_home_system_resources = yes log = "tion homeworld" add_modifier = { modifier = sy_perlemian_trade_route } }
	usage = custom_empire
	max_instances = 1
	planet = {
		name = "Desev"
		class = star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	change_orbit = @base_planet_dist
	planet = {
		name = "Maugina"
		class = star
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 15 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
	planet = {
		name = "Arjus"
		class = pc_gas_giant
		orbit_distance = { min = 65 max = 65 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 25 max = 30 }
		has_ring = no
		change_orbit = @base_moon_dist
		moon = {
			name = "Arjus a"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Arjus b"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Arjus c"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Arjus d"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Arjus e"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Arjus f"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Arjus g"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Arjus h"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Arjus i"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Arjus j"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Arjus k"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Arjus l"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Arjus m"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Arjus n"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Arjus o"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Arjus p"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Arjus q"
			class = random_non_colonizable
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Arjus r"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Arjus s"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
		moon = {
			name = "Arjus t"
			class = random_non_colonizable
			orbit_distance = 0
			orbit_angle = 90
			size = { min = 2 max = 4 }
		}
	}
	planet = {
		name = "Desevro"
		class = pc_ecumenopolis_light
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 20 max = 25 }
		has_ring = no
		entity = "pc_desevro"
		starting_planet = yes
		deposit_blockers = none
		modifiers = none
		init_effect = { prevent_anomaly = yes set_planet_flag = planet_desevro }
		init_effect = {
			set_global_flag = tion_homeworld_spawned
			if = {
				limit = { NOT = { any_country = { has_country_flag = tion_hegemony } } }
				create_species = { 
				    name = "Human" 
				    class = HUMAN2
				    portrait = human 
				    homeworld = THIS 
				    traits = { 
				        trait = "trait_adaptive"
				        ideal_planet_class = "pc_continental" 
					} 
				}
				last_created_species = { save_global_event_target_as = tionSpecies }
				create_country = {
					name = "NAME_tion_hegemony"
					ship_prefix = " "
					type = default
					origin = "origin_swp_confed_remnant"
					ignore_initial_colony_error = yes
					civics = { civic = "civic_distinguished_admiralty" civic = "civic_clone_wars_loser" }
					authority = auth_dictatorial
					name_list = "SWP_Human2"
					ethos = { ethic = "ethic_fanatic_authoritarian" ethic = "ethic_militarist" }
					species = event_target:tionSpecies
					flag = {
						icon = { category = "starwars" file = "TionHegemony.dds" }
						background = { category = "backgrounds" file = "00_solid.dds" }
						colors = { "customcolor932" "customcolor932" "null" "null" }
					}
					effect = {
						set_graphical_culture = misc_02
						set_country_flag = tion_hegemony
						set_country_flag = init_spawned
						set_country_flag = custom_start_screen
						set_country_flag = sandbox_map
						save_global_event_target_as = tion_hegemony
					}
				}
				#create_colony = { owner = event_target:tion_hegemony species = event_target:tionSpecies ethos = owner }
			}
			set_capital = yes
			random_country = {
				limit = { has_country_flag = tion_hegemony }
				save_global_event_target_as = tion_hegemony
				species = { save_global_event_target_as = tionSpecies }
			}
			set_owner = event_target:tion_hegemony
			generate_desevro_buildings = yes
			generate_15_main_1_SYM_13_misc_pops = yes
			give_start_techs = yes
			set_name = "NAME_Desevro"
		}
		init_effect = {
			save_event_target_as = shipyard_site
			solar_system = {
				spawn_megastructure = {
					type = "swp_orbital_shipyard_0" 
					location = event_target:shipyard_site
					owner = event_target:tion_hegemony
					graphical_culture = event_target:tion_hegemony
				}
			}
		}
		change_orbit = @base_moon_dist
		moon = {
			name = "Desevro a"
			class = pc_barren
			orbit_distance = 2
			orbit_angle = { min = 40 max = 180 }
			size = { min = 2 max = 4 }
		}
	}
	planet = {
		name = "Gauther"
		class = random_non_colonizable
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		change_orbit = @base_moon_dist
	}
}