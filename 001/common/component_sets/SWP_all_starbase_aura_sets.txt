

component_set = { key = "STARBASE_AURA_FTL_INHIBITOR" icon = "GFX_ship_part_STARBASE_AURA_FTL_INHIBITOR" icon_frame = 1 }
component_set = { key = "STARBASE_AURA_TRAFFIC" icon = "GFX_ship_part_STARBASE_AURA_TRAFFIC" icon_frame = 1 }
component_set = { key = "STARBASE_AURA_COMMAND_CENTER" icon = "GFX_ship_part_STARBASE_AURA_COMMAND_CENTER" icon_frame = 1 }
component_set = { key = "STARBASE_AURA_DISRUPTION_FIELD" icon = "GFX_ship_part_STARBASE_AURA_DISRUPTION_FIELD" icon_frame = 1 }
component_set = { key = "STARBASE_AURA_COMMUNICATIONS_JAMMER" icon = "GFX_ship_part_STARBASE_AURA_COMMUNICATIONS_JAMMER" icon_frame = 1 }
component_set = { key = "STARBASE_AURA_TRACTOR_BEAM" icon = "GFX_ship_part_STARBASE_AURA_TRACTOR_BEAM" icon_frame = 1 }