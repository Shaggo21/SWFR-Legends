#########################################################################
# SWP OPINION MODIFIERS
##########################################################################

# Rebellion 
em_empire_focus_tier3_republic_loyalist = {
	opinion = {
		base = 300
	}

	min = 0

	decay = {
		base = 10
	}
}

## Opinion modifiers for confronting a senator event as galactic empire
opinion_senator_confronted = {
	opinion = {
		base = -40
	}

	min = 0

	decay = {
		base = 2
	}
}

## Opinion modifiers for leave the senator event as galactic empire
opinion_senator_not_confronted = {
	opinion = {
		base = 40
	}

	min = 0

	decay = {
		base = 2
	}
}