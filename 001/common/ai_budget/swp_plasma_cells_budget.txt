## Categories used in this file must have use_for_ai_budget = yes defined in economic_categories ##
## It is possible to have multiple entries with the same category and resource ##

#################
## Expenditure ##
#################
plasma_cells_expenditure_buffer = { # Buffer, will not be spent
	resource = sr_plasma_cells 
	type = expenditure
	category = buffer
	
	potential = {
		always = no
	}
	
	fraction = {
		weight = 0.05
	}	
}

plasma_cells_expenditure_ships = {
	resource = sr_plasma_cells 
	type = expenditure
	category = ships
	
	potential = {
		always = yes
	}
	
	fraction = {
		weight = 0.8

		# spend more plasma_cells building ships while at war
		modifier = {
			factor = 2
			is_at_war = yes
		}

		# spend less plasma_cells on ships if over capacity
		modifier = {
			factor = 0.5
			used_naval_capacity_percent >= 1.0
		}
	}	
	static_min = {
		base = 0
		modifier = {
			add = 500
			resource_stockpile_compare = {
				resource = sr_plasma_cells
				value > 1000
			}
		}
	}
}

plasma_cells_expenditure_ship_upgrades = {
	resource = sr_plasma_cells 
	type = expenditure
	category = ship_upgrades

	potential = {
		is_at_war = no
	}

	fraction = {
		weight = 0.1

		# spend more on ship upgrades if at capacity
		modifier = {
			factor = 4
			used_naval_capacity_percent >= 1.0
		}
	}
	static_min = {
		base = 0
		modifier = {
			add = 500
			resource_stockpile_compare = {
				resource = sr_plasma_cells
				value > 1000
			}
			used_naval_capacity_percent > 0.95
		}
	}
}

plasma_cells_expenditure_starbases = {
	resource = sr_plasma_cells 
	type = expenditure
	category = starbases
	
	potential = {
		NOT = {
			is_country_type = fallen_empire
		}
	}

	fraction = {
		weight = 0.01
	}
}

plasma_cells_expenditure_starbases_fallen_empires = {
	resource = sr_plasma_cells 
	type = expenditure
	category = starbases
	
	potential = {
		is_country_type = fallen_empire
	}
	
	fraction = {
		weight = 0.01
	}	
}

plasma_cells_expenditure_starbases_expand = {
	resource = sr_plasma_cells 
	type = expenditure
	category = starbases
	
	potential = {
		has_ai_expansion_plan = yes
		highest_threat < 50
		NOT = {
			is_country_type = fallen_empire
			is_country_type = awakened_fallen_empire
		}
		has_resource = { type = influence amount > 75 }
	}

	fraction = {
		weight = 0.01
	}

	static_min = {
		base = 150
	}
}

plasma_cells_expenditure_edicts = {
	resource = sr_plasma_cells 
	type = expenditure
	category = edicts
	
	potential = {
		always = yes
	}
	
	fraction = {
		base = 0.05
	}
}

plasma_cells_expenditure_armies = {
	resource = sr_plasma_cells 
	type = expenditure
	category = armies
	
	potential = {
		always = yes
	}
	
	fraction = {
		weight = 0.15
	}	
}

plasma_cells_expenditure_armies_threatened = {
	resource = sr_plasma_cells
	type = expenditure
	category = armies

	potential = {
		highest_threat > 20
	}

	fraction = {
		weight = 0.20
	}
}

plasma_cells_expenditure_trade = {
	resource = sr_plasma_cells 
	type = expenditure
	category = trade
	
	potential = {
		always = yes
	}
	
	fraction = {
		weight = 0.05
	}	
}

############
## Upkeep ##
############
plasma_cells_upkeep_buffer = {	# Buffer, will not be spent
	resource = sr_plasma_cells 
	type = upkeep
	category = buffer
	
	potential = {
		always = yes
	}
	
	fraction = {
		weight = 0.05
	}	
}

plasma_cells_upkeep_ships = {
	resource = sr_plasma_cells 
	type = upkeep
	category = ships
	
	potential = {
		always = yes
	}
	
	fraction = {
		weight = 0.95
	}	
}